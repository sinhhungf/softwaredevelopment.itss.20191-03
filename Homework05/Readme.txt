Delegate tasks to members:
- Vũ Việt Hưng: design user interface, design class diagram and data modeling for managing inventory
- Nguyễn Sinh Hùng: design user interface, design class diagram and data modeling for managing requests from customer
- Nguyễn Công Thành: design user interface, design class diagram and data modeling for managing sites
- Phạm Hồng Quang: design user interface, design class diagram and data modeling for managing orders
