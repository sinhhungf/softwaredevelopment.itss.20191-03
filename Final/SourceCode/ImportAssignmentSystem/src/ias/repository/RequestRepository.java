package ias.repository;

import ias.model.BaseModel;
import ias.model.Request;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RequestRepository extends BaseRepository {
    public RequestRepository(Request model) {
        super(model);
    }

    protected Request getModel(ResultSet resultSet) {
        try {
            return new Request(resultSet.getDate("modified_date"),
                    resultSet.getString("id"),
                    resultSet.getString("merchandise_id"),
                    resultSet.getString("employee_id"),
                    resultSet.getString("customer_id"),
                    resultSet.getDate("desire_delivery_date"),
                    resultSet.getInt("request_quantity") );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected String generateInsertRecord(BaseModel model) {
        String insertRecord = "'%s', '%s', '%s', '%s', '%s', '%s'";
        Request request = (Request) model;
        insertRecord = String.format(insertRecord,
                request.getRequestID(),
                request.getMerchandiseID(),
                request.getEmployeeID(),
                request.getCustomerID(),
                new SimpleDateFormat("yyyy-MM-dd").format(request.getDate()),
                String.valueOf(request.getQuantity()),
                new Date().toString());
        return insertRecord;
    }

    protected String generateUpdateRecord(BaseModel model) {
        String updateRecord =
                "id = '%s', merchandise_id = '%s', employee_id = '%s', customer_id = '%s', desire_delivery_date = '%s', request_quantity = %d, modified_date = '%s'";
        Request request = (Request) model;
        updateRecord = String.format(updateRecord,
                request.getRequestID(),
                request.getMerchandiseID(),
                request.getEmployeeID(),
                request.getCustomerID(),
                request.getDate().toString(),
                request.getQuantity(),
                new Date().toString());
        return updateRecord;
    }
}
