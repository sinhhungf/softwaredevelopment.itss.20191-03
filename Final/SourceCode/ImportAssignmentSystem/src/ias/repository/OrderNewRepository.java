package ias.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import ias.model.BaseModel;
import ias.model.OrderNew;

public class OrderNewRepository extends BaseRepository {

	public OrderNewRepository(OrderNew model) {
		// TODO Auto-generated constructor stub
		super(model);
	}
	
	protected OrderNew getModel(ResultSet resultSet) {
		try {
			return new OrderNew(resultSet.getDate("modified_date"), resultSet.getString("site_id"),
					resultSet.getString("employee_id"), resultSet.getString("merchandise_id"),
					resultSet.getInt("order_quantity"), resultSet.getString("delivery_mean"),
					resultSet.getInt("return_quantity"), resultSet.getInt("state"), resultSet.getString("id"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	protected String generateInsertRecord(BaseModel model) {
		String insertRecord = "'%s', '%s', '%s', %s, '%s', %s, %s, '%s', '%s' ";
		OrderNew order = (OrderNew) model;
		insertRecord = String.format(insertRecord, order.getEmployeeCode(), order.getMerchandiseCode(),
				order.getSiteCode(), order.getOrderQuantity(), order.getDeliveryMean(), order.getReturnQuantity(),
				order.getState(), order.getModifiedDate(), order.getOrderCode());
		return insertRecord;
	}
	
	protected String generateUpdateRecord(BaseModel model) {
		String updateRecord = "id  = '%s',employee_id = '%s', merchandise_id = '%s', site_id = '%s', order_quantity = %d,"
				+ " delivery_mean = '%s', return_quantity = %d, state = %d, modified_date = '%s'";
		OrderNew order = (OrderNew) model;
		updateRecord = String.format(updateRecord, order.getOrderCode(),order.getEmployeeCode(), order.getMerchandiseCode(), order.getSiteCode(),
				order.getOrderQuantity(), order.getDeliveryMean(), order.getReturnQuantity(), order.getState() ,order.getModifiedDate());
		return updateRecord;
	}
	
}
