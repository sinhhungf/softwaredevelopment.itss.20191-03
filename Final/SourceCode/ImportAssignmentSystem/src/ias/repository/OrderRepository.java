package ias.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import ias.model.BaseModel;
import ias.model.Supply;
import ias.model.Site;
import ias.model.Order;

public class OrderRepository extends BaseRepository {
	public OrderRepository(Order model) {
		super(model);
	}

	/*
	 * action get all data model
	 * 
	 * @ override
	 */
	protected Order getModel(ResultSet resultSet) {
		try {
			return new Order(resultSet.getString("id"), resultSet.getDate("modified_date"), resultSet.getString("site_id"),
					resultSet.getString("employee_id"), resultSet.getString("merchandise_id"),
					resultSet.getInt("order_quantity"), resultSet.getString("delivery_mean"),
					resultSet.getInt("return_quantity"), resultSet.getInt("state"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	protected String generateInsertRecord(BaseModel model) {
		String insertRecord = "'%s', '%s', '%s', %s, '%s', %s, %s, '%s', '%s'";
		Order order = (Order) model;
		insertRecord = String.format(insertRecord, order.getEmployeeCode(), order.getMerchandiseCode(),
				order.getSiteCode(), order.getOrderQuantity(), order.getDeliveryMean(), order.getReturnQuantity(),
				order.getState(), order.getModifiedDate(), order.getOrderId());
		return insertRecord;
	}

	/*
	 * function generate query sql update record
	 * 
	 * @param {BaseModel} model - model that extends BaseModel
	 * 
	 * @override
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	protected String generateUpdateRecord(BaseModel model) {
		String updateRecord = "employee_id = '%s', merchandise_id = '%s', site_id = '%s', order_quantity = %s, delivery_mean = '%s', return_quantity = %s, state = %s, modified_date = '%s', id = '%s'";
		Order order = (Order) model;
		updateRecord = String.format(updateRecord, order.getEmployeeCode(), order.getMerchandiseCode(), order.getSiteCode(),
				order.getOrderQuantity(), order.getDeliveryMean(), order.getReturnQuantity(), order.getState(), order.getModifiedDate(), order.getOrderId());
		return updateRecord;
	}

}
