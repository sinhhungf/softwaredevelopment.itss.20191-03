package ias.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import ias.model.BaseModel;
import ias.model.InstockMerchandise;
import ias.model.Merchandise;

public class MerchandiseRepository extends BaseRepository {
	public MerchandiseRepository(Merchandise model) {
		super(model);
	}

	/*
	 * action get all data model
	 * 
	 * @override
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	protected Merchandise getModel(ResultSet resultSet) {
		try {
			return new Merchandise(resultSet.getDate("modified_date"), 
					resultSet.getString("id"), 
					resultSet.getString("merchandise_name"),
					resultSet.getString("unit"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * function generate query sql insert record template
	 * "<value>, <value>, <value>, <value>, ..."
	 * 
	 * @param {BaseModel} model - model that extends BaseModel
	 * 
	 * @override
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	protected String generateInsertRecord(BaseModel model) {
		String insertRecord = "'%s', '%s', '%s', '%s'";
		Merchandise merchandise = (Merchandise) model;
		insertRecord = String.format(insertRecord, 
				merchandise.getMerchandiseId(), 
				merchandise.getMerchandiseName(), 
				merchandise.getUnit(),
				new Date().toString());
		return insertRecord;
	}

	/*
	 * function generate query sql update record
	 * 
	 * @param {BaseModel} model - model that extends BaseModel
	 * 
	 * @override
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	protected String generateUpdateRecord(BaseModel model) {
		String updateRecord = "id = '%s', merchandise_name = '%s', unit = '%s', modified_date = '%s'";
		Merchandise merchandise = (Merchandise) model;
		updateRecord = String.format(updateRecord, 
				merchandise.getMerchandiseId(), 
				merchandise.getMerchandiseName(),
				merchandise.getUnit(),
				new Date());
		return updateRecord;
	}
}