package ias.model;

import java.util.Date;

public class Customer extends BaseModel {
    private String id;
    private String name;
    private String address;

    public Customer() {
        super(new Date());
        this.id = "";
        this.name = "";
        this.address = "";
    }

    public Customer(Date modifiedDate, String id, String name, String address) {
        super(modifiedDate);
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getTableName() {
        return "customer";
    }

    @Override
    public String getPrimaryKeyFieldName() {
        return "id";
    }

    @Override
    public String getPrimaryKeyFieldValue() {
        return this.id;
    }


    @Override
    public String toString() {
        return this.id + " - " + this.name + " - " + this.address;
    }
}
