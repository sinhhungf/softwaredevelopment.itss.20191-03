package ias.model;

import java.util.Date;

public class Order extends BaseModel{
	private String OrderId;
	private String SiteCode;
	private String EmployeeCode;
	private String MerchandiseCode;
	private int OrderQuantity;
	private String DeliveryMean;
	private int ReturnQuantity;
	private int State;
	
	public Order() {
		super(new Date());
		SiteCode = "";
		EmployeeCode = "";
		MerchandiseCode = "";
		DeliveryMean = "";
		ReturnQuantity = 0;
		State = 0;
	}
	
	public Order(String orderId, Date modifiedDate, String siteCode, String employeCode,
					String merchandiseCode, int orderQuantity, 
					String deliveryMean, int returnQuantity, int state) {
		super(modifiedDate);
		OrderId = orderId;
		SiteCode = siteCode;
		EmployeeCode = employeCode;
		MerchandiseCode = merchandiseCode;
		OrderQuantity = orderQuantity;
		DeliveryMean = deliveryMean;
		ReturnQuantity = returnQuantity;
		State = state;
	}
	
	public String getSiteCode() {
		return SiteCode;
	}
	
	public String getOrderId() {
		return OrderId;
	}

	public void setSiteCode(String siteCode) {
		SiteCode = siteCode;
	}
	
	public String getEmployeeCode() {
		return EmployeeCode;
	}
	
	public void setEmployeeCode(String employeeCode) {
		EmployeeCode = employeeCode;
	}
	
	public int orderQuantity() {
		return OrderQuantity;
	}
	
	public int getOrderQuantity() {
		return OrderQuantity;
	}

	public void setOrderQuantity(int orderQuantity) {
		OrderQuantity = orderQuantity;
	}

	public String getMerchandiseCode() {
		return MerchandiseCode;
	}
	
	public void setMerchandiseCode(String merchandiseCode) {
		MerchandiseCode = merchandiseCode;
	}

	public String getDeliveryMean() {
		return DeliveryMean;
	}

	public void setDeliveryMean(String deliveryMean) {
		DeliveryMean = deliveryMean;
	}

	public int getReturnQuantity() {
		return ReturnQuantity;
	}

	public void setReturnQuantity(int returnQuantity) {
		ReturnQuantity = returnQuantity;
	}

	public int getState() {
		return State;
	}

	public void setState(int state) {
		State = state;
	}
	
	public String getTableName() {
		return "order_new";
	}
	
	public String getPrimaryKeyFieldName() {
		return "id";
	}
	
	public String getPrimaryKeyFieldValue() {
		return this.OrderId;
	}


}
