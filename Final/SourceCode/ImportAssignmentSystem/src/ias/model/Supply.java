package ias.model;

import java.util.Date;

public class Supply extends BaseModel {
	private String SiteCode;
	private int InstockQuantity;
	private String MerchandiseCode;
	
	public Supply() {
		super(new Date());
		SiteCode = "";
		MerchandiseCode = "";
		InstockQuantity = 0;
	}
	
	public Supply(Date modifiedDate, String merchandiseCode, String siteCode, String siteName, int instockQuantity) {
		super(modifiedDate);
		MerchandiseCode = merchandiseCode;
		SiteCode = siteCode;
		InstockQuantity = instockQuantity;
	}

	public String getSiteCode() {
		return SiteCode;
	}

	public void setSiteCode(String siteCode) {
		SiteCode = siteCode;
	}
	
	public String getMerchandiseCode() {
		return MerchandiseCode;
	}

	public void setMerchandiseCode(String merchandiseCode) {
		MerchandiseCode = merchandiseCode;
	}


	public int getInstockQuantity() {
		return InstockQuantity;
	}

	public void setInstockQuantity(int instockQuantity) {
		InstockQuantity = instockQuantity;
	}

}
