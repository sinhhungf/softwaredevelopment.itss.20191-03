package ias.model;

import java.util.Date;

public class InstockMerchandise extends Merchandise {
	private String SupplyId;
	private String SiteId;
	private String SiteName;
	private int InstockQuantity;
	private int DeliveryByShip;
	private int DeliveryByAir;
	
	public InstockMerchandise() {}

	public InstockMerchandise(String merchandiseId, String siteId, int instockQuantity) {
		SiteId = siteId;
		InstockQuantity = instockQuantity;
		MerchandiseId = merchandiseId;
	}

	public InstockMerchandise(Date modifiedDate, String supplyId, String merchandiseId, String merchandiseName,
			String unit, String siteId, String siteName, int instockQuantity, int deliveryByShip, int deliveryByAir) {
		super(modifiedDate, merchandiseId, merchandiseName, unit);
		SupplyId = supplyId;
		SiteId = siteId;
		SiteName = siteName;
		InstockQuantity = instockQuantity;
		DeliveryByShip = deliveryByShip;
		DeliveryByAir = deliveryByAir;
	}

	public String getSupplyId() {
		return SupplyId;
	}

	public void setSupplyId(String supplyId) {
		SupplyId = supplyId;
	}

	public String getMerchandiseId() {
		return MerchandiseId;
	}

	public void setMerchandiseId(String merchandiseId) {
		MerchandiseId = merchandiseId;
	}

	public String getMerchandiseName() {
		return MerchandiseName;
	}

	public void setMerchandiseName(String merchandiseName) {
		MerchandiseName = merchandiseName;
	}

	public String getSiteId() {
		return SiteId;
	}

	public void setSiteId(String siteId) {
		SiteId = siteId;
	}

	public String getSiteName() {
		return SiteName;
	}

	public void setSiteName(String siteName) {
		SiteName = siteName;
	}

	public int getInstockQuantity() {
		return InstockQuantity;
	}

	public void setInstockQuantity(int instockQuantity) {
		InstockQuantity = instockQuantity;
	}

	public int getDeliveryByShip() {
		return DeliveryByShip;
	}

	public void setDeliveryByShip(int deliveryByShip) {
		DeliveryByShip = deliveryByShip;
	}

	public int getDeliveryByAir() {
		return DeliveryByAir;
	}

	public void setDeliveryByAir(int deliveryByAir) {
		DeliveryByAir = deliveryByAir;
	}

	public String getTableName() {
		return "supply";
	}

	public String getPrimaryKeyFieldName() {
		return "id";
	}

	public String getPrimaryKeyFieldValue() {
		return this.SupplyId;
	}
}
