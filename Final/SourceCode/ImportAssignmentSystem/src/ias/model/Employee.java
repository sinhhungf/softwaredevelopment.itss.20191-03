package ias.model;

import java.util.Date;

public class Employee extends BaseModel {
    private String id;
    private String employee_name;
    private String role;
    private String user_name;
    private String password;

    public Employee() {
        super(new Date());
        this.id = "";
        this.employee_name = "";
        this.role = "";
        this.user_name = "";
        this.password = "";
    }

    public Employee(Date modifiedDate, String id, String employee_name, String role, String user_name, String password) {
        super(modifiedDate);
        this.id = id;
        this.employee_name = employee_name;
        this.role = role;
        this.user_name = user_name;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String getTableName() {
        return "employee";
    }

    @Override
    public String getPrimaryKeyFieldName() {
        return "id";
    }

    @Override
    public String getPrimaryKeyFieldValue() {
        return this.id;
    }

    @Override
    public String toString() {
        return this.id + " - " + this.employee_name;
    }
}
