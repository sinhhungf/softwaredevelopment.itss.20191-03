package ias.model;

import java.util.Date;

public class Request extends BaseModel {
    private String requestID;
    private String merchandiseID;
    private String employeeID;
    private String customerID;
    private Date date;
    private int quantity;

    public Request() {
        super(new Date());
        requestID = "";
        merchandiseID = "";
        employeeID = "";
        customerID = "";
        quantity = 0;
        date = new Date();
    }

    public Request(Date modifiedDate, String requestID, String merchandiseID, String employeeID, String customerID,
            Date date, int quantity) {
        super(modifiedDate);
        this.requestID = requestID;
        this.merchandiseID = merchandiseID;
        this.employeeID = employeeID;
        this.customerID = customerID;
        this.quantity = quantity;
        this.date = date;
    }

    public String getRequestID() {
        return requestID;
    }

    public String getMerchandiseID() {
        return merchandiseID;
    }

    public String getEmployeeID() {
        return employeeID;
    }

    public String getCustomerID() {
        return customerID;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getTableName() {
        return "request";
    }

    public String getPrimaryKeyFieldName() {
        return "id";
    }

    public String getPrimaryKeyFieldValue() {
        return this.requestID;
    }

    @Override
    public String toString() {
        return this.merchandiseID;
    }
}