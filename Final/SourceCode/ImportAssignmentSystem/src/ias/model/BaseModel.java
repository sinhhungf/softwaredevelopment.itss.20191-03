package ias.model;

import java.util.Date;

public abstract class BaseModel {
	protected Date ModifiedDate;
	
	public BaseModel(Date modifiedDate) {
		super();
		ModifiedDate = modifiedDate;
	}

	public Date getModifiedDate() {
		return ModifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		ModifiedDate = modifiedDate;
	}

	public String getTableName() {
		return null;
	}
	
	public String getPrimaryKeyFieldName() {
		return null;
	}
	
	public String getPrimaryKeyFieldValue() {
		return null;
	}
	
	public String getForeignKeyFieldName() {
		return null;
	}
	
	public String getForeignKeyFieldValue() {
		return null;
	}
}
