package ias.common;

import java.util.ArrayList;

import ias.model.BaseModel;

public class ServiceResponse {
	public boolean Success;
	public ResponseCode Code;
	public String Message;
	public ArrayList<BaseModel> Data;
	
//	#region public methods
	public ServiceResponse onError(ResponseCode code, String message, ArrayList<BaseModel> data) {
		return new ServiceResponse(false, code, message, data);
	}
	
	public ServiceResponse onSuccess(ArrayList<BaseModel> data) {
		return new ServiceResponse(true, ResponseCode.Success, "Success", data);
	}
//	#endregion public methods
	
//	#region contructor
	
	public ServiceResponse() {
		Success = true;
		Code = ResponseCode.Success;
		Message = "Success";
		Data = new ArrayList<BaseModel>();
	}
	
	public ServiceResponse(boolean success, ResponseCode code, String message, ArrayList<BaseModel> data) {
		super();
		Success = success;
		Code = code;
		Message = message;
		Data = data;
	}
	
//	#endregion contructor
	
//	#region getter and setter
	
	public boolean isSuccess() {
		return Success;
	}
	public void setSuccess(boolean success) {
		Success = success;
	}
	public ResponseCode getCode() {
		return Code;
	}
	public void setCode(ResponseCode code) {
		Code = code;
	}
	public String getMessage() {
		return Message;
	}
	public void setMessage(String message) {
		Message = message;
	}
	public ArrayList<BaseModel> getData() {
		return Data;
	}
	public void setData(ArrayList<BaseModel> data) {
		Data = data;
	}
//	#region getter and setter
}
