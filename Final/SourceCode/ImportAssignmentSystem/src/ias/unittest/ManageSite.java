package ias.unittest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.hamcrest.CoreMatchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;

import ias.common.ServiceResponse;
import ias.controller.InstockMerchandiseController;
import ias.controller.MerchandiseController;
import ias.controller.SiteController;
import ias.model.InstockMerchandise;
import ias.model.Merchandise;
import ias.model.Site;
import ias.repository.InstockMerchandiseRepository;
import ias.repository.MerchandiseRepository;
import ias.repository.SiteRepository;
import ias.view.sites.PopupInstockMerchandise;
import ias.view.sites.PopupSite;

public class ManageSite {
	@Rule
    public ErrorCollector collector = new ErrorCollector();
	
	private static SiteController siteController = new SiteController(new SiteRepository(new Site()));
	private static MerchandiseController merchandiseController = new MerchandiseController(
			new MerchandiseRepository(new Merchandise()));
	private static InstockMerchandiseController instockMerchandiseController = new InstockMerchandiseController(
			new InstockMerchandiseRepository(new InstockMerchandise()));
	
//	#region test site
	@Test
	public void testValidateSite() {
		PopupSite editWindow = new PopupSite(1, null);
		editWindow.siteId.setText("SITE0001");
		editWindow.siteName.setText("Site 1");
		editWindow.deliveryByAir.setText("5");
		editWindow.deliveryByShip.setText("5");
		collector.checkThat("Test validate popup site", editWindow.validateBeforeSave(), CoreMatchers.equalTo(true));
	}

	@Test
	public void testInsertSite() {
		Site site = new Site(null, "SITE0001", "Site 1", 7, 8);
		ServiceResponse response = siteController.insert(site);
		collector.checkThat("Test insert site", response.Success, CoreMatchers.equalTo(true));
	}

	@Test
	public void testEditSite() {
		Site site = new Site(null, "SITE0001", "Site test 1", 7, 8);
		ServiceResponse response = siteController.update("SITE0001", site);
		collector.checkThat("Test update site", response.Success, CoreMatchers.equalTo(true));
	}

	@Test
	public void testGetSite() {
		ServiceResponse response = siteController.getById("1");
		Site site = (Site)response.Data.get(0);
		collector.checkThat("Test get site by id", site.getDeliveryByAir(), CoreMatchers.equalTo(15));
	}

	@Test
	public void testDeleteSite() {
		ServiceResponse response = siteController.delete("SITE0001");
		collector.checkThat("Test delete site", response.Success, CoreMatchers.equalTo(true));
	}

//	#endregion test site
//	#region test instock merchandise
	@Test
	public void testValidateInstockMerchandise() {
		PopupInstockMerchandise editWindow = new PopupInstockMerchandise(1, null);
		editWindow.siteId.setSelectedIndex(0);
		editWindow.merchandiseId.setSelectedIndex(0);
		editWindow.instockQuantity.setText("10");
		collector.checkThat("Test validate popup instock merchandise", editWindow.validateBeforeSave(), CoreMatchers.equalTo(true));
	}

	@Test
	public void testSaveInstockMerchandise() {
		InstockMerchandise instockMerchandise = new InstockMerchandise("SITE0001", "MER001", 7);
		ServiceResponse response = instockMerchandiseController.insert(instockMerchandise);
		collector.checkThat("Test insert instock merchandise", response.Success, CoreMatchers.equalTo(true));
	}

	@Test
	public void testEditInstockMerchandise() {
//		InstockMerchandise instockMerchandise = new InstockMerchandise("SITE0001", "MER001", 17);
//		ServiceResponse response = instockMerchandiseController.update("", instockMerchandise);
//		collector.checkThat("Test update instock merchandise", response.Success, CoreMatchers.equalTo(true));
	}

	@Test
	public void testGetInstockMerchandise() {
//		ServiceResponse response = instockMerchandiseController.getById("SITE0001");
//		InstockMerchandise instockMerchandise = (InstockMerchandise)response.Data.get(0);
//		collector.checkThat("Test get instock merchandise by id", instockMerchandise.getInstockQuantity(), CoreMatchers.equalTo(17));
	}

	@Test
	public void testDeleteInstockMerchandise() {
		ServiceResponse response = instockMerchandiseController.delete("SITE0001");
		collector.checkThat("Test delete instock merchandise", response.Success, CoreMatchers.equalTo(true));
	}
//	#endregion test instock merchandise
}
