package ias.view.sites;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import ias.common.ServiceResponse;
import ias.controller.BaseController;
import ias.controller.InstockMerchandiseController;
import ias.controller.MerchandiseController;
import ias.controller.SiteController;
import ias.model.InstockMerchandise;
import ias.model.Merchandise;
import ias.model.Site;
import ias.repository.InstockMerchandiseRepository;
import ias.repository.MerchandiseRepository;
import ias.repository.SiteRepository;
import ias.view.LoginPage;
//import javafx.scene.control.Tab;

public class SitePage {
	private static SiteController siteController = new SiteController(new SiteRepository(new Site()));
	private static MerchandiseController merchandiseController = new MerchandiseController(
			new MerchandiseRepository(new Merchandise()));
	private static InstockMerchandiseController instockMerchandiseController = new InstockMerchandiseController(
			new InstockMerchandiseRepository(new InstockMerchandise()));
	public JFrame frame;
	JButton btnSideButton1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SitePage window = new SitePage();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SitePage() {
		initialize();
	}

	/**
	 * handle event click button edit
	 * 
	 * @author NCThanh
	 */
	private void onClickBtnEdit(TableSection tableSection) {
		DefaultTableModel model;
		int tabIndex = tableSection.sitePane.getSelectedIndex();
		// If in site tab
		if (tabIndex == 0) {
			PopupSite editWindow = new PopupSite(1, tableSection.siteTab);
			model = (DefaultTableModel) tableSection.siteTab.getModel();
			int selectedRowIndex = tableSection.siteTab.getSelectedRow();
			if (selectedRowIndex != -1) {
				editWindow.siteId.setText(model.getValueAt(selectedRowIndex, 0).toString());
				editWindow.siteName.setText(model.getValueAt(selectedRowIndex, 1).toString());
				editWindow.deliveryByShip.setText(model.getValueAt(selectedRowIndex, 2).toString());
				editWindow.deliveryByAir.setText(model.getValueAt(selectedRowIndex, 3).toString());
				editWindow.frame.setVisible(true);
			} else {
				JOptionPane.showMessageDialog(null, "No row is selected!", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		// If in merchandise tab
		if (tabIndex == 1) {
			PopupMerchandise editWindow = new PopupMerchandise(1, tableSection.merchandiseTab);
			model = (DefaultTableModel) tableSection.merchandiseTab.getModel();
			int selectedRowIndex = tableSection.merchandiseTab.getSelectedRow();
			if (selectedRowIndex != -1) {
				editWindow.merchandiseId.setText(model.getValueAt(selectedRowIndex, 0).toString());
				editWindow.merchandiseName.setText(model.getValueAt(selectedRowIndex, 1).toString());
				editWindow.unit.setText(model.getValueAt(selectedRowIndex, 2).toString());
				editWindow.frame.setVisible(true);
			} else {
				JOptionPane.showMessageDialog(null, "No row is selected!", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		// If in instock merchandise tab
		if (tabIndex == 2) {
			PopupInstockMerchandise editWindow = new PopupInstockMerchandise(1, tableSection.instockMerchandiseTab);
			model = (DefaultTableModel) tableSection.instockMerchandiseTab.getModel();
			int selectedRowIndex = tableSection.instockMerchandiseTab.getSelectedRow();
			if (selectedRowIndex != -1) {
				ServiceResponse responseMerchandise = merchandiseController.getById(model.getValueAt(selectedRowIndex, 7).toString());
				if (responseMerchandise.Data.size() == 1) {
					Merchandise merchandise = (Merchandise)responseMerchandise.Data.get(0);
					editWindow.merchandiseId.getModel().setSelectedItem(merchandise);
				}
				editWindow.merchandiseName.setText(model.getValueAt(selectedRowIndex, 2).toString());
				ServiceResponse responseSite = siteController.getById(model.getValueAt(selectedRowIndex, 8).toString());
				if (responseSite.Data.size() == 1) {
					Site site = (Site)responseSite.Data.get(0);
					editWindow.siteId.getModel().setSelectedItem(site);
				}
				editWindow.siteName.setText(model.getValueAt(selectedRowIndex, 1).toString());
				editWindow.instockQuantity.setText(model.getValueAt(selectedRowIndex, 3).toString());
				editWindow.frame.setVisible(true);
			}
		}
	}

	/**
	 * handle event click button view
	 * 
	 * @author NCThanh
	 */
	private void onClickBtnView(TableSection tableSection) {
		DefaultTableModel model;
		int tabIndex = tableSection.sitePane.getSelectedIndex();
		// If in site tab
		if (tabIndex == 0) {
			PopupSite viewWindow = new PopupSite(2, tableSection.siteTab);
			model = (DefaultTableModel) tableSection.siteTab.getModel();
			int selectedRowIndex = tableSection.siteTab.getSelectedRow();
			if (selectedRowIndex != -1) {
				viewWindow.siteId.setText(model.getValueAt(selectedRowIndex, 0).toString());
				viewWindow.siteName.setText(model.getValueAt(selectedRowIndex, 1).toString());
				viewWindow.deliveryByShip.setText(model.getValueAt(selectedRowIndex, 2).toString());
				viewWindow.deliveryByAir.setText(model.getValueAt(selectedRowIndex, 3).toString());
				viewWindow.frame.setVisible(true);
			} else {
				JOptionPane.showMessageDialog(null, "No row is selected!", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		// If in merchandise tab
		if (tabIndex == 1) {
			PopupMerchandise editWindow = new PopupMerchandise(2, tableSection.merchandiseTab);
			model = (DefaultTableModel) tableSection.merchandiseTab.getModel();
			int selectedRowIndex = tableSection.merchandiseTab.getSelectedRow();
			if (selectedRowIndex != -1) {
				editWindow.merchandiseId.setText(model.getValueAt(selectedRowIndex, 0).toString());
				editWindow.merchandiseName.setText(model.getValueAt(selectedRowIndex, 1).toString());
				editWindow.unit.setText(model.getValueAt(selectedRowIndex, 2).toString());
				editWindow.frame.setVisible(true);
			} else {
				JOptionPane.showMessageDialog(null, "No row is selected!", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		// If in instock merchandise tab
		if (tabIndex == 2) {
			PopupInstockMerchandise editWindow = new PopupInstockMerchandise(2, tableSection.instockMerchandiseTab);
			model = (DefaultTableModel) tableSection.instockMerchandiseTab.getModel();
			int selectedRowIndex = tableSection.instockMerchandiseTab.getSelectedRow();
			if (selectedRowIndex != -1) {
				ServiceResponse responseMerchandise = merchandiseController.getById(model.getValueAt(selectedRowIndex, 7).toString());
				if (responseMerchandise.Data.size() == 1) {
					Merchandise merchandise = (Merchandise)responseMerchandise.Data.get(0);
					editWindow.merchandiseId.getModel().setSelectedItem(merchandise);
				}
				editWindow.merchandiseName.setText(model.getValueAt(selectedRowIndex, 2).toString());
				ServiceResponse responseSite = siteController.getById(model.getValueAt(selectedRowIndex, 8).toString());
				if (responseSite.Data.size() == 1) {
					Site site = (Site)responseSite.Data.get(0);
					editWindow.siteId.getModel().setSelectedItem(site);
				}
				editWindow.siteName.setText(model.getValueAt(selectedRowIndex, 1).toString());
				editWindow.instockQuantity.setText(model.getValueAt(selectedRowIndex, 3).toString());
				editWindow.frame.setVisible(true);
			}
		}
	}

	/**
	 * handle event click button add
	 * 
	 * @author NCThanh
	 */
	private void onClickBtnAdd(TableSection tableSection) {
		int tabIndex = tableSection.sitePane.getSelectedIndex();
		if (tabIndex == 0) {
			PopupSite addWindow = new PopupSite(0, tableSection.siteTab);
			addWindow.frame.setVisible(true);
		}
		// If in merchandise tab
		if (tabIndex == 1) {
			PopupMerchandise addWindow = new PopupMerchandise(0, tableSection.merchandiseTab);
			addWindow.frame.setVisible(true);
		}
		// If in instock merchandise tab
		if (tabIndex == 2) {
			PopupInstockMerchandise addWindow = new PopupInstockMerchandise(0, tableSection.instockMerchandiseTab);
			addWindow.frame.setVisible(true);
		}
	}

	/**
	 * handle event click button delete
	 * 
	 * @author NCThanh
	 */
	private void onClickBtnDelete(TableSection tableSection) {
		int tabIndex = tableSection.sitePane.getSelectedIndex();
		BaseController controller = new BaseController(null);
		JTable table = null;
		if (tabIndex == 0) {
			controller = siteController;
			table = tableSection.siteTab;
		}
		if (tabIndex == 1) {
			controller = merchandiseController;
			table = tableSection.merchandiseTab;
		}
		if (tabIndex == 2) {
			controller = instockMerchandiseController;
			table = tableSection.instockMerchandiseTab;
		}

		DefaultTableModel model = (DefaultTableModel) table.getModel();
		int selectedRowIndex = table.getSelectedRow();
		if (selectedRowIndex != -1) {
			int answer = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete this?",
					"Confirmation message", JOptionPane.ERROR_MESSAGE);
			if (answer == 0) {
				String id = model.getValueAt(selectedRowIndex, 0).toString();
				ServiceResponse response = controller.delete(id);
				if (response.Success) {
//					JOptionPane.showMessageDialog(null, "Success!", "Success", JOptionPane.PLAIN_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(null, response.Message, "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		} else {
			// not choose any row
			JOptionPane.showMessageDialog(null, "No row is selected!", "Error", JOptionPane.ERROR_MESSAGE);
		}
		refresh(tableSection);
	}

	/**
	 * Delete a record in database
	 * 
	 * @author NCThanh
	 */
	private void handleActionDelete(TableSection tableSection) {

	}

	protected void refresh(TableSection tableSection) {
		int tabIndex = tableSection.sitePane.getSelectedIndex();
		if (tabIndex == 0) {
			ServiceResponse responseSite = siteController.getAll();
			Object[][] listSite = new Object[responseSite.Data.size()][];
			for (int i = 0; i < responseSite.Data.size(); i++) {
				Site site = (Site) responseSite.Data.get(i);
				listSite[i] = new Object[] { site.getSiteId(), site.getSiteName(), site.getDeliveryByShip(),
						site.getDeliveryByAir() };
			}
			tableSection.siteTab.setModel(new DefaultTableModel(listSite,
					new String[] { "Site id", "Site code", "Delivery by ship", "Delivery by air" }) {
				@Override
				public boolean isCellEditable(int row, int column) {
					// all cells false
					return false;
				}
			});
		}
		// If in merchandise tab
		if (tabIndex == 1) {
			ServiceResponse responseMerchandise = merchandiseController.getAll();
			Object[][] listMerchandise = new Object[responseMerchandise.Data.size()][];
			for (int i = 0; i < responseMerchandise.Data.size(); i++) {
				Merchandise merchandise = (Merchandise) responseMerchandise.Data.get(i);
				listMerchandise[i] = new Object[] { merchandise.getMerchandiseId(), merchandise.getMerchandiseName(),
						merchandise.getUnit() };
			}
			tableSection.merchandiseTab.setModel(new DefaultTableModel(listMerchandise,
					new String[] { "Merchandise id", "Merchandise name", "Unit" }) {
				@Override
				public boolean isCellEditable(int row, int column) {
					// all cells false
					return false;
				}
			});
		}
		// If in instock merchandise tab
		if (tabIndex == 2) {
			ServiceResponse responseInstockMerchandise = instockMerchandiseController.getByView("view_supply_site");
			Object[][] listInstockMerchandise = new Object[responseInstockMerchandise.Data.size()][];
			for (int i = 0; i < responseInstockMerchandise.Data.size(); i++) {
				InstockMerchandise instockMerchandise = (InstockMerchandise) responseInstockMerchandise.Data.get(i);
				listInstockMerchandise[i] = new Object[] { instockMerchandise.getSupplyId(),
						instockMerchandise.getSiteName(), instockMerchandise.getMerchandiseName(),
						instockMerchandise.getInstockQuantity(), instockMerchandise.getUnit(),
						instockMerchandise.getDeliveryByShip(), instockMerchandise.getDeliveryByAir(),
						instockMerchandise.getSiteId(), instockMerchandise.getMerchandiseId() };
			}
			JTable table = tableSection.instockMerchandiseTab;
			table.setModel(new DefaultTableModel(listInstockMerchandise,
					new String[] { "Supply id", "Site name", "Merchandise name", "In-stock quantity", "Unit",
							"Delivery by ship", "Delivery by air", "33", "333" }) {
				@Override
				public boolean isCellEditable(int row, int column) {
					// all cells false
					return false;
				}
			});
			table.removeColumn(table.getColumnModel().getColumn(0));
			table.removeColumn(table.getColumnModel().getColumn(7));
			table.removeColumn(table.getColumnModel().getColumn(6));
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */

	private void initialize() {
//		#region init
		frame = new JFrame("Import assignment system");
		frame.setBounds(100, 100, 950, 544);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setBackground(Color.BLUE);
		TableSection tableSection = new TableSection();
		tableSection.initialize();
		frame.getContentPane().add(tableSection.layerPane);
//		#endregion init

//		#region Add - Edit - Delete - View
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onClickBtnAdd(tableSection);
			}
		});
		btnAdd.setBounds(289, 407, 97, 25);
		frame.getContentPane().add(btnAdd);

		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onClickBtnDelete(tableSection);
			}
		});
		btnDelete.setBounds(427, 407, 97, 25);
		frame.getContentPane().add(btnDelete);

		JButton btnEdit = new JButton("Edit");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				onClickBtnEdit(tableSection);
			}
		});
		btnEdit.setBounds(557, 407, 97, 25);
		frame.getContentPane().add(btnEdit);

		JButton btnView = new JButton("View");
		btnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				onClickBtnView(tableSection);
			}
		});
		btnView.setBounds(688, 406, 97, 25);
		frame.getContentPane().add(btnView);
//		#endregion Add - Edit - Delete - View

		JButton btnSideButton1 = new JButton("Site");
		btnSideButton1.setBounds(12, 105, 113, 25);
		frame.getContentPane().add(btnSideButton1);

		JButton btnSettings = new JButton("Settings");
		btnSettings.setBounds(12, 281, 97, 25);
		frame.getContentPane().add(btnSettings);

		JButton btnAbout = new JButton("About");
		btnAbout.setBounds(12, 319, 97, 25);
		frame.getContentPane().add(btnAbout);

		JButton btnLogout = new JButton("Logout");
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				LoginPage loginPage = new LoginPage();
				loginPage.main(null);
			}
		});
		btnLogout.setBounds(12, 357, 97, 25);
		frame.getContentPane().add(btnLogout);

		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (JOptionPane.showConfirmDialog(frame, "Confirm if you want to exit", "Login system",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_NO_OPTION) {
					System.exit(0);
				}
			}
		});
		btnExit.setBounds(12, 395, 97, 25);
		frame.getContentPane().add(btnExit);

		btnSideButton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tableSection.switchLayeredPanels(tableSection.layerPane, tableSection.sitePane);
			}
		});
	}

}
