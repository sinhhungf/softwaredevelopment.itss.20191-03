package ias.view.sites;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import ias.common.ServiceResponse;
import ias.controller.InstockMerchandiseController;
import ias.controller.MerchandiseController;
import ias.controller.SiteController;
import ias.model.InstockMerchandise;
import ias.model.Merchandise;
import ias.model.Site;
import ias.repository.InstockMerchandiseRepository;
import ias.repository.MerchandiseRepository;
import ias.repository.SiteRepository;

public class SiteTable {
	public static SiteController siteController = new SiteController(new SiteRepository(new Site()));
	public static MerchandiseController merchandiseController = new MerchandiseController(
			new MerchandiseRepository(new Merchandise()));
	public static InstockMerchandiseController instockMerchandiseController = new InstockMerchandiseController(
			new InstockMerchandiseRepository(new InstockMerchandise()));

	JTabbedPane sitePane;
	JPanel sitePanel;
	JPanel merchandisePanel;
	JPanel instockMerchandisePanel;
	private JTextField searchFieldSite;
	JTable siteTab;
	private JTextField searchFieldMerchandise;
	JTable merchandiseTab;
	private JTextField searchFieldInstockMerchandise;
	JTable instockMerchandiseTab;

	public SiteTable(JTabbedPane importOrderPane, JPanel sitePanel, JPanel merchandisePanel, JPanel placeOrderPanel,
			JTextField searchFieldSite, JTable siteTab, JTextField searchFieldMerchandise, JTable merchandiseTab,
			JTextField searchFieldPlaceOrder, JTable placeOrderTab) {
		super();
		this.sitePane = importOrderPane;
		this.sitePanel = sitePanel;
		this.merchandisePanel = merchandisePanel;
		this.instockMerchandisePanel = placeOrderPanel;
		this.searchFieldSite = searchFieldSite;
		this.siteTab = siteTab;
		this.searchFieldMerchandise = searchFieldMerchandise;
		this.merchandiseTab = merchandiseTab;
		this.searchFieldInstockMerchandise = searchFieldPlaceOrder;
		this.instockMerchandiseTab = placeOrderTab;
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	public void initialize() {
		sitePane.setBounds(30, 0, 734, 350);

		sitePanel.setLayout(null);
		sitePane.addTab("Site", null, sitePanel, null);

		searchFieldSite.setColumns(10);
		searchFieldSite.setBounds(613, 0, 116, 22);
		sitePanel.add(searchFieldSite);

		JLabel label_2 = new JLabel("Search");
		label_2.setBounds(566, 3, 46, 16);
		sitePanel.add(label_2);

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(0, 26, 729, 280);
		sitePanel.add(scrollPane_2);

		// get all site
		ServiceResponse responseSite = siteController.getAll();
		Object[][] listSite = new Object[responseSite.Data.size()][];
		for (int i = 0; i < responseSite.Data.size(); i++) {
			Site site = (Site) responseSite.Data.get(i);
			listSite[i] = new Object[] { site.getSiteId(), site.getSiteName(), site.getDeliveryByShip(),
					site.getDeliveryByAir() };
		}
		siteTab.setModel(new DefaultTableModel(listSite,
				new String[] { "Site id", "Site code", "Delivery by ship", "Delivery by air" }) {
			@Override
			public boolean isCellEditable(int row, int column) {
				// all cells false
				return false;
			}
		});
		scrollPane_2.setViewportView(siteTab);

		merchandisePanel.setLayout(null);
		sitePane.addTab("Merchandise", null, merchandisePanel, null);

		searchFieldMerchandise.setColumns(10);
		searchFieldMerchandise.setBounds(613, 0, 116, 22);
		merchandisePanel.add(searchFieldMerchandise);

		JLabel label_3 = new JLabel("Search");
		label_3.setBounds(566, 3, 46, 16);
		merchandisePanel.add(label_3);

		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(0, 26, 729, 280);
		merchandisePanel.add(scrollPane_3);

		// get all merchandise
		ServiceResponse responseMerchandise = merchandiseController.getAll();
		Object[][] listMerchandise = new Object[responseMerchandise.Data.size()][];
		for (int i = 0; i < responseMerchandise.Data.size(); i++) {
			Merchandise merchandise = (Merchandise) responseMerchandise.Data.get(i);
			listMerchandise[i] = new Object[] { merchandise.getMerchandiseId(), merchandise.getMerchandiseName(),
					merchandise.getUnit() };
		}
		merchandiseTab.setModel(
				new DefaultTableModel(listMerchandise, new String[] { "Merchandise id", "Merchandise name", "Unit" }) {
					@Override
					public boolean isCellEditable(int row, int column) {
						// all cells false
						return false;
					}
				});
		scrollPane_3.setViewportView(merchandiseTab);

		instockMerchandisePanel.setLayout(null);
		sitePane.addTab("Instock Merchandise", null, instockMerchandisePanel, null);

		searchFieldInstockMerchandise.setColumns(10);
		searchFieldInstockMerchandise.setBounds(613, 0, 116, 22);
		instockMerchandisePanel.add(searchFieldInstockMerchandise);

		JLabel label_4 = new JLabel("Search");
		label_4.setBounds(566, 3, 46, 16);
		instockMerchandisePanel.add(label_4);

		JScrollPane scrollPane_4 = new JScrollPane();
		scrollPane_4.setBounds(0, 26, 729, 267);
		instockMerchandisePanel.add(scrollPane_4);

		// get all instock merchandise
		ServiceResponse responseInstockMerchandise = instockMerchandiseController.getByView("view_supply_site");
		Object[][] listInstockMerchandise = new Object[responseInstockMerchandise.Data.size()][];
		for (int i = 0; i < responseInstockMerchandise.Data.size(); i++) {
			InstockMerchandise instockMerchandise = (InstockMerchandise) responseInstockMerchandise.Data.get(i);
			listInstockMerchandise[i] = new Object[] { instockMerchandise.getSupplyId(),
					instockMerchandise.getSiteName(), instockMerchandise.getMerchandiseName(),
					instockMerchandise.getInstockQuantity(), instockMerchandise.getUnit(),
					instockMerchandise.getDeliveryByShip(), instockMerchandise.getDeliveryByAir(),
					instockMerchandise.getSiteId(), instockMerchandise.getMerchandiseId() };
		}
		instockMerchandiseTab.setModel(new DefaultTableModel(listInstockMerchandise, new String[] {"Supply id", "Site name",
				"Merchandise name", "In-stock quantity", "Unit", "Delivery by ship", "Delivery by air",
				"Site id", "Merchandise Id"}) {
			@Override
			public boolean isCellEditable(int row, int column) {
				// all cells false
				return false;
			}
		});	
		instockMerchandiseTab.removeColumn(instockMerchandiseTab.getColumnModel().getColumn(0));
		instockMerchandiseTab.removeColumn(instockMerchandiseTab.getColumnModel().getColumn(7));
		instockMerchandiseTab.removeColumn(instockMerchandiseTab.getColumnModel().getColumn(6));
		scrollPane_4.setViewportView(instockMerchandiseTab);

	}
}
