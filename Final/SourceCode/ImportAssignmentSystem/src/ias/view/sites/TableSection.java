package ias.view.sites;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class TableSection {
	JLayeredPane layerPane;
	JTabbedPane sitePane;
	JPanel sitePanel;
	JPanel merchandisePanel;
	JPanel placeOrderPanel;
	private JTextField searchFieldUnprocessed;
	JTable siteTab;
	private JTextField searchFieldProcessed;
	JTable merchandiseTab;
	private JTextField searchFieldArrived;
	JTable instockMerchandiseTab;

	SiteTable manageSiteTable;

	/**
	 * @wbp.parser.entryPoint
	 */
	public void initialize() {

		layerPane = new JLayeredPane();
		layerPane.setBounds(128, 40, 792, 350);
		layerPane.setLayout(null);

		sitePane = new JTabbedPane(JTabbedPane.TOP);
		sitePanel = new JPanel();
		merchandisePanel = new JPanel();
		placeOrderPanel = new JPanel();
		searchFieldUnprocessed = new JTextField();
		siteTab = new JTable();
		searchFieldProcessed = new JTextField();
		merchandiseTab = new JTable();
		searchFieldArrived = new JTextField();
		instockMerchandiseTab = new JTable();


		manageSiteTable = new SiteTable(sitePane, sitePanel, merchandisePanel,
				placeOrderPanel, searchFieldUnprocessed, siteTab, searchFieldProcessed, merchandiseTab,
				searchFieldArrived, instockMerchandiseTab);
		manageSiteTable.initialize();
		layerPane.add(sitePane);
		layerPane.setLayer(sitePane, 1);


		switchLayeredPanels(layerPane, sitePane);
	}

	public void switchLayeredPanels(JLayeredPane layeredPane, JTabbedPane tabbedPane) {
		layeredPane.removeAll();
		layeredPane.add(tabbedPane);
	}
}
