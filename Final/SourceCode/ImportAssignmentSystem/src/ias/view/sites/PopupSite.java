package ias.view.sites;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import ias.common.ResponseCode;
import ias.common.ServiceResponse;
import ias.controller.SiteController;
import ias.model.Site;
import ias.repository.SiteRepository;
import ias.view.BasePopup;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Date;
import java.awt.event.ActionEvent;

public class PopupSite extends BasePopup {
	private static SiteController siteController = new SiteController(new SiteRepository(new Site()));
	
	public JTextField siteId;
	public JTextField siteName;
	public JTextField deliveryByShip;
	public JTextField deliveryByAir;
	
	/**
	 * Create the application.
	 */

	public PopupSite(int state, JTable table) {
		super();
		this.state = state;
		this.table = table;
		initialize();
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PopupSite window = new PopupSite(0, null);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	protected String getModelName() {
		return "site";
	}
	
	protected void refresh() {
		ServiceResponse responseSite = siteController.getAll();
		Object[][] listSite = new Object[responseSite.Data.size()][];
		for (int i = 0; i < responseSite.Data.size(); i++) {
			Site site = (Site) responseSite.Data.get(i);
			if (this.state == 0) {
				listSite[i] = new Object[] { site.getSiteId(), site.getSiteName(), 
						site.getDeliveryByShip(),
						site.getDeliveryByAir() };
			} else {
				listSite[i] = new Object[] { site.getSiteId(), site.getSiteName(),
						site.getDeliveryByAir(),
						site.getDeliveryByShip()
						};
			}
		}
		table.setModel(new DefaultTableModel(listSite,
				new String[] { "Site id", "Site name", "Delivery by ship", "Delivery by air" }) {
			@Override
			public boolean isCellEditable(int row, int column) {
				// all cells false
				return false;
			}
		});
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public void initialize() {
		frame = new JFrame(getPopupName(state));
		frame.setBounds(100, 100, 446, 420);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblSiteId = new JLabel("Site Id");
		lblSiteId.setBounds(70, 63, 126, 21);
		frame.getContentPane().add(lblSiteId);

		JLabel lblSiteName = new JLabel("Site Name");
		lblSiteName.setBounds(70, 116, 126, 21);
		frame.getContentPane().add(lblSiteName);

		JLabel lblDeliveryByShip = new JLabel("Delivery By Ship");
		lblDeliveryByShip.setBounds(70, 183, 113, 16);
		frame.getContentPane().add(lblDeliveryByShip);

		JLabel lblDeliveryByAir = new JLabel("Delivery By Air");
		lblDeliveryByAir.setBounds(70, 256, 167, 21);
		frame.getContentPane().add(lblDeliveryByAir);

		siteId = new JTextField();
		siteId.setBounds(231, 62, 116, 22);
		frame.getContentPane().add(siteId);
		siteId.setColumns(10);

		siteName = new JTextField();
		siteName.setColumns(10);
		siteName.setBounds(231, 115, 116, 22);
		frame.getContentPane().add(siteName);

		deliveryByShip = new JTextField();
		deliveryByShip.setColumns(10);
		deliveryByShip.setBounds(231, 180, 116, 22);
		frame.getContentPane().add(deliveryByShip);

		deliveryByAir = new JTextField();
		deliveryByAir.setColumns(10);
		deliveryByAir.setBounds(231, 256, 116, 22);
		frame.getContentPane().add(deliveryByAir);

		// If mode is View
		if (state == 2) {
			siteId.setEditable(false);
			siteName.setEditable(false);
			deliveryByShip.setEditable(false);
			deliveryByAir.setEditable(false);
		}

		JButton btnAccept = new JButton("Accept");
		btnAccept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (validateBeforeSave()) {
					Site newSite = new Site(new Date(), siteId.getText(), siteName.getText(),
							Integer.valueOf(deliveryByShip.getText()), Integer.valueOf(deliveryByAir.getText()));
					switch (state) {
					case 0: // mode add
						handleActionAdd(frame, newSite, siteController);
						refresh();
						break;
					case 1: // mode edit
						handleActionEdit(frame, newSite, siteController);
						refresh();
						break;
					default: // mode delete - view
						frame.setVisible(false);
						break;
					}
					
				}
			}
		});
		btnAccept.setBounds(99, 324, 97, 25);
		frame.getContentPane().add(btnAccept);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				onClickBtnCancel();
			}
		});
		btnCancel.setBounds(231, 324, 97, 25);
		frame.getContentPane().add(btnCancel);
	}
	
	/**
	 * validate data before save
	 * 
	 * @author NCThanh
	 */
	@Override
	public boolean validateBeforeSave() {
		if (siteId.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Site id can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (siteName.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Site name can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (deliveryByAir.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Delivery by air can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (deliveryByShip.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Delivery by ship can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		int deliveryByAirVal = 0,
				deliveryByShipVal = 0;
		try {
			deliveryByAirVal = Integer.parseInt(deliveryByAir.getText());
			deliveryByShipVal = Integer.parseInt(deliveryByShip.getText());
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(null, "Delivery by ship or delivery by air is not valid", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (deliveryByAirVal < 0) {
			JOptionPane.showMessageDialog(null, "Delivery by air can not be negative!", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (deliveryByShipVal < 0) {
			JOptionPane.showMessageDialog(null, "Delivery by ship can not be negative!", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}
}
