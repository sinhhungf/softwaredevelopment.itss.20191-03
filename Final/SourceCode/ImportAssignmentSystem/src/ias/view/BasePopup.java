package ias.view;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import ias.common.ResponseCode;
import ias.common.ServiceResponse;
import ias.controller.BaseController;
import ias.model.BaseModel;

public abstract class BasePopup {
	// 0 = add, 1 = edit, 2 = view
	protected int state;
	public JFrame frame;
	protected JTable table;
	
	protected String getModelName() {
		return "";
	}
	
	protected String getPopupName(int state) {
		if (state == 0)
			return "Add new " + getModelName();
		else if (state == 1)
			return "Edit " + getModelName();
		else
			return "View " + getModelName();
	}
	
	/**
	 * event click button Accept
	 * 
	 * @author NCThanh
	 */
	protected void onClickBtnAccept(BaseModel model) {
		
	}
	
	/**
	 * event click button Cancel
	 * 
	 * @author NCThanh
	 */
	protected void onClickBtnCancel() {
		frame.setVisible(false);
	}
		
	/**
	 * validate data before save
	 * 
	 * @author NCThanh
	 */
	protected boolean validateBeforeSave() {
		return true;
	}
	
	/**
	 * Insert a record in database
	 * 
	 * @author NCThanh
	 */
	public boolean handleActionAdd(JFrame frame, BaseModel model, BaseController controller) {
		ServiceResponse response = controller.insert(model);
		if (response.Success) {
			frame.setVisible(false);
//			JOptionPane.showMessageDialog(null, "Success!", "Success", JOptionPane.PLAIN_MESSAGE);
		} else {
			if (response.Code == ResponseCode.Duplicate) {
				JOptionPane.showMessageDialog(null, getDuplicatedField(model) + " id is duplicated", "Error", JOptionPane.ERROR_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(null, response.Message, "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		return response.Success;
	}
	
	protected String getDuplicatedField(BaseModel model) {
		return model.getClass().getName().replace("ias.model.", "");
	}

	/**
	 * Edit a record in database
	 * 
	 * @author NCThanh
	 */
	public boolean handleActionEdit(JFrame frame, BaseModel model, BaseController controller) {
		ServiceResponse response = controller.update(model.getPrimaryKeyFieldValue(), model);
		if (response.Success) {
			frame.setVisible(false);
//			JOptionPane.showMessageDialog(null, "Success!", "Success", JOptionPane.PLAIN_MESSAGE);
		} else {
			if (response.Code == ResponseCode.Duplicate) {
				JOptionPane.showMessageDialog(null, model.getPrimaryKeyFieldName() + " id is duplicated", "Error", JOptionPane.ERROR_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(null, response.Message, "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		return response.Success;
	}
}
