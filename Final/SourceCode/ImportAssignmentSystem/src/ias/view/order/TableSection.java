package ias.view.order;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import ias.view.PopupWindow;

public class TableSection {
	JLayeredPane layerPane;
	JTabbedPane importOrderPane;
	JPanel unprocessedOrderPanel;
	JPanel processedOrderPanel;
	JPanel placeOrderPanel;
	private JTextField searchFieldUnprocessed;
	JTable unprocessedOrderTab;
	private JTextField searchFieldProcessed;
	JTable processedOrderTab;
	private JTextField searchFieldPlaceOrder;
	JTable placeOrderTab;

	ManageOrderTable manageImportOrderTable;
	private JButton btnOrderNow;

	/**
	 * @wbp.parser.entryPoint
	 */
	public void initialize() {

		layerPane = new JLayeredPane();
		layerPane.setBounds(128, 40, 792, 350);
		layerPane.setLayout(null);


		importOrderPane = new JTabbedPane(JTabbedPane.TOP);
		unprocessedOrderPanel = new JPanel();
		processedOrderPanel = new JPanel();
		placeOrderPanel = new JPanel();
		searchFieldUnprocessed = new JTextField();
		unprocessedOrderTab = new JTable();
		searchFieldProcessed = new JTextField();
		processedOrderTab = new JTable();
		searchFieldPlaceOrder = new JTextField();
		placeOrderTab = new JTable();

		manageImportOrderTable = new ManageOrderTable(importOrderPane, unprocessedOrderPanel, processedOrderPanel,
				placeOrderPanel, searchFieldUnprocessed, unprocessedOrderTab, searchFieldProcessed, processedOrderTab,
				searchFieldPlaceOrder, placeOrderTab);
		manageImportOrderTable.initialize();
		layerPane.add(importOrderPane);
		layerPane.setLayer(importOrderPane, 1);

		btnOrderNow = new JButton("Order now");
		btnOrderNow.setBounds(596, 297, 97, 25);
		placeOrderPanel.add(btnOrderNow);

		switchLayeredPanels(layerPane, importOrderPane);

		btnOrderNow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel model = (DefaultTableModel) placeOrderTab.getModel();
				int selectedRowIndex = placeOrderTab.getSelectedRow();
				PopupPlaceOrder addWindow = new PopupPlaceOrder(0, placeOrderTab, unprocessedOrderTab);
				if (selectedRowIndex != -1) {
					addWindow.merchID.setText(model.getValueAt(selectedRowIndex, 8).toString());
					addWindow.quantityOrdered.setText(model.getValueAt(selectedRowIndex, 3).toString());
					addWindow.siteID.setText(model.getValueAt(selectedRowIndex, 7).toString());
					addWindow.quantityReturned.setText("0");
					addWindow.deliveryMeans.setText("Ship");
					addWindow.frame.setVisible(true);					
				}
				else {
					JOptionPane.showMessageDialog(null, "No row is selected!", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
	}

	public void switchLayeredPanels(JLayeredPane layeredPane, JTabbedPane tabbedPane) {
		layeredPane.removeAll();
		layeredPane.add(tabbedPane);
	}

}
