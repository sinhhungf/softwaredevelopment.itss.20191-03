package ias.view.order;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import ias.common.ServiceResponse;
import ias.controller.BaseController;
import ias.controller.InstockMerchandiseController;
import ias.controller.MerchandiseController;
import ias.controller.OrderController;
import ias.controller.SiteController;
import ias.model.InstockMerchandise;
import ias.model.Merchandise;
import ias.model.Order;
import ias.model.Site;
import ias.repository.InstockMerchandiseRepository;
import ias.repository.MerchandiseRepository;
import ias.repository.OrderRepository;
import ias.repository.SiteRepository;
import ias.view.LoginPage;
import ias.view.PopupWindow;
import ias.view.order.TableSection;
import ias.view.order.PopupPlaceOrder;
import ias.view.order.PopupProcessedOrder;
import ias.view.order.PopupUnprocessedOrder;

import java.awt.Color;

public class OrderPage {
	private static OrderController orderController = new OrderController(new OrderRepository(new Order()));
	private static InstockMerchandiseController instockMerchandiseController = new InstockMerchandiseController(
			new InstockMerchandiseRepository(new InstockMerchandise()));

	public JFrame frame;
	JButton btnSideButton1;
	JButton btnSideButton2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OrderPage window = new OrderPage();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public OrderPage() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	protected void refresh(TableSection tableSection) {
		int tabIndex = tableSection.importOrderPane.getSelectedIndex();
		if (tabIndex == 0) {
			int j = 0;
			ServiceResponse responseOrder = orderController.getAll();
			Object[][] listUnprocessedOrder = new Object[responseOrder.Data.size()][];
			for (int i = 0; i < responseOrder.Data.size(); i++) {
				Order order = (Order) responseOrder.Data.get(i);
				if (order.getReturnQuantity() == 0 || order.getReturnQuantity() < order.getOrderQuantity()) {
					listUnprocessedOrder[j] = new Object[] { order.getEmployeeCode(), order.getMerchandiseCode(),
							order.getSiteCode(), order.getOrderQuantity(), order.getDeliveryMean(),
							order.getReturnQuantity(), order.getState(), order.getModifiedDate(), order.getOrderId() };
					j++;
				}
			}
			tableSection.unprocessedOrderTab.setModel(new DefaultTableModel(listUnprocessedOrder,
					new String[] { "Employee ID", "Merchandise ID", "Site code", "Quantity ordered", "Delivery means",
							"Quantity returned", "State", "Modified Date", "Order ID" }) {
				@Override
				public boolean isCellEditable(int row, int column) {
					// all cells false
					return false;
				}
			});
		}
		// If in processed tab
		if (tabIndex == 1) {
			ServiceResponse responseOrder = orderController.getAll();
			Object[][] listProcessedOrder = new Object[responseOrder.Data.size()][];

			int k = 0;
			for (int i = 0; i < responseOrder.Data.size(); i++) {
				Order order = (Order) responseOrder.Data.get(i);
				if (order.getReturnQuantity() >= order.getOrderQuantity()) {
					listProcessedOrder[k] = new Object[] { order.getEmployeeCode(), order.getMerchandiseCode(),
							order.getSiteCode(), order.getOrderQuantity(), order.getDeliveryMean(),
							order.getReturnQuantity(), order.getState(), order.getModifiedDate(), order.getOrderId() };
					k++;
				}
			}
			tableSection.processedOrderTab.setModel(new DefaultTableModel(listProcessedOrder,
					new String[] { "Employee ID", "Merchandise ID", "Site code", "Quantity ordered", "Delivery means",
							"Quantity returned", "State", "Modified Date", "Order ID" }));

		}
		// If in place order tab
		if (tabIndex == 2) {
			ServiceResponse responseInstockMerchandise = instockMerchandiseController.getByView("view_supply_site");
			Object[][] listInstockMerchandise = new Object[responseInstockMerchandise.Data.size()][];
			for (int i = 0; i < responseInstockMerchandise.Data.size(); i++) {
				InstockMerchandise instockMerchandise = (InstockMerchandise) responseInstockMerchandise.Data.get(i);
				listInstockMerchandise[i] = new Object[] { instockMerchandise.getSiteName(),
						instockMerchandise.getMerchandiseName(), instockMerchandise.getInstockQuantity(),
						instockMerchandise.getDeliveryByShip(), instockMerchandise.getDeliveryByAir() };
				tableSection.placeOrderTab.setModel(new DefaultTableModel(listInstockMerchandise, new String[] {
						"Site name", "Merchandise name", "In-stock quantity", "Delivery by ship", "Delivery by air" }));

			}
		}
	}

	private void initialize() {
		frame = new JFrame("Import assignment system");
		frame.setBounds(100, 100, 950, 544);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setBackground(Color.BLUE);

		JButton btnSideButton1 = new JButton("Import Order");
		btnSideButton1.setBounds(12, 105, 113, 25);
		frame.getContentPane().add(btnSideButton1);

		JButton btnSettings = new JButton("Settings");
		btnSettings.setBounds(12, 281, 97, 25);
		frame.getContentPane().add(btnSettings);

		JButton btnAbout = new JButton("About");
		btnAbout.setBounds(12, 319, 97, 25);
		frame.getContentPane().add(btnAbout);

		JButton btnLogout = new JButton("Logout");
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				LoginPage loginPage = new LoginPage();
				loginPage.main(null);
			}
		});
		btnLogout.setBounds(12, 357, 97, 25);
		frame.getContentPane().add(btnLogout);

		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (JOptionPane.showConfirmDialog(frame, "Confirm if you want to exit", "Login system",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_NO_OPTION) {
					System.exit(0);
				}
			}
		});
		btnExit.setBounds(12, 395, 97, 25);
		frame.getContentPane().add(btnExit);

		JButton btnAdd = new JButton("Add");
		btnAdd.setBounds(289, 407, 97, 25);
		frame.getContentPane().add(btnAdd);

		JButton btnDelete = new JButton("Delete");
		btnDelete.setBounds(427, 407, 97, 25);
		frame.getContentPane().add(btnDelete);

		JButton btnEdit = new JButton("Edit");
		btnEdit.setBounds(557, 407, 97, 25);
		frame.getContentPane().add(btnEdit);

		JButton btnView = new JButton("View");
		btnView.setBounds(688, 406, 97, 25);
		frame.getContentPane().add(btnView);

		TableSection tableSection = new TableSection();
		tableSection.initialize();
		frame.getContentPane().add(tableSection.layerPane);

		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int tabIndex = tableSection.importOrderPane.getSelectedIndex();
				refresh(tableSection);
				if (tabIndex == 0) {
					PopupUnprocessedOrder addWindow = new PopupUnprocessedOrder(0, tableSection.unprocessedOrderTab);
					addWindow.frame.setVisible(true);
				}
				// If in processed tab
				if (tabIndex == 1) {
					PopupProcessedOrder addWindow = new PopupProcessedOrder(0, tableSection.processedOrderTab);
					addWindow.frame.setVisible(true);
				}
				// If in place order tab
				if (tabIndex == 2) {
				}
			}
		});

		btnSideButton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tableSection.switchLayeredPanels(tableSection.layerPane, tableSection.importOrderPane);
			}
		});

		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int tabIndex = tableSection.importOrderPane.getSelectedIndex();
				BaseController controller = new BaseController(null);
				JTable table = null;
				if (tabIndex == 0) {
					controller = orderController;
					table = tableSection.unprocessedOrderTab;
				}
				if (tabIndex == 1) {
					controller = orderController;
					table = tableSection.processedOrderTab;
				}

				if (tabIndex != 2) {
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					int selectedRowIndex = table.getSelectedRow();
					if (selectedRowIndex != -1) {
						int answer = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete this?",
								"Confirmation message", JOptionPane.ERROR_MESSAGE);
						if (answer == 0) {
							String id = model.getValueAt(selectedRowIndex, 8).toString();
							ServiceResponse response = controller.delete(id);
							if (response.Success) {
								JOptionPane.showMessageDialog(null, "Success!", "Success", JOptionPane.PLAIN_MESSAGE);
								refresh(tableSection);
							} else {
								JOptionPane.showMessageDialog(null, response.Message, "Error",
										JOptionPane.ERROR_MESSAGE);
							}
						}
					} else {
						// not choose any row
						JOptionPane.showMessageDialog(null, "No row is selected!", "Error", JOptionPane.ERROR_MESSAGE);
					}
				} else {
				}
			}
		});

		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel model;
				// If current layer is Import Order pane
				if (tableSection.layerPane.highestLayer() == 1) {
					// If in unprocessed tab
					if (tableSection.importOrderPane.getSelectedIndex() == 0) {
						PopupUnprocessedOrder editWindow = new PopupUnprocessedOrder(1,
								tableSection.unprocessedOrderTab);
						model = (DefaultTableModel) tableSection.unprocessedOrderTab.getModel();
						int selectedRowIndex = tableSection.unprocessedOrderTab.getSelectedRow();
						if (selectedRowIndex != -1) {
							editWindow.employeeID.setText(model.getValueAt(selectedRowIndex, 0).toString());
							editWindow.merchID.setText(model.getValueAt(selectedRowIndex, 1).toString());
							editWindow.siteID.setText(model.getValueAt(selectedRowIndex, 2).toString());
							editWindow.quantityOrdered.setText(model.getValueAt(selectedRowIndex, 3).toString());
							editWindow.deliveryMeans.setText(model.getValueAt(selectedRowIndex, 4).toString());
							editWindow.quantityReturned.setText(model.getValueAt(selectedRowIndex, 5).toString());
							editWindow.dateChooser.setDate((Date) model.getValueAt(selectedRowIndex, 7));
							editWindow.frame.setVisible(true);
						} else
							JOptionPane.showMessageDialog(null, "No row is selected!", "Error",
									JOptionPane.ERROR_MESSAGE);

					}
					// If in processed tab
					if (tableSection.importOrderPane.getSelectedIndex() == 1) {
						PopupProcessedOrder editWindow = new PopupProcessedOrder(1, tableSection.processedOrderTab);

						model = (DefaultTableModel) tableSection.processedOrderTab.getModel();
						int selectedRowIndex = tableSection.processedOrderTab.getSelectedRow();

						if (selectedRowIndex != -1) {
							editWindow.employeeID.setText(model.getValueAt(selectedRowIndex, 0).toString());
							editWindow.merchID.setText(model.getValueAt(selectedRowIndex, 1).toString());
							editWindow.siteID.setText(model.getValueAt(selectedRowIndex, 2).toString());
							editWindow.quantityOrdered.setText(model.getValueAt(selectedRowIndex, 3).toString());
							editWindow.deliveryMeans.setText(model.getValueAt(selectedRowIndex, 4).toString());
							editWindow.quantityReturned.setText(model.getValueAt(selectedRowIndex, 5).toString());
							editWindow.dateChooser.setDate((Date) model.getValueAt(selectedRowIndex, 7));
							editWindow.frame.setVisible(true);
						} else
							JOptionPane.showMessageDialog(null, "No row is selected!", "Error",
									JOptionPane.ERROR_MESSAGE);

					}
				}
			}
		});

		btnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel model;
				// If current layer is Import Order pane
				if (tableSection.layerPane.highestLayer() == 1) {
					// If in unprocessed tab
					if (tableSection.importOrderPane.getSelectedIndex() == 0) {
						model = (DefaultTableModel) tableSection.unprocessedOrderTab.getModel();
						PopupUnprocessedOrder viewWindow = new PopupUnprocessedOrder(2,
								tableSection.unprocessedOrderTab);
						int selectedRowIndex = tableSection.unprocessedOrderTab.getSelectedRow();

						if (selectedRowIndex != -1) {
							viewWindow.employeeID.setText(model.getValueAt(selectedRowIndex, 0).toString());
							viewWindow.merchID.setText(model.getValueAt(selectedRowIndex, 1).toString());
							viewWindow.siteID.setText(model.getValueAt(selectedRowIndex, 2).toString());
							viewWindow.quantityOrdered.setText(model.getValueAt(selectedRowIndex, 3).toString());
							viewWindow.deliveryMeans.setText(model.getValueAt(selectedRowIndex, 4).toString());
							viewWindow.quantityReturned.setText(model.getValueAt(selectedRowIndex, 5).toString());
							viewWindow.dateChooser.setDate((Date) model.getValueAt(selectedRowIndex, 7));
							viewWindow.frame.setVisible(true);
						} else
							JOptionPane.showMessageDialog(null, "No row is selected!", "Error",
									JOptionPane.ERROR_MESSAGE);

					}
					// If in processed tab
					if (tableSection.importOrderPane.getSelectedIndex() == 1) {
						PopupProcessedOrder viewWindow = new PopupProcessedOrder(2, tableSection.unprocessedOrderTab);
						model = (DefaultTableModel) tableSection.processedOrderTab.getModel();
						int selectedRowIndex = tableSection.processedOrderTab.getSelectedRow();

						if (selectedRowIndex != -1) {
							viewWindow.employeeID.setText(model.getValueAt(selectedRowIndex, 0).toString());
							viewWindow.merchID.setText(model.getValueAt(selectedRowIndex, 1).toString());
							viewWindow.siteID.setText(model.getValueAt(selectedRowIndex, 2).toString());
							viewWindow.quantityOrdered.setText(model.getValueAt(selectedRowIndex, 3).toString());
							viewWindow.deliveryMeans.setText(model.getValueAt(selectedRowIndex, 4).toString());
							viewWindow.quantityReturned.setText(model.getValueAt(selectedRowIndex, 5).toString());
							viewWindow.dateChooser.setDate((Date) model.getValueAt(selectedRowIndex, 7));
							viewWindow.frame.setVisible(true);
						} else
							JOptionPane.showMessageDialog(null, "No row is selected!", "Error",
									JOptionPane.ERROR_MESSAGE);

					}

					if (tableSection.importOrderPane.getSelectedIndex() == 2) {
//						model = (DefaultTableModel) tableSection.placeOrderTab.getModel();
//						int selectedRowIndex = tableSection.placeOrderTab.getSelectedRow();
//
//						if (selectedRowIndex != -1) {
//							PopupPlaceOrder viewWindow = new PopupPlaceOrder(2, tableSection.placeOrderTab, tableSection.unprocessedOrderTab);
//							viewWindow.frame.setVisible(true);
//						}
					}

				}
			}
		});

	}
}
