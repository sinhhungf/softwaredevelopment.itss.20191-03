package ias.view.order;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import ias.common.ServiceResponse;
import ias.controller.InstockMerchandiseController;
import ias.controller.MerchandiseController;
import ias.controller.OrderController;
import ias.controller.SiteController;
import ias.model.InstockMerchandise;
import ias.model.Merchandise;
import ias.model.Order;
import ias.model.Site;
import ias.repository.InstockMerchandiseRepository;
import ias.repository.MerchandiseRepository;
import ias.repository.OrderRepository;
import ias.repository.SiteRepository;

public class ManageOrderTable {
	public static OrderController orderController = new OrderController(new OrderRepository(new Order()));
	private static InstockMerchandiseController instockMerchandiseController = new InstockMerchandiseController(
			new InstockMerchandiseRepository(new InstockMerchandise()));

	JTabbedPane importOrderPane;
	JPanel unprocessedOrderPanel;
	JPanel processedOrderPanel;
	JPanel placeOrderPanel;
	private JTextField searchFieldUnprocessed;
	JTable unprocessedOrderTab;
	private JTextField searchFieldProcessed;
	JTable processedOrderTab;
	private JTextField searchFieldPlaceOrder;
	JTable placeOrderTab;

	public ManageOrderTable(JTabbedPane importOrderPane, JPanel unprocessedOrderPanel, JPanel processedOrderPanel,
			JPanel placeOrderPanel, JTextField searchFieldUnprocessed, JTable unprocessedOrderTab,
			JTextField searchFieldProcessed, JTable processedOrderTab, JTextField searchFieldPlaceOrder,
			JTable placeOrderTab) {
		super();
		this.importOrderPane = importOrderPane;
		this.unprocessedOrderPanel = unprocessedOrderPanel;
		this.processedOrderPanel = processedOrderPanel;
		this.placeOrderPanel = placeOrderPanel;
		this.searchFieldUnprocessed = searchFieldUnprocessed;
		this.unprocessedOrderTab = unprocessedOrderTab;
		this.searchFieldProcessed = searchFieldProcessed;
		this.processedOrderTab = processedOrderTab;
		this.searchFieldPlaceOrder = searchFieldPlaceOrder;
		this.placeOrderTab = placeOrderTab;
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	public void initialize() {
		importOrderPane.setBounds(30, 0, 734, 350);

		unprocessedOrderPanel.setLayout(null);
		importOrderPane.addTab("Unprocessed Import Order", null, unprocessedOrderPanel, null);

		searchFieldUnprocessed.setColumns(10);
		searchFieldUnprocessed.setBounds(613, 0, 116, 22);
		unprocessedOrderPanel.add(searchFieldUnprocessed);

		JLabel label_2 = new JLabel("Search");
		label_2.setBounds(566, 3, 46, 16);
		unprocessedOrderPanel.add(label_2);

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(0, 26, 729, 280);
		unprocessedOrderPanel.add(scrollPane_2);

		ServiceResponse responseOrder = orderController.getAll();
		Object[][] listUnprocessedOrder = new Object[responseOrder.Data.size()][];
		int j = 0;
		for (int i = 0; i < responseOrder.Data.size(); i++) {
			Order order = (Order) responseOrder.Data.get(i);
			if (order.getReturnQuantity() == 0 || order.getReturnQuantity() < order.getOrderQuantity()) {
				listUnprocessedOrder[j] = new Object[] { order.getEmployeeCode(), order.getMerchandiseCode(),
						order.getSiteCode(), order.getOrderQuantity(), order.getDeliveryMean(),
						order.getReturnQuantity(), order.getState(), order.getModifiedDate(), order.getOrderId() };
				j++;
			}
		}

		unprocessedOrderTab.setModel(new DefaultTableModel(listUnprocessedOrder,
				new String[] { "Employee ID", "Merchandise ID", "Site code", "Quantity ordered", "Delivery means",
						"Quantity returned", "State", "Modified Date", "Order ID" }));
		scrollPane_2.setViewportView(unprocessedOrderTab);

		processedOrderPanel.setLayout(null);
		importOrderPane.addTab("Processed Import Order", null, processedOrderPanel, null);

		searchFieldProcessed.setColumns(10);
		searchFieldProcessed.setBounds(613, 0, 116, 22);
		processedOrderPanel.add(searchFieldProcessed);

		JLabel label_3 = new JLabel("Search");
		label_3.setBounds(566, 3, 46, 16);
		processedOrderPanel.add(label_3);

		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(0, 26, 729, 280);
		processedOrderPanel.add(scrollPane_3);

		Object[][] listProcessedOrder = new Object[responseOrder.Data.size()][];

		int k = 0;
		for (int i = 0; i < responseOrder.Data.size(); i++) {
			Order order = (Order) responseOrder.Data.get(i);
			if (order.getReturnQuantity() >= order.getOrderQuantity()) {
				listProcessedOrder[k] = new Object[] { order.getEmployeeCode(), order.getMerchandiseCode(),
						order.getSiteCode(), order.getOrderQuantity(), order.getDeliveryMean(),
						order.getReturnQuantity(), order.getState(), order.getModifiedDate(), order.getOrderId() };
				k++;
			}
		}

		processedOrderTab.setModel(new DefaultTableModel(listProcessedOrder,
				new String[] { "Employee ID", "Merchandise ID", "Site code", "Quantity ordered", "Delivery means",
						"Quantity returned", "State", "Modified Date", "Order ID" }));
		scrollPane_3.setViewportView(processedOrderTab);

		placeOrderPanel.setLayout(null);
		importOrderPane.addTab("Place Order", null, placeOrderPanel, null);

		searchFieldPlaceOrder.setColumns(10);
		searchFieldPlaceOrder.setBounds(613, 0, 116, 22);
		placeOrderPanel.add(searchFieldPlaceOrder);

		JLabel label_4 = new JLabel("Search");
		label_4.setBounds(566, 3, 46, 16);
		placeOrderPanel.add(label_4);

		JScrollPane scrollPane_4 = new JScrollPane();
		scrollPane_4.setBounds(0, 26, 729, 267);
		placeOrderPanel.add(scrollPane_4);

		ServiceResponse responseInstockMerchandise = instockMerchandiseController.getByView("view_supply_site");
		Object[][] listInstockMerchandise = new Object[responseInstockMerchandise.Data.size()][];
		for (int i = 0; i < responseInstockMerchandise.Data.size(); i++) {
			InstockMerchandise instockMerchandise = (InstockMerchandise) responseInstockMerchandise.Data.get(i);
			listInstockMerchandise[i] = new Object[] { instockMerchandise.getSupplyId(),
					instockMerchandise.getSiteName(), instockMerchandise.getMerchandiseName(),
					instockMerchandise.getInstockQuantity(), instockMerchandise.getUnit(),
					instockMerchandise.getDeliveryByShip(), instockMerchandise.getDeliveryByAir(),
					instockMerchandise.getSiteId(), instockMerchandise.getMerchandiseId() };
		}
		placeOrderTab.setModel(new DefaultTableModel(listInstockMerchandise,
				new String[] { "Supply id", "Site name", "Merchandise name", "In-stock quantity", "Unit",
						"Delivery by ship", "Delivery by air", "Site ID", "Merchandise ID" }));
		scrollPane_4.setViewportView(placeOrderTab);

		placeOrderTab.removeColumn(placeOrderTab.getColumnModel().getColumn(0));
		placeOrderTab.removeColumn(placeOrderTab.getColumnModel().getColumn(3));
		placeOrderTab.removeColumn(placeOrderTab.getColumnModel().getColumn(5));
		placeOrderTab.removeColumn(placeOrderTab.getColumnModel().getColumn(5));

	}
}
