package ias.view.request;

import ias.common.ServiceResponse;
import ias.controller.MerchandiseController;
import ias.controller.RequestController;
import ias.model.Merchandise;
import ias.model.Request;
import ias.repository.MerchandiseRepository;
import ias.repository.RequestRepository;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

class RequestTable {
    private JTabbedPane requestPane;
    private JPanel requestPanel, merchandisePanel;
    private JTable requestTab, merchandiseTab;

    private static RequestController requestController = new RequestController( new RequestRepository( new Request() ) );
    private static MerchandiseController merchandiseController = new MerchandiseController(new MerchandiseRepository(new Merchandise()));

    RequestTable(JTabbedPane requestPane, JPanel requestPanel, JPanel merchandisePanel,
                 JTable requestTab,
                 JTable merchandiseTab) {
        super();

        this.requestPane = requestPane;
        this.requestTab = requestTab;
        this.merchandiseTab = merchandiseTab;
        this.requestPanel = requestPanel;
        this.merchandisePanel = merchandisePanel;

        initialize();
    }

    private void initialize(){
        requestPane.setBounds(30, 0, 734, 350);

        requestPanel.setLayout(null);
        requestPane.addTab("Customer Requests", null, requestPanel, null);

        JScrollPane scrollPane_2 = new JScrollPane();
        scrollPane_2.setBounds(0, 26, 729, 280);
        requestPanel.add(scrollPane_2);

        // Get all unprocessed request
        ServiceResponse responseRequest = requestController.getAll();

        Object[][] listSite = new Object[responseRequest.Data.size()][];
        for (int i = 0; i < responseRequest.Data.size(); i++) {
            Request site = (Request) responseRequest.Data.get(i);
            listSite[i] = new Object[] {
                    site.getRequestID(),
                    site.getMerchandiseID(),
                    site.getEmployeeID(),
                    site.getCustomerID(),
                    site.getDate(),
                    site.getQuantity()
            };
        }
        requestTab.setModel(new DefaultTableModel(listSite,
                new String[] { "ID", "Merchandise ID", "Employee ID", "Customer ID", "Desire Date", "Quantity" }) {
            @Override
            public boolean isCellEditable(int row, int column) {
                // all cells false
                return false;
            }
        });
        scrollPane_2.setViewportView(requestTab);

        merchandisePanel.setLayout(null);
        requestPane.addTab("Merchandise", null, merchandisePanel, null);

        JScrollPane scrollPane_3 = new JScrollPane();
        scrollPane_3.setBounds(0, 26, 729, 280);
        merchandisePanel.add(scrollPane_3);

        // get all merchandise
        ServiceResponse responseMerchandise = merchandiseController.getAll();
        Object[][] listMerchandise = new Object[responseMerchandise.Data.size()][];
        for (int i = 0; i < responseMerchandise.Data.size(); i++) {
            Merchandise merchandise = (Merchandise) responseMerchandise.Data.get(i);
            listMerchandise[i] = new Object[] { merchandise.getMerchandiseId(), merchandise.getMerchandiseName(),
                    merchandise.getUnit() };
        }
        merchandiseTab.setModel(
                new DefaultTableModel(listMerchandise, new String[] { "Merchandise ID", "Merchandise Name", "Unit" }) {
                    @Override
                    public boolean isCellEditable(int row, int column) {
                        // all cells false
                        return false;
                    }
                });

        scrollPane_3.setViewportView(merchandiseTab);
    }
}