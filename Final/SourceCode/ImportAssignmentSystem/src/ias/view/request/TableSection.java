package ias.view.request;

import javax.swing.*;

public class TableSection {
    JLayeredPane layerPane;
    JTabbedPane requestPane;
    JTable unprocessedRequestTab, processedRequestTab;

    TableSection() {
        initialize();
    }

    private void initialize() {
        layerPane = new JLayeredPane();
        layerPane.setBounds(128, 40, 792, 350);
        layerPane.setLayout(null);

        requestPane = new JTabbedPane(JTabbedPane.TOP);
        JPanel unprocessedRequest = new JPanel();
        JPanel processedRequest = new JPanel();

        unprocessedRequestTab = new JTable();
        processedRequestTab = new JTable();

        new RequestTable(requestPane,
                unprocessedRequest, processedRequest,
                unprocessedRequestTab,
                processedRequestTab);


        layerPane.add(requestPane);
        layerPane.setLayer(requestPane, 1);
        switchLayeredPanels(layerPane, requestPane);
    }

    private void switchLayeredPanels(JLayeredPane layeredPane, JTabbedPane tabbedPane) {
        layeredPane.removeAll();
        layeredPane.add(tabbedPane);
    }
}