package ias.view.request;

import com.toedter.calendar.JDateChooser;
import ias.common.ServiceResponse;
import ias.controller.CustomerController;
import ias.controller.EmployeeController;
import ias.controller.MerchandiseController;
import ias.controller.RequestController;
import ias.model.Customer;
import ias.model.Employee;
import ias.model.Merchandise;
import ias.model.Request;
import ias.repository.CustomerRepository;
import ias.repository.EmployeeRepository;
import ias.repository.MerchandiseRepository;
import ias.repository.RequestRepository;
import ias.view.BasePopup;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.Date;
import java.util.regex.Pattern;

public class PopUpRequest extends BasePopup {
    private static RequestController requestController = new RequestController(new RequestRepository(new Request()));
    private static MerchandiseController merchandiseController = new MerchandiseController(new MerchandiseRepository(new Merchandise()));
    private static EmployeeController employeeController = new EmployeeController(new EmployeeRepository(new Employee()));
    private static CustomerController customerController = new CustomerController(new CustomerRepository(new Customer()));

    JDateChooser dateChooser = new JDateChooser();
    JTextField requestID = new JTextField();
    JComboBox<Merchandise> merchandiseId = new JComboBox<>();
    JComboBox<Employee> employeeID = new JComboBox<>();
    JComboBox<Customer> customerID = new JComboBox<>();
    public JTextField quantity = new JTextField();

    /**
     * Create the application.
     */

    PopUpRequest(int state, JTable table) {
        super();
        this.state = state;
        this.table = table;
        initialize();
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
                try {
                    new PopUpRequest(0, null).frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
    }

    private void initialize() {
        frame = new JFrame(getPopupName(state));
        frame.setBounds(150, 100, 500, 420);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JLabel lblSiteId = new JLabel("Customer ID");
        lblSiteId.setBounds(70, 33, 126, 21);
        frame.getContentPane().add(lblSiteId);

        JLabel lblMerchandiseId = new JLabel("Merchandise ID");
        lblMerchandiseId.setBounds(70, 73, 126, 21);
        frame.getContentPane().add(lblMerchandiseId);

        JLabel lblMerchandiseName = new JLabel("Employee ID");
        lblMerchandiseName.setBounds(70, 113, 113, 16);
        frame.getContentPane().add(lblMerchandiseName);

        JLabel lblOrderQuan = new JLabel("Request Quantity");
        lblOrderQuan.setBounds(70, 153, 153, 21);
        frame.getContentPane().add(lblOrderQuan);

        JLabel lblReturnQuan = new JLabel("Desired Date");
        lblReturnQuan.setBounds(70, 193, 167, 21);
        frame.getContentPane().add(lblReturnQuan);

        JLabel lbl = new JLabel("Request ID");
        lbl.setBounds(70, 233, 167, 21);
        frame.getContentPane().add(lbl);


        customerID.setBounds(231, 32, 200, 22);
        frame.getContentPane().add(customerID);
        ServiceResponse responseCustomer = customerController.getAll();
        Customer[] listCustomer = new Customer[responseCustomer.Data.size()];
        for (int i = 0; i < responseCustomer.Data.size(); i++) {
            Customer merchandise = (Customer) responseCustomer.Data.get(i);
            listCustomer[i] = merchandise;
        }
        customerID.setModel(new DefaultComboBoxModel<>(listCustomer));

        merchandiseId.setBounds(231, 72, 200, 22);
        frame.getContentPane().add(merchandiseId);
        ServiceResponse responseMerchandise = merchandiseController.getAll();
        Merchandise[] listMerchandise = new Merchandise[responseMerchandise.Data.size()];
        for (int i = 0; i < responseMerchandise.Data.size(); i++) {
            Merchandise merchandise = (Merchandise) responseMerchandise.Data.get(i);
            listMerchandise[i] = merchandise;
        }
        merchandiseId.setModel(new DefaultComboBoxModel<>(listMerchandise));

        employeeID.setBounds(231, 112, 200, 22);
        frame.getContentPane().add(employeeID);
        ServiceResponse responseEmployee = employeeController.getAll();
        Employee[] listEmployee = new Employee[responseEmployee.Data.size()];
        for (int i = 0; i < responseEmployee.Data.size(); i++) {
            Employee employee = (Employee) responseEmployee.Data.get(i);
            listEmployee[i] = employee;
            System.out.println(listEmployee[i].getId());
        }

        employeeID.setModel(new DefaultComboBoxModel<>(listEmployee));

        quantity.setBounds(231, 152, 200, 22);
        frame.getContentPane().add(quantity);
        quantity.setColumns(10);


        dateChooser.setBounds(231, 192, 200, 22);
        dateChooser.setDateFormatString("yyyy-MM-dd");
        Date date = new Date();
        dateChooser.setDate(date);
        frame.getContentPane().add(dateChooser);

        requestID.setBounds(231, 232, 200, 22);
        frame.getContentPane().add(requestID);


        // If mode is View
        if (state == 2) {
            customerID.setEditable(false);
            employeeID.setEditable(false);
            merchandiseId.setEditable(false);
            quantity.setEditable(false);
//            dateChooser.setDateFormatString().setEditable(false);
            requestID.setEditable(false);
        }

        JButton btnAccept = new JButton("Accept");
        btnAccept.addActionListener( e -> {
                if (validateBeforeSave()) {
                    Request request = null;
                    try {
                        Merchandise newMerchandise = (Merchandise) merchandiseId.getSelectedItem();
                        Employee newEmployee = (Employee) employeeID.getSelectedItem();
                        Customer newCustomer = (Customer) customerID.getSelectedItem();
                        assert newMerchandise != null;
                        assert newEmployee != null;
                        assert newCustomer != null;
                        request = new Request(
                                new Date(),
                                requestID.getText(),
                                newMerchandise.getMerchandiseId(),
                                newEmployee.getId(),
                                newCustomer.getId(),
                                dateChooser.getDate(),
                                Integer.valueOf( quantity.getText() ));
                    } catch (NumberFormatException ex) {
                        ex.printStackTrace();
                    }
                    switch (state) {
                        case 0: // mode add
                            handleActionAdd(frame, request, requestController);
                            refresh();
                            break;
                        case 1: // mode edit
                            handleActionEdit(frame, request, requestController);
                            refresh();
                            break;
                        default: // mode delete - view
                            frame.setVisible(false);
                            break;
                    }
                }
        });
        btnAccept.setBounds(99, 324, 97, 25);
        frame.getContentPane().add(btnAccept);

        JButton btnCancel = new JButton("Cancel");
        btnCancel.addActionListener( e -> onClickBtnCancel() );
        btnCancel.setBounds(231, 324, 97, 25);
        frame.getContentPane().add(btnCancel);
    }

    private void refresh() {
        ServiceResponse responseMerchandise = requestController.getAll();
        Object[][] listMerchandise = new Object[responseMerchandise.Data.size()][];
        for (int i = 0; i < responseMerchandise.Data.size(); i++) {
            Request merchandise = (Request) responseMerchandise.Data.get(i);
            listMerchandise[i] = new Object[] {
                    merchandise.getRequestID(),
                    merchandise.getMerchandiseID(),
                    merchandise.getEmployeeID(),
                    merchandise.getCustomerID(),
                    merchandise.getDate(),
                    merchandise.getQuantity()
            };

        }
        table.setModel(new DefaultTableModel(listMerchandise,
                new String[] {"ID", "Merchandise ID", "Employee ID", "Customer ID", "Desired Date", "Quantity" }) {
            @Override
            public boolean isCellEditable(int row, int column) {
                // all cells false
                return false;
            }
        });
    }


    protected boolean validateBeforeSave() {
        if(requestID.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "ID can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
       if (dateChooser.getDate() == null) {
            JOptionPane.showMessageDialog(null, "Desired date can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if (quantity.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "quantity can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if (!isNumeric(quantity.getText())) {
            JOptionPane.showMessageDialog(null, "Quantity must be an integer", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if ( Integer.valueOf(quantity.getText()) < 0) {
            JOptionPane.showMessageDialog(null, "Quantity must be a POSITIVE integer", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    private Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");

    private boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        return pattern.matcher(strNum).matches();
    }
}