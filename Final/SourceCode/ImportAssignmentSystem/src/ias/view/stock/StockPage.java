package ias.view.stock;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import ias.common.ServiceResponse;
import ias.controller.BaseController;
import ias.controller.OrderController;
import ias.controller.OrderNewController;
import ias.model.Order;
import ias.model.OrderNew;
import ias.repository.OrderNewRepository;
import ias.repository.OrderRepository;
import ias.view.LoginPage;
import ias.view.PopupWindow;
import ias.view.order.PopupPlaceOrder;
import ias.view.order.PopupProcessedOrder;
import ias.view.order.PopupUnprocessedOrder;
import ias.view.sites.PopupSite;
import ias.view.stock.TableSection;
import java.awt.Color;

public class StockPage {
	private static OrderNewController orderController = new OrderNewController(new OrderNewRepository(new OrderNew()));
//	private static OrderController orderController = new OrderController(new OrderRepository(new Order()));

	public JFrame frame;
	JButton btnSideButton1;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StockPage window = new StockPage();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public StockPage() {
		initialize();
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	
	private void initialize() {
		frame = new JFrame("Import assignment system");
		frame.setBounds(100, 100, 950, 544);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setBackground(Color.BLUE);
		
		JButton btnSideButton1 = new JButton("Stock");
		btnSideButton1.setBounds(12, 105, 113, 25);
		frame.getContentPane().add(btnSideButton1);

		JButton btnSettings = new JButton("Settings");
		btnSettings.setBounds(12, 281, 97, 25);
		frame.getContentPane().add(btnSettings);

		JButton btnAbout = new JButton("About");
		btnAbout.setBounds(12, 319, 97, 25);
		frame.getContentPane().add(btnAbout);

		JButton btnLogout = new JButton("Logout");
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				LoginPage loginPage = new LoginPage();
				loginPage.main(null);
			}
		});
		btnLogout.setBounds(12, 357, 97, 25);
		frame.getContentPane().add(btnLogout);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (JOptionPane.showConfirmDialog(frame, "Confirm if you want to exit", "Login system",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_NO_OPTION) {
					System.exit(0);
				}
			}
		});
		btnExit.setBounds(12, 395, 97, 25);
		frame.getContentPane().add(btnExit);
		

		JButton btnEdit = new JButton("Edit");
		btnEdit.setBounds(557, 407, 97, 25);
		frame.getContentPane().add(btnEdit);

		JButton btnView = new JButton("View");
		btnView.setBounds(688, 406, 97, 25);
		frame.getContentPane().add(btnView);
		
		JButton btnComplete = new JButton("Mark as Complete");
		btnComplete.setBounds(427, 407, 97, 25);
		frame.getContentPane().add(btnComplete);
		
		TableSection tableSection = new TableSection();
		tableSection.initialize();
		frame.getContentPane().add(tableSection.layerPane);
		
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				onClickBtnEdit(tableSection);
			}
		});

		btnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				onClickBtnView(tableSection);
			}
		});

		
		btnComplete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onClickBtnComplete(tableSection);
			}
		});
	}

	private void onClickBtnEdit(TableSection tableSection) {
		PopUpStock editWindow = new PopUpStock(1, tableSection.stockTab);
		DefaultTableModel model;
		model = (DefaultTableModel) tableSection.stockTab.getModel();
		int selectedRowIndex = tableSection.stockTab.getSelectedRow();
		if (selectedRowIndex != -1) {
			editWindow.orderCode.setText(model.getValueAt(selectedRowIndex, 0).toString());
			editWindow.employeeCode.setText(model.getValueAt(selectedRowIndex, 1).toString());
			editWindow.merchCode.setText(model.getValueAt(selectedRowIndex, 2).toString());
			editWindow.siteCode.setText(model.getValueAt(selectedRowIndex, 3).toString());
			editWindow.quantityOrdered.setText(model.getValueAt(selectedRowIndex, 4).toString());
			editWindow.quantityReturned.setText(model.getValueAt(selectedRowIndex, 5).toString());
			editWindow.State.setText(model.getValueAt(selectedRowIndex, 7).toString());
			editWindow.deliveryMean.setText(model.getValueAt(selectedRowIndex, 6).toString());
			
			editWindow.frame.setVisible(true);
		} 
		else {
			JOptionPane.showMessageDialog(null, "No row is selected!", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void onClickBtnView(TableSection tableSection) {
		PopUpStock editWindow = new PopUpStock(2, tableSection.stockTab);
		DefaultTableModel model;
		model = (DefaultTableModel) tableSection.stockTab.getModel();
		int selectedRowIndex = tableSection.stockTab.getSelectedRow();
	
		if (selectedRowIndex != -1) {
			editWindow.orderCode.setText(model.getValueAt(selectedRowIndex, 0).toString());
			editWindow.employeeCode.setText(model.getValueAt(selectedRowIndex, 1).toString());
			editWindow.merchCode.setText(model.getValueAt(selectedRowIndex, 2).toString());
			editWindow.siteCode.setText(model.getValueAt(selectedRowIndex, 3).toString());
			editWindow.quantityOrdered.setText(model.getValueAt(selectedRowIndex, 4).toString());
			editWindow.quantityReturned.setText(model.getValueAt(selectedRowIndex, 5).toString());
			editWindow.State.setText(model.getValueAt(selectedRowIndex, 7).toString());
			editWindow.deliveryMean.setText(model.getValueAt(selectedRowIndex, 6).toString());
			
			editWindow.frame.setVisible(true);
		} 
		else {
			JOptionPane.showMessageDialog(null, "No row is selected!", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
		
	private void onClickBtnComplete(TableSection tableSection) {
		JTable table = tableSection.stockTab;
		DefaultTableModel model= (DefaultTableModel) table.getModel();
		int selectedRowIndex = table.getSelectedRow();
		int answer = JOptionPane.showConfirmDialog(null, "Are you sure you want to mark as complete?",
				"Confirmation message", JOptionPane.ERROR_MESSAGE);
		if (selectedRowIndex != -1) {
			int State_int = Integer.parseInt(model.getValueAt(selectedRowIndex, 7).toString());

			if (answer == 0) {
				if (State_int != 2) {
					JOptionPane.showMessageDialog(null, "The order is not complete", "Error", JOptionPane.ERROR_MESSAGE);
				} 
				else {
					String id = model.getValueAt(selectedRowIndex, 0).toString();
					ServiceResponse response = orderController.delete(id);
					if (response.Success) {
		//				JOptionPane.showMessageDialog(null, "Success!", "Success", JOptionPane.PLAIN_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(null, response.Message, "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		}
		else {
			// not choose any row

			JOptionPane.showMessageDialog(null, "No row is selected!", "Error", JOptionPane.ERROR_MESSAGE);

		}
		refresh(tableSection);
	}
	
	protected void refresh(TableSection tableSection) {
		ServiceResponse responseOrder = orderController.getAll();
		Object[][] listStock = new Object[responseOrder.Data.size()][];
		for (int i = 0; i < responseOrder.Data.size(); i++) {
			OrderNew order= (OrderNew) responseOrder.Data.get(i);
			listStock[i] = new Object[] {order.getOrderCode(), order.getEmployeeCode() ,order.getMerchandiseCode(), order.getSiteCode(), 
					order.getOrderQuantity(), order.getReturnQuantity(), order.getDeliveryMean(),  order.getState(), order.getModifiedDate() };
		}
		tableSection.stockTab.setModel(new DefaultTableModel(listStock,
				new String[] {"ID", "Employee ID", "Merchandise ID", "Site ID", "Quantity ordered",  "Quantity returned", "Delivery means" ,"State", "Modified Date"}) {
			@Override
			public boolean isCellEditable(int row, int column) {
				// all cells false
				return false;
			}
		});
	}
}