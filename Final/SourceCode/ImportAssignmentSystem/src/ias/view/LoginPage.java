package ias.view;

import javax.swing.*;

import ias.common.ServiceResponse;
import ias.controller.EmployeeController;
import ias.model.Employee;
import ias.repository.EmployeeRepository;
import ias.view.request.RequestPage;
import ias.view.sites.SitePage;
import ias.view.order.OrderPage;
import ias.view.stock.StockPage;
import java.awt.*;

public class LoginPage {
	private JFrame frame;
	private JTextField txtUsername;
	private JPasswordField txtPassword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
			try {
				LoginPage window = new LoginPage();
				window.frame.setVisible(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoginPage() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Import assignment system");
		frame.setBounds(100, 200, 500, 365);
		frame.getContentPane().setLayout(null);

		JLabel lblLoginSystem = new JLabel("Login System");
		lblLoginSystem.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblLoginSystem.setBounds(151, 13, 212, 37);
		frame.getContentPane().add(lblLoginSystem);

		frame.getContentPane().add(addLoginLabel("Username", 88));
		frame.getContentPane().add(addLoginLabel("Password", 137));
		frame.getContentPane().add(addLoginLabel("Role", 190));

		txtUsername = new JTextField();
		txtUsername.setBounds(236, 79, 116, 31);
		frame.getContentPane().add(txtUsername);
		txtUsername.setColumns(10);

		txtPassword = new JPasswordField();
		txtPassword.setBounds(236, 133, 116, 31);
		frame.getContentPane().add(txtPassword);

		JButton btnLogin = new JButton("Login");
		btnLogin.setBounds(108, 256, 97, 25);
		frame.getContentPane().add(btnLogin);

		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(e -> {
			frame = new JFrame("Exit");
			if (JOptionPane.showConfirmDialog(frame, "Confirm if you want to exit", "Login system",
					JOptionPane.YES_NO_OPTION) == JOptionPane.YES_NO_OPTION) {
				System.exit(0);
			}
		});
		btnExit.setBounds(286, 256, 97, 25);
		frame.getContentPane().add(btnExit);

		DefaultComboBoxModel<String> defaultComboBoxModel = new DefaultComboBoxModel<>(
				new String[]{"Order placement employee", "Sales employee", "Stock employee", "Site employee"});
		JComboBox<String> comboBox = new JComboBox<>(defaultComboBoxModel);
		comboBox.setBounds(236, 190, 182, 22);
		frame.getContentPane().add(comboBox);


		// authentication
		btnLogin.addActionListener(e -> {
					String password = String.valueOf(txtPassword.getPassword());
					String username = txtUsername.getText();

					ServiceResponse responseRequest = new EmployeeController(new EmployeeRepository(new Employee())).getAll();
					String selectedItemComboBox = (String) comboBox.getSelectedItem();

					int i = 0;
					for (; i < responseRequest.Data.size(); i++) {
						Employee site = (Employee) responseRequest.Data.get(i);

						assert selectedItemComboBox != null;
						if (password.equals(site.getPassword()) &&
								username.equals(site.getUser_name()) ) {
							if (selectedItemComboBox.equals("Order placement employee") && site.getRole().equals("1")) {
								OrderPage importOrderPage = new OrderPage();
								importOrderPage.frame.setVisible(true);
								frame.dispose();
								break;
							}
							if (selectedItemComboBox.equals("Sales employee") && site.getRole().equals("2")) {
								RequestPage salesOrderPage = new RequestPage();
								salesOrderPage.frame.setVisible(true);
								frame.dispose();
								break;
							}
							if (selectedItemComboBox.equals("Stock employee") && site.getRole().equals("3")) {
								StockPage stockPage = new StockPage();
								stockPage.frame.setVisible(true);
								frame.dispose();
								break;
							}
							if (selectedItemComboBox.equals("Site employee") && site.getRole().equals("4")) {
								SitePage sitePage = new SitePage();
								sitePage.frame.setVisible(true);
								frame.dispose();
								break;
							}
						}
					}

					txtPassword.setText(null);
					txtUsername.setText(null);
					if (i == responseRequest.Data.size())
						JOptionPane.showMessageDialog(null, "Invalid Login Details", "Login Error", JOptionPane.ERROR_MESSAGE);

				}
		);
	}

	private JLabel addLoginLabel(String name, int i1) {
		JLabel lblLoginSystem = new JLabel(name);
		lblLoginSystem.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblLoginSystem.setBounds(108, i1, 116, 16);

		return lblLoginSystem;
	}
}