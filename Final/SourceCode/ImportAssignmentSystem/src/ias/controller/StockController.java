package ias.controller;

import ias.model.Stock;
import ias.repository.BaseRepository;
import ias.repository.StockRepository;

public class StockController extends BaseController {
	private StockRepository stockRepository = new StockRepository(new Stock());
	
	public StockController(BaseRepository baseRepository) {
		super(baseRepository);
	}
}

