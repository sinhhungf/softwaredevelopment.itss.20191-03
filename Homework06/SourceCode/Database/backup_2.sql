--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1 (Debian 12.1-1.pgdg100+1)
-- Dumped by pg_dump version 12.1 (Debian 12.1-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: customer; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.customer (
    id character varying NOT NULL,
    customer_name character varying,
    customer_address character varying,
    modified_date timestamp(4) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.customer OWNER TO "user";

--
-- Name: employee; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.employee (
    id character varying NOT NULL,
    employee_name character varying NOT NULL,
    role character varying NOT NULL,
    user_name character varying(50),
    password character varying(20),
    modified_date timestamp(4) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.employee OWNER TO "user";

--
-- Name: merchandise; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.merchandise (
    id character varying NOT NULL,
    merchandise_name character varying(50) NOT NULL,
    unit character varying,
    modified_date timestamp(4) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.merchandise OWNER TO "user";

--
-- Name: order; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public."order" (
    employee_id character varying NOT NULL,
    merchandise_id character varying NOT NULL,
    site_id character varying NOT NULL,
    order_quantity integer NOT NULL,
    delivery_mean character varying NOT NULL,
    return_quantity integer,
    state smallint DEFAULT 0 NOT NULL,
    modified_date timestamp(4) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public."order" OWNER TO "user";

--
-- Name: order_new; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.order_new (
    employee_id character varying NOT NULL,
    merchandise_id character varying NOT NULL,
    site_id character varying NOT NULL,
    order_quantity integer NOT NULL,
    delivery_mean character varying NOT NULL,
    return_quantity integer,
    state smallint DEFAULT 0 NOT NULL,
    modified_date timestamp(4) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    id character varying NOT NULL
);


ALTER TABLE public.order_new OWNER TO "user";

--
-- Name: request; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.request (
    id character varying NOT NULL,
    merchandise_id character varying NOT NULL,
    employee_id character varying NOT NULL,
    customer_id character varying NOT NULL,
    desire_delivery_date timestamp(4) without time zone NOT NULL,
    request_quantity integer NOT NULL,
    modified_date timestamp(4) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.request OWNER TO "user";

--
-- Name: site; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.site (
    id character varying NOT NULL,
    site_name character varying(50) NOT NULL,
    deliver_by_ship smallint,
    deliver_by_air smallint,
    modified_date timestamp(4) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.site OWNER TO "user";

--
-- Name: supply; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.supply (
    site_id character varying NOT NULL,
    merchandise_id character varying NOT NULL,
    instock_quantity integer NOT NULL,
    modified_date timestamp(4) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    id character varying NOT NULL
);


ALTER TABLE public.supply OWNER TO "user";

--
-- Name: view_order_merchandise; Type: VIEW; Schema: public; Owner: user
--

CREATE VIEW public.view_order_merchandise AS
 SELECT o.site_id,
    o.merchandise_id,
    m.merchandise_name,
    o.order_quantity,
    o.return_quantity,
    o.delivery_mean,
    o.state,
    o.modified_date
   FROM (public."order" o
     LEFT JOIN public.merchandise m ON (((o.merchandise_id)::text = (m.id)::text)));


ALTER TABLE public.view_order_merchandise OWNER TO "user";

--
-- Name: view_supply_site; Type: VIEW; Schema: public; Owner: user
--

CREATE VIEW public.view_supply_site AS
 SELECT sl.merchandise_id,
    sl.instock_quantity,
    sl.modified_date,
    s.id AS site_id,
    s.site_name,
    s.deliver_by_ship,
    s.deliver_by_air,
    m.merchandise_name,
    m.unit,
    sl.id
   FROM ((public.supply sl
     LEFT JOIN public.merchandise m ON (((m.id)::text = (sl.merchandise_id)::text)))
     LEFT JOIN public.site s ON (((sl.site_id)::text = (s.id)::text)));


ALTER TABLE public.view_supply_site OWNER TO "user";

--
-- Data for Name: customer; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.customer (id, customer_name, customer_address, modified_date) FROM stdin;
11	Hoang Anh Tu	Binh Dinh	2019-12-04 00:00:00
1	Hoang Anh Ha	Ha Noi\n	2019-12-04 00:00:00
2	Dinh Anh Minh	Can Tho\n	2019-12-04 00:00:00
3	Vu Van Ha	Ho Chi Minh\n	2019-12-04 00:00:00
4	Le Anh Hung	Da Nang\n	2019-12-04 00:00:00
5	Le The Hung	Hai Phong	2019-12-04 00:00:00
6	Nguyen Anh Viet	Lao Cai	2019-12-04 00:00:00
7	Le The Ha	Ca Mau\n	2019-12-04 00:00:00
8	Tran Anh Nam	Hung Yen\n	2019-12-04 00:00:00
9	Hoang Anh Tu	Phu Tho\n	2019-12-04 00:00:00
10	Tran Anh Viet	Thai Binh	2019-12-04 00:00:00
\.


--
-- Data for Name: employee; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.employee (id, employee_name, role, user_name, password, modified_date) FROM stdin;
1	Hoang The Viet	1	vietht	123	2019-12-04 00:00:00
2	Nguyen Anh Minh	2	minhna	123	2019-12-04 00:00:00
3	Vu The Ha	3	havt	123	2019-12-04 00:00:00
4	Tran Anh Viet	1	vietta	123	2019-12-04 00:00:00
5	Hoang The Viet	2\n	vietht2	123	2019-12-04 00:00:00
6	Dinh Van Viet	2	vietdv	123	2019-12-04 00:00:00
7	Tran Anh Nam	3	namta	123	2019-12-04 00:00:00
8	Nguyen The Minh	1	minhnt	123	2019-12-04 00:00:00
9	Dinh Van Tu	2	tudv	123	2019-12-04 00:00:00
10	Vu The Nam	3	namvt	123	2019-12-04 00:00:00
\.


--
-- Data for Name: merchandise; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.merchandise (id, merchandise_name, unit, modified_date) FROM stdin;
1	Pan	\N	2019-12-04 00:00:00
2	Shoes	\N	2019-12-04 00:00:00
3	Shirt	\N	2019-12-04 00:00:00
4	Computer	\N	2019-12-04 00:00:00
5	Smartphone	\N	2019-12-04 00:00:00
6	Glasses	\N	2019-12-04 00:00:00
7	Flycam\n	\N	2019-12-04 00:00:00
8	Cup	\N	2019-12-04 00:00:00
9	Book	\N	2019-12-04 00:00:00
10	Paper Box	\N	2019-12-04 00:00:00
\.


--
-- Data for Name: order; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public."order" (employee_id, merchandise_id, site_id, order_quantity, delivery_mean, return_quantity, state, modified_date) FROM stdin;
3	1	1	7	Ship	\N	0	2019-12-04 00:00:00
3	2	2	8	Air	\N	0	2019-12-04 00:00:00
5	3	3	9	Ship	\N	0	2019-12-04 00:00:00
5	4	3	2	Air	\N	0	2019-12-04 00:00:00
1	1	1	1	Air	1	2	2019-12-04 00:00:00
1	2	1	5	Air	4	1	2019-12-04 00:00:00
\.


--
-- Data for Name: order_new; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.order_new (employee_id, merchandise_id, site_id, order_quantity, delivery_mean, return_quantity, state, modified_date, id) FROM stdin;
\.


--
-- Data for Name: request; Type: TABLE DATA; Schema: public; Owner: user
--

INSERT INTO public.request (id, merchandise_id, employee_id, customer_id, desire_delivery_date, request_quantity, modified_date)
VALUES 
  ('1' , '1' , '1' , '1' , '2019-12-31 00:00:00', '1', '2019-12-04 00:00:00'),
  ('2' , '2' , '1' , '1' , '2019-12-31 00:00:00', '2', '2019-12-04 00:00:00'),
  ('3' , '1' , '3' , '2' , '2019-12-31 00:00:00', '5', '2019-12-04 00:00:00'),
  ('4' , '2' , '3' , '2' , '2019-12-31 00:00:00', '2', '2019-12-04 00:00:00'),
  ('5' , '3' , '5' , '3' , '2019-12-31 00:00:00', '1', '2019-12-04 00:00:00'),
  ('6' , '4' , '5' , '3' , '2019-12-31 00:00:00', '6', '2019-12-04 00:00:00')
;

--
-- Data for Name: site; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.site (id, site_name, deliver_by_ship, deliver_by_air, modified_date) FROM stdin;
6	Routine	4	8	2019-12-15 20:22:40
5	Alibaba	4	9	2019-12-16 11:27:18
2	Tiki	18	5	2019-12-19 23:19:16
1	Amazon	15	7	2019-12-19 23:35:03
3	Shopee	10	14	2019-12-19 23:46:32
7	Alo	8	9	2019-12-19 23:46:39
\.


--
-- Data for Name: supply; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.supply (site_id, merchandise_id, instock_quantity, modified_date, id) FROM stdin;
1	1	50	2019-12-04 00:00:00	1
1	2	50	2019-12-04 00:00:00	2
1	3	50	2019-12-04 00:00:00	3
1	4	50	2019-12-04 00:00:00	4
1	5	50	2019-12-04 00:00:00	5
1	6	50	2019-12-04 00:00:00	6
1	7	50	2019-12-04 00:00:00	7
5	1	50	2019-12-19 16:35:50	446d7b6e-b3fe-4143-b361-e1671aca1373
\.


--
-- Name: customer customer_pk; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.customer
    ADD CONSTRAINT customer_pk PRIMARY KEY (id);


--
-- Name: employee employee_pk; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.employee
    ADD CONSTRAINT employee_pk PRIMARY KEY (id);


--
-- Name: merchandise merchandise_pk; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.merchandise
    ADD CONSTRAINT merchandise_pk PRIMARY KEY (id);


--
-- Name: order order_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public."order"
    ADD CONSTRAINT order_pkey PRIMARY KEY (employee_id, merchandise_id, site_id);


--
-- Name: order_new ordernew_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.order_new
    ADD CONSTRAINT ordernew_pkey PRIMARY KEY (id);


--
-- Name: supply pkey_supply; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.supply
    ADD CONSTRAINT pkey_supply PRIMARY KEY (id);


--
-- Name: request request_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.request
    ADD CONSTRAINT request_pkey PRIMARY KEY (id);


--
-- Name: site site_pk; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.site
    ADD CONSTRAINT site_pk PRIMARY KEY (id);


--
-- Name: order order_fk_employee; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public."order"
    ADD CONSTRAINT order_fk_employee FOREIGN KEY (employee_id) REFERENCES public.employee(id) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- Name: order order_fk_merchandise; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public."order"
    ADD CONSTRAINT order_fk_merchandise FOREIGN KEY (merchandise_id) REFERENCES public.merchandise(id) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- Name: order order_fk_site; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public."order"
    ADD CONSTRAINT order_fk_site FOREIGN KEY (site_id) REFERENCES public.site(id) NOT VALID;


--
-- Name: order_new ordernew_fk_employee; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.order_new
    ADD CONSTRAINT ordernew_fk_employee FOREIGN KEY (employee_id) REFERENCES public.employee(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: order_new ordernew_fk_merchandise; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.order_new
    ADD CONSTRAINT ordernew_fk_merchandise FOREIGN KEY (merchandise_id) REFERENCES public.merchandise(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: order_new ordernew_fk_site; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.order_new
    ADD CONSTRAINT ordernew_fk_site FOREIGN KEY (site_id) REFERENCES public.site(id);


--
-- Name: request request_fk_customer; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.request
    ADD CONSTRAINT request_fk_customer FOREIGN KEY (customer_id) REFERENCES public.customer(id) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- Name: request request_fk_employee; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.request
    ADD CONSTRAINT request_fk_employee FOREIGN KEY (employee_id) REFERENCES public.employee(id) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- Name: request request_fk_merchandise; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.request
    ADD CONSTRAINT request_fk_merchandise FOREIGN KEY (merchandise_id) REFERENCES public.merchandise(id) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- Name: supply supply_fk_merchandise; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.supply
    ADD CONSTRAINT supply_fk_merchandise FOREIGN KEY (merchandise_id) REFERENCES public.merchandise(id) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- Name: supply supply_fk_site; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.supply
    ADD CONSTRAINT supply_fk_site FOREIGN KEY (site_id) REFERENCES public.site(id) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: user
--

REVOKE ALL ON SCHEMA public FROM "user";
GRANT ALL ON SCHEMA public TO "user" WITH GRANT OPTION;


--
-- Name: TABLE supply; Type: ACL; Schema: public; Owner: user
--

GRANT ALL ON TABLE public.supply TO PUBLIC;


--
-- PostgreSQL database dump complete
--

