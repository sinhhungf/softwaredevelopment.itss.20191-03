- copy a file to docker container
docker cp backup.sql postgres_container:/

- restore a sql file
psql -U <username> -d <dbname> -1 -f <filename>.sql

psql -U user -d postgres -f backup.sql 

====================================================
- use another database
docker exec -it postgres_container psql -U user -d user

or 

\connect user

- drop database 'postgres'
drop database postgres;

=====================================================

- delete all container
docker rm $(docker ps -a -q) -f

- delete all volumes
docker volume prune

or 

docker volume ls
docker volume rm <name_of_volume>
