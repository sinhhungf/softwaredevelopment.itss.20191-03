### Documentation
- config: same as SiteController, SiteRepository, Site, InstockMerchandise
- executeCommand(sql): execute a insert, update, delete sql
- executeQuery(sql): execute a select sql

### Download jar postgresql
https://jdbc.postgresql.org/download.html
Choose PostgreSQL JDBC 4.2 Driver, 42.2.8

### Config db
https://examples.javacodegeeks.com/core-java/sql/java-jdbc-postgresql-connection-example/