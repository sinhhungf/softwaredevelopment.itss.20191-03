package ias.unittest;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.hamcrest.CoreMatchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;

import ias.common.ServiceResponse;
import ias.controller.OrderNewController;
import ias.model.OrderNew;
import ias.repository.OrderNewRepository;
import ias.repository.OrderRepository;
import ias.view.stock.PopUpStock;
import ias.controller.OrderNewController;


public class ManageStock {
	@Rule
	public ErrorCollector collector = new ErrorCollector();
	
	private static OrderNewController orderController = new OrderNewController(new OrderNewRepository(new OrderNew()));
	
	//#region test site
	@Test
	public void testValidateStock() {
		PopUpStock editWindow = new PopUpStock(2, null);
		editWindow.orderCode.setText("001");
		editWindow.employeeCode.setText("001");
		editWindow.merchCode.setText("001");
		editWindow.siteCode.setText("001");
		editWindow.deliveryMean.setText("Air");
		editWindow.quantityOrdered.setText("6");
		editWindow.quantityReturned.setText("5");
		editWindow.State.setText("Air");

		collector.checkThat("Test validate popup site", editWindow.validateBeforeSave(), CoreMatchers.equalTo(true));
	}
	
	@Test
	public void testEditSite() {
		OrderNew order= new OrderNew(null, "001", "001", "001", 7, "Air", 8, 0, "001");
		ServiceResponse response = orderController.update("001", order);
		collector.checkThat("Test update site", response.Success, CoreMatchers.equalTo(true));
	}

	@Test
	public void testGetSite() {
		ServiceResponse response = orderController.getById("5_3_3");
		OrderNew order = (OrderNew)response.Data.get(0);
		collector.checkThat("Test get site by id", order.getOrderQuantity(), CoreMatchers.equalTo(9));
		
	}

	@Test
	public void testDeleteSite() {
		ServiceResponse response = orderController.delete("5_4_3");
		collector.checkThat("Test delete site", response.Success, CoreMatchers.equalTo(true));
	}
}

