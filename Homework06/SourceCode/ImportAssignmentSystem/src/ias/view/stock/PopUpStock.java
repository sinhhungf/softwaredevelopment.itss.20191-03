package ias.view.stock;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;
//import org.jdatepicker.util.JDatePickerUtil;
//import com.toedter.calendar.JDateChooser;

import ias.common.ServiceResponse;
import ias.controller.OrderController;
import ias.controller.OrderNewController;
import ias.model.Merchandise;
import ias.model.Order;
import ias.model.OrderNew;
import ias.repository.OrderNewRepository;
import ias.repository.OrderRepository;
import ias.view.BasePopup;

public class PopUpStock extends BasePopup {
	public static OrderNewController orderController = new OrderNewController(new OrderNewRepository(new OrderNew()));
//	public static OrderController orderController = new OrderController(new OrderRepository(new Order()));

	
	public JFrame frame;
	public JTextField orderCode;
	public JTextField employeeCode;
	public JTextField merchCode;
	public JTextField siteCode;
	public JTextField quantityOrdered;
	public JTextField quantityReturned;
	public JTextField deliveryMean;
	public JTextField State;
	/**
	 * Create the application.
	 */

	public PopUpStock(int state, JTable table) {
		super();
		this.state = state;
		this.table = table;
		initialize();
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PopUpStock window = new PopUpStock(0, null);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	@Override
	protected String getPopupName(int state) {
		if (state == 0)
		return "Add new order";
		else if (state == 1)
			return "Edit order";
		else return "View order";
	}
	
	protected void refresh() {
		ServiceResponse responseOrder = orderController.getAll();
		Object[][] listStock = new Object[responseOrder.Data.size()][];
		for (int i = 0; i < responseOrder.Data.size(); i++) {
			OrderNew order= (OrderNew) responseOrder.Data.get(i);
			listStock[i] = new Object[] {order.getOrderCode(), order.getEmployeeCode() ,order.getMerchandiseCode(), order.getSiteCode(), 
					order.getOrderQuantity(), order.getReturnQuantity(), order.getDeliveryMean(),  order.getState(), order.getModifiedDate() };
		}
		table.setModel(new DefaultTableModel(listStock,
				new String[] {"ID", "Employee ID", "Merchandise ID", "Site ID", "Quantity ordered",  "Quantity returned", "Delivery means" ,"State", "Modified Date"}) {
			@Override
			public boolean isCellEditable(int row, int column) {
				// all cells false
				return false;
			}
		});
	}
	
	/**
	 * Initialize the contents of the frame.
	 * @wbp.parser.entryPoint
	 */
	public void initialize() {
		frame = new JFrame(getPopupName(state));
		frame.setBounds(100, 100, 446, 510);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblOrder = new JLabel("ID");
		lblOrder.setBounds(70, 30, 149, 21);			
		frame.getContentPane().add(lblOrder);
		
		orderCode = new JTextField();
		orderCode.setBounds(231, 29, 116, 22);
		frame.getContentPane().add(orderCode);
		orderCode.setColumns(10);
		
		JLabel lblEmployee = new JLabel("Employee ID");
		lblEmployee.setBounds(70, 70, 149, 21);			
		frame.getContentPane().add(lblEmployee);
		
		employeeCode = new JTextField();
		employeeCode.setBounds(231, 69, 116, 22);
		frame.getContentPane().add(employeeCode);
		employeeCode.setColumns(10);
		
		JLabel lblMerchandise = new JLabel("Merchandise ID");
		lblMerchandise.setBounds(70, 110, 149, 21);			
		frame.getContentPane().add(lblMerchandise);
		
		merchCode = new JTextField();
		merchCode.setBounds(231, 109, 116, 22);
		frame.getContentPane().add(merchCode);
		merchCode.setColumns(10);
		
		JLabel lblSite = new JLabel("Site ID");
		lblSite.setBounds(70, 150, 149, 21);
		frame.getContentPane().add(lblSite);
		
		siteCode = new JTextField();
		siteCode.setBounds(231, 149, 116, 22);
		frame.getContentPane().add(siteCode);
		siteCode.setColumns(10);
		
		JLabel lblQuantityOrdered = new JLabel("Quantity ordered");
		lblQuantityOrdered.setBounds(70, 190, 149, 21);
		frame.getContentPane().add(lblQuantityOrdered);
		
		quantityOrdered = new JTextField();
		quantityOrdered.setColumns(10);
		quantityOrdered.setBounds(231, 189, 116, 22);
		frame.getContentPane().add(quantityOrdered);
		
		JLabel lblQuantityReturned = new JLabel("Quantity Returned");
		lblQuantityReturned.setBounds(70, 230, 149, 16);
		frame.getContentPane().add(lblQuantityReturned);
		
		quantityReturned = new JTextField();
		quantityReturned.setColumns(10);
		quantityReturned.setBounds(231, 229, 116, 22);
		frame.getContentPane().add(quantityReturned);
		
		JLabel lblDeliveryMean = new JLabel("Delivery Mean");
		lblDeliveryMean.setBounds(70, 270, 149, 16);
		frame.getContentPane().add(lblDeliveryMean);
		
		deliveryMean = new JTextField();
		deliveryMean.setColumns(10);
		deliveryMean.setBounds(231, 269, 116, 22);
		frame.getContentPane().add(deliveryMean);
		
		JLabel lblState = new JLabel("State");
		lblState.setBounds(70, 310, 149, 16);
		frame.getContentPane().add(lblState);
		
		State = new JTextField();
		State.setColumns(10);
		State.setBounds(231, 309, 116, 22);
		frame.getContentPane().add(State);
		
		if (state == 1) {
			orderCode.setEditable(false);
			employeeCode.setEditable(false);
			quantityOrdered.setEditable(false);
			siteCode.setEditable(false);
			merchCode.setEditable(false);
			deliveryMean.setEditable(false);
			State.setEditable(false);
		}
		else if (state == 2) {
			orderCode.setEditable(false);
			employeeCode.setEditable(false);
			quantityOrdered.setEditable(false);
			quantityReturned.setEditable(false);
			siteCode.setEditable(false);
			merchCode.setEditable(false);
			deliveryMean.setEditable(false);
			State.setEditable(false);
		}
		
		JButton btnAccept = new JButton("Accept");
		btnAccept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (validateBeforeSave()) {
					Integer State_int = 0;
					Integer returnQuan = Integer.valueOf(quantityReturned.getText());
					Integer orderQuan  = Integer.valueOf(quantityOrdered.getText());
					if (returnQuan == 0 ) {
						State_int = 0; 
					} else if ( returnQuan < orderQuan) {
						State_int = 1;
					} else {
						State_int = 2;
					}
					OrderNew order = new OrderNew(new Date(), siteCode.getText(), employeeCode.getText() , merchCode.getText(), 
							orderQuan , deliveryMean.getText(), returnQuan ,  State_int, orderCode.getText());
					switch (state) {
					case 1: // mode edit
//						System.out.println(state);
						handleActionEdit(frame, order, orderController);
						refresh();
						break;
					default: // mode delete - view
						frame.setVisible(false);
						break;
					}
				}
			}
		});
		btnAccept.setBounds(70, 400, 97, 25);
		frame.getContentPane().add(btnAccept);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
//				System.out.println("Hello world");
			}
		});
		btnCancel.setBounds(250, 400, 97, 25);
		frame.getContentPane().add(btnCancel);

	}
	
	@Override
	public boolean validateBeforeSave() {
		if (merchCode.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Merchandise ID can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (quantityOrdered.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Quantity ordered can not be empty", "Error",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (deliveryMean.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Delivery means can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (employeeCode.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "EmployeeID can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (siteCode.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "SiteID can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		try {
			int orderedQuantity = Integer.parseInt(quantityOrdered.getText());
			int returnedQuantity = Integer.parseInt(quantityReturned.getText());
			if (returnedQuantity < 0 ) {
				JOptionPane.showMessageDialog(null, "Return quantity must >= 0 ", "Error",
						JOptionPane.ERROR_MESSAGE);
				return false;
			}
			else if (orderedQuantity < returnedQuantity) {
				JOptionPane.showMessageDialog(null, "Returned quantity > ordered quantity", "Error",
						JOptionPane.ERROR_MESSAGE);
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			JOptionPane.showMessageDialog(null, "Returned quantity must be integer", "Error",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		
		
		
		
		return true;
	}
}
