package ias.view.stock;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import ias.view.stock.StockTable;


public class TableSection {
	JLayeredPane layerPane;
	JTabbedPane stockPane;
	private JTextField searchFieldStock;
	JPanel stockPanel;
	JTable stockTab;
	StockTable stockTable;

	/**
	 * @wbp.parser.entryPoint
	 */
	public void initialize() {
		layerPane = new JLayeredPane();
		layerPane.setBounds(128, 40, 792, 350);
		layerPane.setLayout(null);
		
		//set component for stock view 
		stockPane = new JTabbedPane(JTabbedPane.TOP);
		searchFieldStock = new JTextField();
		stockPanel = new JPanel();
		stockTab = new JTable();
		
		stockTable = new StockTable(stockPane, stockPanel, searchFieldStock, stockTab);
		stockTable.initialize();
		
		layerPane.add(stockPane);
		layerPane.setLayer(stockPane, 0);
	}
	
	public void switchLayeredPanels(JLayeredPane layeredPane, JTabbedPane tabbedPane) {
		layeredPane.removeAll();
		layeredPane.add(tabbedPane);
	}
}
