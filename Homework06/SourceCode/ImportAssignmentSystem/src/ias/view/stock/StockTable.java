package ias.view.stock;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import ias.common.ServiceResponse;
import ias.controller.OrderController;
import ias.controller.OrderNewController;
import ias.controller.StockController;
import ias.model.Order;
import ias.model.OrderNew;
import ias.model.Stock;
import ias.repository.OrderNewRepository;
import ias.repository.OrderRepository;
import ias.repository.StockRepository;

public class StockTable {
	JTabbedPane stockPane;
	private JTextField searchFieldStock;
	JPanel stockPanel;
	JTable stockTab;
	public static OrderNewController orderController = new OrderNewController (new OrderNewRepository(new OrderNew()));
	
	public StockTable(JTabbedPane stockPane, JPanel stockPanel, JTextField searchFieldStock, JTable stockTab) {
		super();
		this.stockPane = stockPane;
		this.searchFieldStock = searchFieldStock;
		this.stockPanel = stockPanel;
		this.stockTab = stockTab;
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	public void initialize() {
		stockPane.setBounds(30, 0, 734, 350);
		
		stockPanel.setLayout(null);
		stockPane.addTab("Stock", null, stockPanel, null);
		
		searchFieldStock.setColumns(10);
		searchFieldStock.setBounds(613, 0, 116, 22);
		stockPanel.add(searchFieldStock);
		
		JLabel label = new JLabel();
		label.setBounds(566, 3, 46, 16);
		stockPanel.add(label);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 26, 729, 280);
		stockPanel.add(scrollPane);
		
		ServiceResponse responseOrder = orderController.getAll();
		Object[][] listStock = new Object[responseOrder.Data.size()][];

		for (int i = 0; i < responseOrder.Data.size(); i++) {
//			Order order = (Order) responseOrder.Data.get(i);
			OrderNew order = (OrderNew) responseOrder.Data.get(i);
			listStock[i] = new Object[] { order.getOrderCode(), order.getEmployeeCode() ,order.getMerchandiseCode(), order.getSiteCode(), 
					order.getOrderQuantity(), order.getReturnQuantity(), order.getDeliveryMean(),  order.getState(), order.getModifiedDate() };
//			System.out.println(order.getPrimaryKeyFieldValue());
//			listStock[i] = new Object[] {order.getEmployeeCode() ,order.getMerchandiseCode(), order.getSiteCode(), 
//					order.getOrderQuantity(), order.getReturnQuantity(), order.getDeliveryMean(),  order.getState(), order.getModifiedDate() };
			
		}

		stockTab.setModel(new DefaultTableModel(listStock,
				new String[] {"ID", "Employee ID", "Merchandise ID", "Site ID", "Quantity ordered",  "Quantity returned", "Delivery means" ,"State", "Modified Date"}));
		scrollPane.setViewportView(stockTab);
	}
}
