package ias.view.order;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.toedter.calendar.JDateChooser;

import ias.common.ResponseCode;
import ias.common.ServiceResponse;
import ias.controller.InstockMerchandiseController;
import ias.controller.MerchandiseController;
import ias.controller.OrderController;
import ias.controller.SiteController;
import ias.controller.InstockMerchandiseController;
import ias.model.InstockMerchandise;
import ias.model.Merchandise;
import ias.model.Order;
import ias.model.Site;
import ias.model.InstockMerchandise;
import ias.repository.InstockMerchandiseRepository;
import ias.repository.MerchandiseRepository;
import ias.repository.OrderRepository;
import ias.repository.SiteRepository;
import ias.repository.InstockMerchandiseRepository;
import ias.view.BasePopup;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;

public class PopupPlaceOrder extends BasePopup {
	private static OrderController orderController = new OrderController(new OrderRepository(new Order()));
	private static SiteController siteController = new SiteController(new SiteRepository(new Site()));
	private static InstockMerchandiseController instockMerchandiseController = new InstockMerchandiseController(
			new InstockMerchandiseRepository(new InstockMerchandise()));

	public JTable table2;
	public JFrame frame;
	public JTextField merchID;
	public JTextField quantityOrdered;
	public JTextField deliveryMeans;
	public JTextField employeeID;
	public JTextField siteID;
	JDateChooser dateChooser;
	public JTextField quantityReturned;

	/**
	 * Create the application.
	 */

	public PopupPlaceOrder(int state, JTable table, JTable table2) {
		super();
		this.state = state;
		this.table = table;
		this.table2 = table2;
		initialize();
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PopupPlaceOrder window = new PopupPlaceOrder(0, null, null);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	protected String getModelName() {
		return "instock merchandise";
	}

	protected void refresh() {
		ServiceResponse responseOrder = orderController.getAll();
		Object[][] listUnprocessedOrder = new Object[responseOrder.Data.size()][];
		int j = 0;
		for (int i = 0; i < responseOrder.Data.size(); i++) {
			Order order = (Order) responseOrder.Data.get(i);
			if (order.getReturnQuantity() == 0 || order.getReturnQuantity() < order.getOrderQuantity()) {
				listUnprocessedOrder[j] = new Object[] { order.getEmployeeCode(), order.getMerchandiseCode(),
						order.getSiteCode(), order.getOrderQuantity(), order.getDeliveryMean(),
						order.getReturnQuantity(), order.getState(), order.getModifiedDate() };
				j++;
			}
		}
		table2.setModel(new DefaultTableModel(listUnprocessedOrder, new String[] { "Employee ID", "Merchandise ID",
				"Site code", "Quantity ordered", "Delivery means", "Quantity returned", "State", "Modified Date" }) {
			@Override
			public boolean isCellEditable(int row, int column) {
				// all cells false
				return false;
			}
		});

		ServiceResponse responseInstockMerchandise = instockMerchandiseController.getByView("view_supply_site");
		Object[][] listInstockMerchandise = new Object[responseInstockMerchandise.Data.size()][];
		for (int i = 0; i < responseInstockMerchandise.Data.size(); i++) {
			InstockMerchandise instockMerchandise = (InstockMerchandise) responseInstockMerchandise.Data.get(i);
			listInstockMerchandise[i] = new Object[] { instockMerchandise.getSupplyId(),
					instockMerchandise.getSiteName(), instockMerchandise.getMerchandiseName(),
					instockMerchandise.getInstockQuantity(), instockMerchandise.getUnit(),
					instockMerchandise.getDeliveryByShip(), instockMerchandise.getDeliveryByAir(),
					instockMerchandise.getSiteId(), instockMerchandise.getMerchandiseId() };
		}
		table.setModel(new DefaultTableModel(listInstockMerchandise,
				new String[] { "Supply id", "Site name", "Merchandise name", "In-stock quantity", "Unit",
						"Delivery by ship", "Delivery by air", "Site ID", "Merchandise ID" }) {
			@Override
			public boolean isCellEditable(int row, int column) {
				// all cells false
				return false;
			}
		});
		table.removeColumn(table.getColumnModel().getColumn(0));
		table.removeColumn(table.getColumnModel().getColumn(3));
		table.removeColumn(table.getColumnModel().getColumn(5));
		table.removeColumn(table.getColumnModel().getColumn(5));
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public void initialize() {
		frame = new JFrame("Order");
		frame.setBounds(100, 100, 446, 510);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblMerchandise = new JLabel("Merchandise ID");
		lblMerchandise.setBounds(70, 110, 149, 21);
		frame.getContentPane().add(lblMerchandise);

		JLabel lblQuantityOrdered = new JLabel("Quantity ordered");
		lblQuantityOrdered.setBounds(70, 181, 149, 21);
		frame.getContentPane().add(lblQuantityOrdered);

		JLabel lblNewLabel = new JLabel("Delivery means");
		lblNewLabel.setBounds(70, 249, 149, 16);
		frame.getContentPane().add(lblNewLabel);

		JLabel lblDesiredDeliveryDate = new JLabel("Desired delivery date");
		lblDesiredDeliveryDate.setBounds(70, 274, 167, 21);
		frame.getContentPane().add(lblDesiredDeliveryDate);

		JLabel lblNewLabel_1 = new JLabel("Date");
		lblNewLabel_1.setBounds(70, 308, 56, 16);
		frame.getContentPane().add(lblNewLabel_1);

		merchID = new JTextField();
		merchID.setBounds(231, 109, 116, 22);
		frame.getContentPane().add(merchID);
		merchID.setColumns(10);

		quantityOrdered = new JTextField();
		quantityOrdered.setColumns(10);
		quantityOrdered.setBounds(231, 180, 116, 22);
		frame.getContentPane().add(quantityOrdered);

		deliveryMeans = new JTextField();
		deliveryMeans.setColumns(10);
		deliveryMeans.setBounds(231, 246, 116, 22);
		frame.getContentPane().add(deliveryMeans);

		JButton btnAccept = new JButton("Accept");
		btnAccept.setBounds(70, 400, 97, 25);
		frame.getContentPane().add(btnAccept);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
			}
		});
		btnCancel.setBounds(250, 400, 97, 25);
		frame.getContentPane().add(btnCancel);

		dateChooser = new JDateChooser();
		dateChooser.setBounds(231, 308, 135, 22);
		dateChooser.setDateFormatString("dd/MM/yyyy");

		frame.getContentPane().add(dateChooser);

		employeeID = new JTextField();
		employeeID.setColumns(10);
		employeeID.setBounds(231, 75, 116, 22);
		frame.getContentPane().add(employeeID);

		JLabel lblEmployeeId = new JLabel("Employee ID");
		lblEmployeeId.setBounds(70, 76, 149, 21);
		frame.getContentPane().add(lblEmployeeId);

		JLabel lblSiteId = new JLabel("Site ID");
		lblSiteId.setBounds(70, 145, 149, 21);
		frame.getContentPane().add(lblSiteId);

		siteID = new JTextField();
		siteID.setColumns(10);
		siteID.setBounds(231, 144, 116, 22);
		frame.getContentPane().add(siteID);

		JLabel lblQuantityReturned = new JLabel("Quantity returned");
		lblQuantityReturned.setBounds(70, 216, 149, 21);
		frame.getContentPane().add(lblQuantityReturned);

		quantityReturned = new JTextField();
		quantityReturned.setColumns(10);
		quantityReturned.setBounds(231, 215, 116, 22);
		frame.getContentPane().add(quantityReturned);

		if (state == 2) {
			quantityOrdered.setEditable(false);
			deliveryMeans.setEditable(false);
			merchID.setEditable(false);
			siteID.setEditable(false);
			employeeID.setEditable(false);
			merchID.setEditable(false);
			quantityReturned.setEditable(false);
			dateChooser.setEnabled(false);
		}
		if (state == 0) {
			quantityReturned.setEditable(false);
		}
		btnAccept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (validateBeforeSave()) {
					int selectedRow = table.getSelectedRow();
					int stock = (int) table.getModel().getValueAt(selectedRow, 3);
					int ordered = Integer.parseInt(quantityOrdered.getText());
					String orderId = Integer.toString(table2.getModel().getRowCount());
					if (orderId == null) orderId = "0";
					int left = stock - ordered;
										
					Order order = new Order(orderId.toString(), dateChooser.getDate(), siteID.getText(), employeeID.getText(), merchID.getText(),
							Integer.parseInt(quantityOrdered.getText()), deliveryMeans.getText(),
							Integer.parseInt(quantityReturned.getText()), 0);
					InstockMerchandise instockMerchandise = new InstockMerchandise(new Date(),
							(String) table.getModel().getValueAt(selectedRow, 0), merchID.getText(), "merchandiseId", "unit",
							siteID.getText(), "siteName", left, 0, 0);

				switch (state) {
				case 0: // mode add
					handleActionAdd(frame, order, orderController);
					handleActionEdit(frame, instockMerchandise, instockMerchandiseController);
					refresh();
					break;
				case 1: 
					break;
				default: // mode delete - view
					frame.setVisible(false);
					break;
				}
				}
			}
		});
	}

	@Override
	public boolean validateBeforeSave() {
		if (merchID.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Merchandise ID can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (quantityOrdered.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Quantity ordered can not be empty", "Error",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (deliveryMeans.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Delivery means can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (employeeID.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "EmployeeID can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (siteID.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "SiteID can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		try {
			Date date = dateChooser.getDate();
			String strDate = DateFormat.getDateInstance().format(date);

		} catch (Exception e) {
			// TODO: handle exception
			JOptionPane.showMessageDialog(null, "Date can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		int orderedQuantity = Integer.parseInt(quantityOrdered.getText());
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		int selectedIndexRow = table.getSelectedRow();
		int instockQuantity = Integer.parseInt(table.getValueAt(selectedIndexRow, 2).toString());
		if (orderedQuantity > instockQuantity) {
			int answer = JOptionPane.showConfirmDialog(null,
					"Ordered quantity > in-stock quantity. Do you want to order from different sites?", "Error",
					JOptionPane.ERROR_MESSAGE);
			if (answer == 0) {

			} else
				frame.setVisible(false);
			return false;
		}
		return true;
	}

}
