package ias.view.order;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.toedter.calendar.JDateChooser;

import ias.common.ResponseCode;
import ias.common.ServiceResponse;
import ias.controller.OrderController;
import ias.model.Order;
import ias.repository.OrderRepository;
import ias.view.BasePopup;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;

public class PopupProcessedOrder extends BasePopup {
	private static OrderController orderController = new OrderController(new OrderRepository(new Order()));

	public JFrame frame;
	public JTextField merchID;
	public JTextField quantityOrdered;
	public JTextField deliveryMeans;
	public JTextField employeeID;
	public JTextField siteID;
	JDateChooser dateChooser;
	public JTextField quantityReturned;

	/**
	 * Create the application.
	 */

	public PopupProcessedOrder(int state, JTable table) {
		super();
		this.state = state;
		this.table = table;
		initialize();
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PopupProcessedOrder window = new PopupProcessedOrder(0, null);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	protected String getModelName() {
		return "processed order";
	}
	
	protected void refresh() {
		ServiceResponse responseOrder = orderController.getAll();
		Object[][] listProcessedOrder = new Object[responseOrder.Data.size()][];
		int k = 0;
		for (int i = 0; i < responseOrder.Data.size(); i++) {
			Order order = (Order) responseOrder.Data.get(i);
			if (order.getReturnQuantity() >= order.getOrderQuantity()) {
				listProcessedOrder[k] = new Object[] {order.getEmployeeCode(), order.getMerchandiseCode(), order.getSiteCode(), 
						order.getOrderQuantity(), order.getDeliveryMean(), order.getReturnQuantity(), order.getState(), order.getModifiedDate(), order.getOrderId()};
				k++;
			}
		}
		table.setModel(new DefaultTableModel(listProcessedOrder,
				new String[] { "Employee ID", "Merchandise ID", "Site code", "Quantity ordered", "Delivery means" , "Quantity returned", "State", "Modified Date", "Order ID"}));
	}
	
	/**
	 * Initialize the contents of the frame.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public void initialize() {
		frame = new JFrame(getPopupName(state));
		frame.setBounds(100, 100, 446, 510);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblMerchandise = new JLabel("Merchandise ID");
		lblMerchandise.setBounds(70, 110, 149, 21);
		frame.getContentPane().add(lblMerchandise);

		JLabel lblQuantityOrdered = new JLabel("Quantity ordered");
		lblQuantityOrdered.setBounds(70, 181, 149, 21);
		frame.getContentPane().add(lblQuantityOrdered);

		JLabel lblNewLabel = new JLabel("Delivery means");
		lblNewLabel.setBounds(70, 249, 149, 16);
		frame.getContentPane().add(lblNewLabel);

		JLabel lblDesiredDeliveryDate = new JLabel("Desired delivery date");
		lblDesiredDeliveryDate.setBounds(70, 274, 167, 21);
		frame.getContentPane().add(lblDesiredDeliveryDate);

		JLabel lblNewLabel_1 = new JLabel("Date");
		lblNewLabel_1.setBounds(70, 308, 56, 16);
		frame.getContentPane().add(lblNewLabel_1);

		merchID = new JTextField();
		merchID.setBounds(231, 109, 116, 22);
		frame.getContentPane().add(merchID);
		merchID.setColumns(10);

		quantityOrdered = new JTextField();
		quantityOrdered.setColumns(10);
		quantityOrdered.setBounds(231, 180, 116, 22);
		frame.getContentPane().add(quantityOrdered);

		deliveryMeans = new JTextField();
		deliveryMeans.setColumns(10);
		deliveryMeans.setBounds(231, 246, 116, 22);
		frame.getContentPane().add(deliveryMeans);


		JButton btnAccept = new JButton("Accept");
		btnAccept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (validateBeforeSave()) {
					switch (state) {
					case 0: // mode add
						refresh();
						String orderId = Integer.toString(table.getModel().getRowCount());
						System.out.println(orderId);
						// Number of sites = 5, so set siteID = 6 won't add to database					
						Order order = new Order(orderId, dateChooser.getDate(), siteID.getText(), employeeID.getText(), merchID.getText(),
								Integer.parseInt(quantityOrdered.getText()), deliveryMeans.getText(), Integer.parseInt(quantityReturned.getText()), 2);
						handleActionAdd(frame, order, orderController);
						refresh();
						break;
					case 1: // mode edit
						int selectedIndex = table.getSelectedRow();

						refresh();
						System.out.println(selectedIndex);
						String order_Id = (String) table.getModel().getValueAt(selectedIndex, 8);
//						System.out.println(order_Id);
						Order order1 = new Order(order_Id, dateChooser.getDate(), siteID.getText(), employeeID.getText(), merchID.getText(),
								Integer.parseInt(quantityOrdered.getText()), deliveryMeans.getText(), Integer.parseInt(quantityReturned.getText()), 2);
						handleActionEdit(frame, order1, orderController);
						refresh();
						break;
					default: // mode delete - view
						frame.setVisible(false);
						break;
					}
				}
			}
		});
		btnAccept.setBounds(70, 400, 97, 25);
		frame.getContentPane().add(btnAccept);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
			}
		});
		btnCancel.setBounds(250, 400, 97, 25);
		frame.getContentPane().add(btnCancel);

		dateChooser = new JDateChooser();
		dateChooser.setBounds(231, 308, 135, 22);
		dateChooser.setDateFormatString("dd/MM/yyyy");

		if (state == 2)
			dateChooser.setEnabled(false);

		frame.getContentPane().add(dateChooser);

		employeeID = new JTextField();
		employeeID.setColumns(10);
		employeeID.setBounds(231, 75, 116, 22);
		frame.getContentPane().add(employeeID);

		JLabel lblEmployeeId = new JLabel("Employee ID");
		lblEmployeeId.setBounds(70, 76, 149, 21);
		frame.getContentPane().add(lblEmployeeId);

		JLabel lblSiteId = new JLabel("Site ID");
		lblSiteId.setBounds(70, 145, 149, 21);
		frame.getContentPane().add(lblSiteId);

		siteID = new JTextField();
		siteID.setColumns(10);
		siteID.setBounds(231, 144, 116, 22);
		frame.getContentPane().add(siteID);
		
		JLabel lblQuantityReturned = new JLabel("Quantity returned");
		lblQuantityReturned.setBounds(70, 216, 149, 21);
		frame.getContentPane().add(lblQuantityReturned);
		
		quantityReturned = new JTextField();
		quantityReturned.setColumns(10);
		quantityReturned.setBounds(231, 215, 116, 22);
		frame.getContentPane().add(quantityReturned);
		
		if (state == 2) {
			quantityOrdered.setEditable(false);
			deliveryMeans.setEditable(false);
			merchID.setEditable(false);
			siteID.setEditable(false);
			employeeID.setEditable(false);
			merchID.setEditable(false);
			quantityReturned.setEditable(false);
			dateChooser.setEnabled(false);	
		}
	}
	
	@Override
	public boolean validateBeforeSave() {
		if (merchID.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Merchandise ID can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (quantityOrdered.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Quantity ordered can not be empty", "Error",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (deliveryMeans.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Delivery means can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (employeeID.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "EmployeeID can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (siteID.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "SiteID can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		try {
			Date date = dateChooser.getDate();			
			String strDate = DateFormat.getDateInstance().format(date);

		} catch (Exception e) {
			// TODO: handle exception
			JOptionPane.showMessageDialog(null, "Date can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		int orderedQuantity = Integer.parseInt(quantityOrdered.getText());
		int returnedQuantity = Integer.parseInt(quantityReturned.getText());
		if (orderedQuantity > returnedQuantity) {
			JOptionPane.showMessageDialog(null, "Returned quantity < ordered quantity", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}		
		return true;
	}

}
