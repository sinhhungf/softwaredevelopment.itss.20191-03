package ias.view;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import org.jdatepicker.util.JDatePickerUtil;
import com.toedter.calendar.JDateChooser;

public class PopupWindow extends BasePopup {

	public JFrame frame;
	public JTextField merchCode;
	public JTextField quantityOrdered;
	public JTextField unit;
	private JTextField textField;
	private JTextField textField_1;
	
	/**
	 * Create the application.
	 */

	public PopupWindow(int state) {
		super();
		this.state = state;
		initialize();
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PopupWindow window = new PopupWindow(0);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	@Override
	protected String getPopupName(int state) {
		if (state == 0)
		return "Add new order";
		else if (state == 1)
			return "Edit order";
		else return "View order";
	}
	
	/**
	 * Initialize the contents of the frame.
	 * @wbp.parser.entryPoint
	 */
	public void initialize() {
		frame = new JFrame(getPopupName(state));
		frame.setBounds(100, 100, 446, 510);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblMerchandise = new JLabel("Merchandise ID");
		lblMerchandise.setBounds(70, 110, 149, 21);			
		frame.getContentPane().add(lblMerchandise);
		
		JLabel lblQuantityOrdered = new JLabel("Quantity ordered");
		lblQuantityOrdered.setBounds(70, 181, 149, 21);
		frame.getContentPane().add(lblQuantityOrdered);
		
		JLabel lblNewLabel = new JLabel("Delivery means");
		lblNewLabel.setBounds(70, 218, 149, 16);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblDesiredDeliveryDate = new JLabel("Desired delivery date");
		lblDesiredDeliveryDate.setBounds(70, 274, 167, 21);
		frame.getContentPane().add(lblDesiredDeliveryDate);
		
		JLabel lblNewLabel_1 = new JLabel("Date");
		lblNewLabel_1.setBounds(70, 308, 56, 16);
		frame.getContentPane().add(lblNewLabel_1);
		
		merchCode = new JTextField();
		merchCode.setBounds(231, 109, 116, 22);
		frame.getContentPane().add(merchCode);
		merchCode.setColumns(10);
		
		quantityOrdered = new JTextField();
		quantityOrdered.setColumns(10);
		quantityOrdered.setBounds(231, 180, 116, 22);
		frame.getContentPane().add(quantityOrdered);
		
		unit = new JTextField();
		unit.setColumns(10);
		unit.setBounds(231, 215, 116, 22);
		frame.getContentPane().add(unit);
		
		if (state == 2) {
			quantityOrdered.setEditable(false);
			unit.setEditable(false);
			merchCode.setEditable(false);
		}
		
		JButton btnAccept = new JButton("Accept");
		btnAccept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (state == 0) {
					
				}
			}
		});
		btnAccept.setBounds(70, 400, 97, 25);
		frame.getContentPane().add(btnAccept);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
			}
		});
		btnCancel.setBounds(250, 400, 97, 25);
		frame.getContentPane().add(btnCancel);
		
		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setBounds(231, 308, 135, 22);
		dateChooser.setDateFormatString("dd/MM/yyyy");
		
		if (state == 2) dateChooser.setEnabled(false);
		
		frame.getContentPane().add(dateChooser);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(231, 75, 116, 22);
		frame.getContentPane().add(textField);
		
		JLabel lblEmployeeId = new JLabel("Employee ID");
		lblEmployeeId.setBounds(70, 76, 149, 21);
		frame.getContentPane().add(lblEmployeeId);
		
		JLabel lblSiteId = new JLabel("Site ID");
		lblSiteId.setBounds(70, 145, 149, 21);
		frame.getContentPane().add(lblSiteId);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(231, 144, 116, 22);
		frame.getContentPane().add(textField_1);
	}
}
