package ias.view.sites;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import ias.common.ResponseCode;
import ias.common.ServiceResponse;
import ias.controller.MerchandiseController;
import ias.model.Merchandise;
import ias.model.Merchandise;
import ias.repository.MerchandiseRepository;
import ias.view.BasePopup;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Date;
import java.awt.event.ActionEvent;

public class PopupMerchandise extends BasePopup {
	private static MerchandiseController merchandiseController = new MerchandiseController(
			new MerchandiseRepository(new Merchandise()));
	
	public JTextField merchandiseId = new JTextField();
	public JTextField merchandiseName = new JTextField();
	public JTextField unit = new JTextField();
	protected JTable table;

	/**
	 * Create the application.
	 */

	public PopupMerchandise(int state, JTable table) {
		super();
		this.state = state;
		this.table = table;
		initialize();
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PopupMerchandise window = new PopupMerchandise(0, null);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	protected String getModelName() {
		return "merchandise";
	}
	
	protected void refresh() {
		ServiceResponse responseMerchandise = merchandiseController.getAll();
		Object[][] listMerchandise = new Object[responseMerchandise.Data.size()][];
		for (int i = 0; i < responseMerchandise.Data.size(); i++) {
			Merchandise merchandise = (Merchandise) responseMerchandise.Data.get(i);
				listMerchandise[i] = new Object[] { merchandise.getMerchandiseId(), merchandise.getMerchandiseName(), 
						merchandise.getUnit() };

		}
		table.setModel(new DefaultTableModel(listMerchandise,
				new String[] { "Merchandise id", "Merchandise name", "Unit" }) {
			@Override
			public boolean isCellEditable(int row, int column) {
				// all cells false
				return false;
			}
		});
	}
	
	/**
	 * Initialize the contents of the frame.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public void initialize() {
		frame = new JFrame(getPopupName(state));
		frame.setBounds(100, 100, 446, 420);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblMerchandiseId = new JLabel("Merchandise Id");
		lblMerchandiseId.setBounds(70, 63, 126, 21);
		frame.getContentPane().add(lblMerchandiseId);

		JLabel lblMerchandiseName = new JLabel("Merchandise Name");
		lblMerchandiseName.setBounds(70, 116, 126, 21);
		frame.getContentPane().add(lblMerchandiseName);

		JLabel lblUnit = new JLabel("Unit");
		lblUnit.setBounds(70, 183, 113, 16);
		frame.getContentPane().add(lblUnit);

		merchandiseId = new JTextField();
		merchandiseId.setBounds(231, 62, 116, 22);
		frame.getContentPane().add(merchandiseId);
		merchandiseId.setColumns(10);

		merchandiseName = new JTextField();
		merchandiseName.setColumns(10);
		merchandiseName.setBounds(231, 115, 116, 22);
		frame.getContentPane().add(merchandiseName);

		unit = new JTextField();
		unit.setColumns(10);
		unit.setBounds(231, 180, 116, 22);
		frame.getContentPane().add(unit);

		// If mode is View
		if (state == 2) {
			merchandiseId.setEditable(false);
			merchandiseName.setEditable(false);
			unit.setEditable(false);
		}

		JButton btnAccept = new JButton("Accept");
		btnAccept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (validateBeforeSave()) {
					Merchandise merchandise = new Merchandise(new Date(), merchandiseId.getText(),
							merchandiseName.getText(), unit.getText());
					switch (state) {
					case 0: // mode add
						handleActionAdd(frame, merchandise, merchandiseController);
						refresh();
						break;
					case 1: // mode edit
						handleActionEdit(frame, merchandise, merchandiseController);
						refresh();
						break;
					default: // mode delete - view
						frame.setVisible(false);
						break;
					}
				}
			}
		});
		btnAccept.setBounds(99, 324, 97, 25);
		frame.getContentPane().add(btnAccept);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				onClickBtnCancel();
			}
		});
		btnCancel.setBounds(231, 324, 97, 25);
		frame.getContentPane().add(btnCancel);
	}
	
	protected boolean validateBeforeSave() {
		if (merchandiseId.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Merchandise id can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (merchandiseName.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Merchandisename can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (unit.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Unit can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}
}
