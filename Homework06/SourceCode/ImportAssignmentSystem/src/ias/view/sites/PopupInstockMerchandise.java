package ias.view.sites;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import ias.common.ResponseCode;
import ias.common.ServiceResponse;
import ias.controller.InstockMerchandiseController;
import ias.controller.MerchandiseController;
import ias.controller.SiteController;
import ias.controller.InstockMerchandiseController;
import ias.model.InstockMerchandise;
import ias.model.Merchandise;
import ias.model.Site;
import ias.model.InstockMerchandise;
import ias.repository.InstockMerchandiseRepository;
import ias.repository.MerchandiseRepository;
import ias.repository.SiteRepository;
import ias.repository.InstockMerchandiseRepository;
import ias.view.BasePopup;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Date;
import java.awt.event.ActionEvent;

public class PopupInstockMerchandise extends BasePopup {
	private static SiteController siteController = new SiteController(new SiteRepository(new Site()));
	private static MerchandiseController merchandiseController = new MerchandiseController(
			new MerchandiseRepository(new Merchandise()));
	private static InstockMerchandiseController instockMerchandiseController = new InstockMerchandiseController(
			new InstockMerchandiseRepository(new InstockMerchandise()));

	public JTextField supplyId = new JTextField();
	public JComboBox<Site> siteId;
	public JTextField siteName = new JTextField();
	public JComboBox<Merchandise> merchandiseId;
	public JTextField merchandiseName = new JTextField();
	public JTextField instockQuantity = new JTextField();
	public JTextField unit = new JTextField();
	public JTextField deliveryByShip = new JTextField();
	public JTextField deliveryByAir= new JTextField();

	/**
	 * Create the application.
	 */

	public PopupInstockMerchandise(int state, JTable table) {
		super();
		this.state = state;
		this.table = table;
		initialize();
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PopupInstockMerchandise window = new PopupInstockMerchandise(0, null);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	protected String getModelName() {
		return "instock merchandise";
	}

	protected void refresh() {
		ServiceResponse responseInstockMerchandise = instockMerchandiseController.getByView("view_supply_site");
		Object[][] listInstockMerchandise = new Object[responseInstockMerchandise.Data.size()][];
		for (int i = 0; i < responseInstockMerchandise.Data.size(); i++) {
			InstockMerchandise instockMerchandise = (InstockMerchandise) responseInstockMerchandise.Data.get(i);
			listInstockMerchandise[i] = new Object[] { instockMerchandise.getSupplyId(),
					instockMerchandise.getSiteName(), instockMerchandise.getMerchandiseName(),
					instockMerchandise.getInstockQuantity(), instockMerchandise.getUnit(),
					instockMerchandise.getDeliveryByShip(), instockMerchandise.getDeliveryByAir(),
					instockMerchandise.getSiteId(), instockMerchandise.getMerchandiseId() };
		}
		table.setModel(new DefaultTableModel(listInstockMerchandise, new String[] {"Supply id", "Site name",
				"Merchandise name", "In-stock quantity", "Unit", "Delivery by ship", "Delivery by air",
				"33", "333"}) {
			@Override
			public boolean isCellEditable(int row, int column) {
				// all cells false
				return false;
			}
		});	
		table.removeColumn(table.getColumnModel().getColumn(0));
		table.removeColumn(table.getColumnModel().getColumn(7));
		table.removeColumn(table.getColumnModel().getColumn(6));
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public void initialize() {
		frame = new JFrame(getPopupName(state));
		frame.setBounds(100, 100, 800, 370);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblSiteId = new JLabel("Site id");
		lblSiteId.setBounds(70, 63, 167, 21);
		frame.getContentPane().add(lblSiteId);

		JLabel lblSiteName = new JLabel("Site name");
		lblSiteName.setBounds(394, 63, 202, 21);
		frame.getContentPane().add(lblSiteName);

		JLabel lblMerchandiseId = new JLabel("Merchandise id");
		lblMerchandiseId.setBounds(70, 119, 167, 21);
		frame.getContentPane().add(lblMerchandiseId);

		JLabel lblMerchandiseName = new JLabel("Merchandise name");
		lblMerchandiseName.setBounds(394, 119, 202, 21);
		frame.getContentPane().add(lblMerchandiseName);

		JLabel lblInstockQuantity = new JLabel("Instock quantity");
		lblInstockQuantity.setBounds(70, 183, 167, 16);
		frame.getContentPane().add(lblInstockQuantity);

		siteId = new JComboBox<Site>();
		siteId.setBounds(255, 63, 116, 22);
		frame.getContentPane().add(siteId);
		ServiceResponse responseSite = siteController.getAll();
		Site[] listSite = new Site[responseSite.Data.size()];
		for (int i = 0; i < responseSite.Data.size(); i++) {
			Site site = (Site) responseSite.Data.get(i);
			listSite[i] = site;
		}
		siteId.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Site selectedSite = (Site) siteId.getSelectedItem();
				siteName.setText(selectedSite.getSiteName());
			}
		});
		siteId.setModel(new DefaultComboBoxModel(listSite));

		siteName = new JTextField();
		if (state == 0) {
			Site firstSite = (Site) siteId.getSelectedItem();
			siteName.setText(firstSite.getSiteName());
		}

		siteName.setColumns(10);
		siteName.setBounds(604, 63, 116, 22);
		frame.getContentPane().add(siteName);

		merchandiseId = new JComboBox<Merchandise>();
		merchandiseId.setBounds(255, 119, 116, 22);
		frame.getContentPane().add(merchandiseId);
		ServiceResponse responseMerchandise = merchandiseController.getAll();
		Merchandise[] listMerchandise = new Merchandise[responseMerchandise.Data.size()];
		for (int i = 0; i < responseMerchandise.Data.size(); i++) {
			Merchandise merchandise = (Merchandise) responseMerchandise.Data.get(i);
			listMerchandise[i] = merchandise;
		}
		merchandiseId.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Merchandise selectedMerchandise = (Merchandise) merchandiseId.getSelectedItem();
				merchandiseName.setText(selectedMerchandise.getMerchandiseName());
			}
		});
		merchandiseId.setModel(new DefaultComboBoxModel(listMerchandise));

		merchandiseName = new JTextField();
		if (state == 0) {
			Merchandise firstMerchandise = (Merchandise) merchandiseId.getSelectedItem();
			merchandiseName.setText(firstMerchandise.getMerchandiseName());
		}

		merchandiseName.setColumns(10);
		merchandiseName.setBounds(604, 119, 116, 22);
		frame.getContentPane().add(merchandiseName);

		instockQuantity = new JTextField();
		instockQuantity.setColumns(10);
		instockQuantity.setBounds(255, 181, 116, 22);
		frame.getContentPane().add(instockQuantity);

		// If mode is View
		if (state == 2) {
			merchandiseId.setEditable(false);
			merchandiseId.setEnabled(false);
			merchandiseName.setEditable(false);
			siteId.setEditable(false);
			siteId.setEnabled(false);
			siteName.setEditable(false);
			instockQuantity.setEditable(false);
		}

		JButton btnAccept = new JButton("Accept");
		btnAccept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (validateBeforeSave()) {
					String supplyIdVal = "";
					try {
						supplyIdVal = supplyId.getText();
					} catch (Exception ex) {
						supplyIdVal = "";
					}
					InstockMerchandise instockMerchandise = new InstockMerchandise(new Date(), supplyIdVal,
							merchandiseId.getSelectedItem().toString(), "merchandiseId", "unit",
							siteId.getSelectedItem().toString(), "siteName", Integer.valueOf(instockQuantity.getText()),
							0, 0);
					switch (state) {
					case 0: // mode add
						handleActionAdd(frame, instockMerchandise, instockMerchandiseController);
						refresh();
						break;
					case 1: // mode edit
						handleActionEdit(frame, instockMerchandise, instockMerchandiseController);
						refresh();
						break;
					default: // mode delete - view
						frame.setVisible(false);
						break;
					}
				}

			}
		});
		btnAccept.setBounds(287, 265, 97, 25);
		frame.getContentPane().add(btnAccept);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				onClickBtnCancel();
			}
		});
		btnCancel.setBounds(438, 265, 97, 25);
		frame.getContentPane().add(btnCancel);
	}

	/**
	 * validate data before save
	 * 
	 * @author NCThanh
	 */
	public boolean validateBeforeSave() {
		if (siteId.getSelectedItem() == null) {
			JOptionPane.showMessageDialog(null, "Site id can not be empty!", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (merchandiseId.getSelectedItem() == null) {
			JOptionPane.showMessageDialog(null, "Merchandise id can not be empty!", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (instockQuantity.getText() == null) {
			JOptionPane.showMessageDialog(null, "Quantity can not be empty!", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		} else {
			int instockQuantityVal;
			try {
				instockQuantityVal = Integer.parseInt(instockQuantity.getText());
			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(null, "Quantity must be a number!", "Error", JOptionPane.ERROR_MESSAGE);
				return false;
			}
			if (instockQuantityVal < 0) {
				JOptionPane.showMessageDialog(null, "Quantity can not be negative!", "Error",
						JOptionPane.ERROR_MESSAGE);
				return false;
			}
		}

		return true;
	}
}
