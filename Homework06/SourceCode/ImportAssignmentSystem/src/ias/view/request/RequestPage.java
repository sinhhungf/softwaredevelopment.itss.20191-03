package ias.view.request;

import ias.common.ServiceResponse;
import ias.controller.*;
import ias.model.Customer;
import ias.model.Employee;
import ias.model.Merchandise;
import ias.model.Request;
import ias.repository.CustomerRepository;
import ias.repository.EmployeeRepository;
import ias.repository.MerchandiseRepository;
import ias.repository.RequestRepository;
import ias.view.LoginPage;
import ias.view.sites.PopupMerchandise;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.Date;

public class RequestPage {
    public JFrame frame;
    private TableSection tableSection;

    private static CustomerController customerController = new CustomerController(new CustomerRepository(new Customer()));
    private static RequestController requestController = new RequestController(new RequestRepository(new Request()));
    private static MerchandiseController merchandiseController = new MerchandiseController(
            new MerchandiseRepository(new Merchandise()));
    private static EmployeeController employeeController = new EmployeeController(new EmployeeRepository(new Employee()));
    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        // Lambda Runnable
        Runnable task = () -> {
            try {
                RequestPage window = new RequestPage();
                window.frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
        EventQueue.invokeLater(task);
    }

    /**
     * Create the application.
     */
     public RequestPage() {
        initialize();
    }

    /**
     * Initialize the content of the frame
     */
    private void initialize() {
        tableSection = new TableSection();

        // Initialize Frame
        frame = new JFrame("Request Page");
        frame.setBounds(100, 100, 950, 544);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        frame.setBackground(Color.BLUE);
        frame.getContentPane().add(tableSection.layerPane);

        // Add CRUD
        frame.getContentPane().add(addCRUDBtn("Add", 289));
        frame.getContentPane().add(addCRUDBtn("Delete", 427));
        frame.getContentPane().add(addCRUDBtn("Edit", 557));
         frame.getContentPane().add(addCRUDBtn("View", 688));

        // Add Menu
        frame.getContentPane().add(addMenuBtn("Request", 105));
        frame.getContentPane().add(addMenuBtn("Settings", 281));
        frame.getContentPane().add(addMenuBtn("About", 319));
        frame.getContentPane().add(addMenuBtn("Logout", 357));
        frame.getContentPane().add(addMenuBtn("Exit", 395));

    }

    private JButton addMenuBtn(String name, int i1) {
        JButton btnLogout = new JButton(name);
        btnLogout.addActionListener( actionEvent ->  {
                    switch (name) {
                        case "Logout":
                            frame.dispose();
                            LoginPage.main(null);
                            break;
                        case "Exit":
                            if (JOptionPane.showConfirmDialog(frame, "Confirm if you want to exit", "Login system",
                                    JOptionPane.YES_NO_OPTION) == JOptionPane.YES_NO_OPTION)
                                System.exit(0);
                            break;
                        default:
                            break;
                    }
                }
        );
        btnLogout.setBounds(12, i1, 97, 25);

        return btnLogout;
    }

    private void onClickBtnAdd(TableSection tableSection) {
        int tabIndex = tableSection.requestPane.getSelectedIndex();
        System.out.println("Add");

        if (tabIndex == 0) {
            PopUpRequest addWindow = new PopUpRequest(0, tableSection.requestTab);
            addWindow.frame.setVisible(true);
        }
        // If in merchandise tab
        if (tabIndex == 1) {
            PopupMerchandise addWindow = new PopupMerchandise(0, tableSection.merchandiseTab);
            addWindow.frame.setVisible(true);
        }
    }

    private void onClickBtnDelete(TableSection tableSection) {
        System.out.println("Delete");
        int tabIndex = tableSection.requestPane.getSelectedIndex();
        BaseController controller = new BaseController(null);
        JTable table = null;
        if (tabIndex == 0) {
            controller = requestController;
            table = tableSection.requestTab;
        }
        if (tabIndex == 1) {
            controller = merchandiseController;
            table = tableSection.merchandiseTab;
        }

        assert table != null;
        DefaultTableModel model = (DefaultTableModel) table.getModel();

        int selectedRowIndex = table.getSelectedRow();
        if (selectedRowIndex != -1) {
            int answer = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete this?",
                    "Confirmation message", JOptionPane.YES_NO_OPTION);
            if (answer == 0) {
                String id = model.getValueAt(selectedRowIndex, 0).toString();
                ServiceResponse response = controller.delete(id);
                if (response.Success) {
					JOptionPane.showMessageDialog(null, "Success!", "Success", JOptionPane.PLAIN_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null, response.Message, "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        } else {
            // not choose any row
            JOptionPane.showMessageDialog(null, "No row is selected!", "Error", JOptionPane.ERROR_MESSAGE);
        }
        refresh(tableSection);
    }

    private void onClickBtnEdit(TableSection tableSection) {
        DefaultTableModel model;
        int tabIndex = tableSection.requestPane.getSelectedIndex();

        // If in site tab
        if (tabIndex == 0) {
            PopUpRequest editWindow = new PopUpRequest(1, tableSection.requestTab);
            model = (DefaultTableModel) tableSection.requestTab.getModel();
            int selectedRowIndex = tableSection.requestTab.getSelectedRow();
            if (selectedRowIndex != -1) {
                editWindow.requestID.setText(model.getValueAt(selectedRowIndex, 0).toString());

                ServiceResponse responseMerchandise = merchandiseController.getById(model.getValueAt(selectedRowIndex, 1).toString());
                if (responseMerchandise.Data.size() == 1) {
                    Merchandise merchandise = (Merchandise)responseMerchandise.Data.get(0);
                    editWindow.merchandiseId.getModel().setSelectedItem(merchandise);
                }

                ServiceResponse responseEmployee = employeeController.getById(model.getValueAt(selectedRowIndex, 2).toString());
                if (responseEmployee.Data.size() == 1) {
                    Employee site = (Employee) responseEmployee.Data.get(0);
                    editWindow.employeeID.getModel().setSelectedItem(site);
                }

                ServiceResponse responseCustomer = customerController.getById(model.getValueAt(selectedRowIndex, 3).toString());
                if (responseCustomer.Data.size() == 1) {
                    Customer site = (Customer) responseCustomer.Data.get(0);
                    editWindow.customerID.getModel().setSelectedItem(site);
                }

                editWindow.desiredDate.setDateFormatString("yyyy-MM-dd");
                Date date = (Date) model.getValueAt(selectedRowIndex, 4);
                editWindow.desiredDate.setDate(date);

                editWindow.quantity.setText(model.getValueAt(selectedRowIndex, 5).toString());

                editWindow.frame.setVisible(true);
            } else {
                JOptionPane.showMessageDialog(null, "No row is selected!", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        // If in merchandise tab
        if (tabIndex == 1) {
            PopupMerchandise editWindow = new PopupMerchandise(1, tableSection.merchandiseTab);
            model = (DefaultTableModel) tableSection.merchandiseTab.getModel();
            int selectedRowIndex = tableSection.merchandiseTab.getSelectedRow();
            if (selectedRowIndex != -1) {
                editWindow.merchandiseId.setText(model.getValueAt(selectedRowIndex, 0).toString());
                editWindow.merchandiseName.setText(model.getValueAt(selectedRowIndex, 1).toString());
                if (model.getValueAt(selectedRowIndex, 2) != null)
                        editWindow.unit.setText(model.getValueAt(selectedRowIndex, 2).toString())
                ;

                editWindow.frame.setVisible(true);
            } else {
                JOptionPane.showMessageDialog(null, "No row is selected!", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private JButton addCRUDBtn(String name, int i) {
        JButton btn = new JButton(name);
        btn.addActionListener( event -> {
            System.out.println(tableSection.requestPane.getSelectedIndex());
            switch (name) {
                case "Add" :
                    onClickBtnAdd(tableSection);
                    break;
                case "Delete" :
                    onClickBtnDelete(tableSection);
                    break;
                case "Edit" :
                    onClickBtnEdit(tableSection);
                    break;

            }
        });

        btn.setBounds(i, 406, 97, 25);

        return btn;
    }

    protected void refresh(TableSection tableSection) {
        int tabIndex = tableSection.requestPane.getSelectedIndex();
        if (tabIndex == 0) {
            ServiceResponse responseSite = requestController.getAll();
            Object[][] listSite = new Object[responseSite.Data.size()][];
            for (int i = 0; i < responseSite.Data.size(); i++) {
                Request site = (Request) responseSite.Data.get(i);
                listSite[i] = new Object[]{ site.getRequestID(),
                        site.getMerchandiseID(), site.getEmployeeID(), site.getCustomerID(),
                        site.getDate(), site.getQuantity()};
            }
            tableSection.requestTab.setModel(new DefaultTableModel(listSite,
                    new String[]{"ID", "Merchandise ID", "Employee ID", "Customer ID", "Desired Date", "Quantity"}) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    // all cells false
                    return false;
                }
            });
        }

        // If in merchandise tab
        if (tabIndex == 1) {
            ServiceResponse responseMerchandise = merchandiseController.getAll();
            Object[][] listMerchandise = new Object[responseMerchandise.Data.size()][];
            for (int i = 0; i < responseMerchandise.Data.size(); i++) {
                Merchandise merchandise = (Merchandise) responseMerchandise.Data.get(i);
                listMerchandise[i] = new Object[]{merchandise.getMerchandiseId(), merchandise.getMerchandiseName(),
                        merchandise.getUnit()};
            }
            tableSection.merchandiseTab.setModel(new DefaultTableModel(listMerchandise,
                    new String[]{"Merchandise id", "Merchandise name", "Unit"}) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    // all cells false
                    return false;
                }
            });
        }
    }
}
