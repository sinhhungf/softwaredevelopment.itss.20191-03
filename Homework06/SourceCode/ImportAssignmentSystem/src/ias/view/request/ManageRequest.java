package ias.view.request;

import ias.common.ServiceResponse;
import ias.controller.MerchandiseController;
import ias.controller.RequestController;
import ias.model.Merchandise;
import ias.model.Request;
import ias.repository.MerchandiseRepository;
import ias.repository.RequestRepository;
import ias.view.request.PopUpRequest;
import org.hamcrest.CoreMatchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;

import java.util.Date;

public class ManageRequest {
    @Rule
    public ErrorCollector collector = new ErrorCollector();

    private static RequestController requestController = new RequestController(new RequestRepository(new Request()));
    private static MerchandiseController merchandiseController = new MerchandiseController(
            new MerchandiseRepository(new Merchandise()));


    //	#region test site
    @Test
    public void testValidateRequest() {
        PopUpRequest editWindow = new PopUpRequest(1, null);
        editWindow.requestID.setText("SITE0001");
        editWindow.merchandiseId.setSelectedItem("1");
        editWindow.employeeID.setSelectedItem("1");
        editWindow.customerID.setSelectedItem("1");
        editWindow.quantity.setText("100");
        collector.checkThat("Test validate popup request", editWindow.validateBeforeSave(), CoreMatchers.equalTo(true));
    }


    @Test
    public void testInsertRequest() {
        Request request = new Request(new Date(), "TESTID", "1", "1", "1", new Date(), 100);
        ServiceResponse response = requestController.insert(request);
        collector.checkThat("Test insert request", response.Success, CoreMatchers.equalTo(true));
    }

    @Test
    public void testEditRequest() {
        Request request = new Request(new Date(), "TESTID", "1", "1", "1", new Date(), 100);
        ServiceResponse response = requestController.update("TESTID", request);
        collector.checkThat("Test update site", response.Success, CoreMatchers.equalTo(true));
    }


    @Test
    public void testGetRequest() {
        ServiceResponse response = requestController.getById("1");
        Request site = (Request)response.Data.get(0);
        collector.checkThat("Test get request by id", site.getQuantity(), CoreMatchers.equalTo(12));
    }

    @Test
    public void testDeleteRequest() {
        ServiceResponse response = requestController.delete("TESTID");
        collector.checkThat("Test delete request", response.Success, CoreMatchers.equalTo(true));
    }


}
