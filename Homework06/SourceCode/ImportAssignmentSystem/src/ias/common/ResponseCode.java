package ias.common;

public enum ResponseCode {
	Success,
	Error,
	Duplicate,
	InvalidData,
	NotFound,
	Database
}