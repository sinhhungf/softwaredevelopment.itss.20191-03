package ias.controller;

import ias.model.InstockMerchandise;
import ias.repository.BaseRepository;
import ias.repository.InstockMerchandiseRepository;

public class InstockMerchandiseController extends BaseController {
	private InstockMerchandiseRepository merchandiseRepository = new InstockMerchandiseRepository(new InstockMerchandise());
	
	public InstockMerchandiseController(BaseRepository baseRepository) {
		super(baseRepository);
	}
}
