package ias.controller;

import ias.common.MessageResource;
import ias.common.ResponseCode;
import ias.common.ServiceResponse;
import ias.model.BaseModel;
import ias.repository.BaseRepository;

public class BaseController {
	private BaseRepository repository;

	public BaseController(BaseRepository baseRepository) {
		this.repository = baseRepository;
	}

	// #region Public Methods
	/*
	 * action get all data model
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	public ServiceResponse getAll() {
		ServiceResponse response = new ServiceResponse();
		try {
			response = repository.getAll();
		} catch (Exception e) {
			response.onError(ResponseCode.Error, MessageResource.ERROR_GET_ALL, null);
			e.printStackTrace();
		}
		return response;
	}

	/*
	 * action get record by primary key id
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	public ServiceResponse getById(String id) {
		ServiceResponse response = new ServiceResponse();
		try {
			response = repository.getById(id);
		} catch (Exception e) {
			response.onError(ResponseCode.Error, MessageResource.ERROR_GET_ALL, null);
			e.printStackTrace();
		}
		return response;
	}
	
	/*
	 * action get record by primary key id
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	public ServiceResponse getByView(String viewName) {
		ServiceResponse response = new ServiceResponse();
		try {
			response = repository.getByViewName(viewName);
		} catch (Exception e) {
			response.onError(ResponseCode.Error, MessageResource.ERROR_GET_ALL, null);
			e.printStackTrace();
		}
		return response;
	}

	/*
	 * action get all data model
	 * 
	 * @override
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	public ServiceResponse insert(BaseModel model) {
		ServiceResponse response = new ServiceResponse();
		try {
			response = validateBeforeInsert(model);
			if (!response.Success) {
				return response;
			}
			response = repository.insert(model);
		} catch (Exception e) {
			response.onError(ResponseCode.Error, MessageResource.ERROR_DELETE, null);
			e.printStackTrace();
		}
		return response;
	}

	/*
	 * action get all data model
	 * 
	 * @override
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	public ServiceResponse update(String id, BaseModel model) {
		ServiceResponse response = new ServiceResponse();
		try {
			response = validateBeforeUpdate(model);
			if (!response.Success) {
				return response;
			}
			response = repository.update(id, model);
		} catch (Exception e) {
			response.onError(ResponseCode.Error, MessageResource.ERROR_DELETE, null);
			e.printStackTrace();
		}
		return response;
	}

	/*
	 * action get all data model
	 * 
	 * @override
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	public ServiceResponse delete(String id) {
		ServiceResponse response = new ServiceResponse();
		try {
			response = repository.delete(id);
		} catch (Exception e) {
			response.onError(ResponseCode.Error, MessageResource.ERROR_DELETE, null);
			e.printStackTrace();
		}
		return response;
	}
//	#endregion Public Methods

//	#region Protected Methods
	/*
	 * validate custom rules before insert model
	 * 
	 * @override
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	protected ServiceResponse validateBeforeInsert(BaseModel model) {
		return new ServiceResponse();
	}
	
	/*
	 * validate custom rules before update model
	 * 
	 * @override
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	protected ServiceResponse validateBeforeUpdate(BaseModel model) {
		return new ServiceResponse();
	}
	
	/*
	 * validate custom rules before delete model
	 * 
	 * @override
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	protected ServiceResponse validateBeforeDelete(BaseModel model) {
		return new ServiceResponse();
	}
//	#endregion Protected Methods

}
