package ias.controller;

import ias.model.Site;
import ias.repository.BaseRepository;
import ias.repository.SiteRepository;

public class SiteController extends BaseController {
	private SiteRepository siteRepository = new SiteRepository(new Site());
	
	public SiteController(BaseRepository baseRepository) {
		super(baseRepository);
	}
}
