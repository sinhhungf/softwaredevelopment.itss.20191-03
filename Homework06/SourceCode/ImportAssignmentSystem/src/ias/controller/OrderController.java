package ias.controller;

import ias.model.Order;
import ias.repository.BaseRepository;
import ias.repository.OrderRepository;

public class OrderController extends BaseController {
	private OrderRepository orderRepository = new OrderRepository(new Order());
	
	public OrderController(BaseRepository baseRepository) {
		super(baseRepository);
	}
}

