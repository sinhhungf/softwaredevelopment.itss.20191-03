package ias.test;

import ias.common.ServiceResponse;
import ias.controller.InstockMerchandiseController;
import ias.controller.MerchandiseController;
import ias.controller.SiteController;
import ias.controller.StockController;
import ias.model.InstockMerchandise;
import ias.model.Merchandise;
import ias.model.Site;
import ias.model.Stock;
import ias.repository.InstockMerchandiseRepository;
import ias.repository.MerchandiseRepository;
import ias.repository.SiteRepository;
import ias.repository.StockRepository;

public class Test {
	public static SiteController siteController = new SiteController(new SiteRepository(new Site()));
	public static MerchandiseController merchandiseController = new MerchandiseController(
			new MerchandiseRepository(new Merchandise()));
	public static StockController stockController = new StockController(
			new StockRepository(new Stock()));

	public static void main(String[] args) {
//		ServiceResponse response = siteController.getAll();
//		ServiceResponse response = siteController.getByView("view_supply_site");
//		Site site = new Site(null, "3", "Shopee", 8, 19);
//		ServiceResponse response = siteController.update("3", site);
//		Merchandise merchandise = new Merchandise(null, "9", "Phone", "unit 2");
//		ServiceResponse response = merchandiseController.insert(merchandise);
//		ServiceResponse response = merchandiseController.update("19", merchandise);
//		InstockMerchandise instockMerchandise = new InstockMerchandise("1", "3", 18);
		ServiceResponse response = stockController.getByView("view_order_merchandise");
//		ServiceResponse response = instockMerchandiseController.update("10", instockMerchandise);
//		System.out.println(response.Success);
//		System.out.println(response.Message);
//		System.out.println(response.Code);
		
		
//		SiteCode = siteCode;
//		EmployeeCode = employeCode;
//		MerchandiseCode = merchandiseCode;
//		MerchandiseName = merchandiseName;
//		OrderQuantity = orderQuantity;
//		DeliveryMean = deliveryMean;
//		ReturnQuantity = returnQuantity;
//		State = state
		
		for (int i = 0; i < response.Data.size(); i += 1) {
//			Site result = (Site) response.Data.get(i);
			Stock result = (Stock) response.Data.get(i);
			System.out.println(result.getMerchandiseCode());
			System.out.println(result.getMerchandiseName());
			System.out.println(result.getOrderQuantity());
			System.out.println(result.getModifiedDate());
			System.out.println(result.getSiteCode());
		}
	}
}