package ias.model;

import java.util.Date;

public class Merchandise extends BaseModel {
	protected String MerchandiseId;
	protected String MerchandiseName;
	protected String Unit;
	
	public Merchandise() {
		super(new Date());
		MerchandiseId = "";
		MerchandiseName = "";
		Unit = "";
	}
	
	public Merchandise(Date modifiedDate, String merchandiseId, String merchandiseName, String unit) {
		super(modifiedDate);
		MerchandiseId = merchandiseId;
		MerchandiseName = merchandiseName;
		Unit = unit;
	}

	public String getMerchandiseId() {
		return MerchandiseId;
	}

	public void setMerchandiseId(String merchandiseId) {
		MerchandiseId = merchandiseId;
	}

	public String getMerchandiseName() {
		return MerchandiseName;
	}

	public void setMerchandiseName(String merchandiseName) {
		MerchandiseName = merchandiseName;
	}

	public String getUnit() {
		return Unit;
	}

	public void setUnit(String unit) {
		Unit = unit;
	}
	
	public String getTableName() {
		return "merchandise";
	}
	
	public String getPrimaryKeyFieldName() {
		return "id";
	}
	
	public String getPrimaryKeyFieldValue() {
		return this.MerchandiseId;
	}
	
	@Override
	public String toString() {
		return this.MerchandiseId;
	}
}
