package ias.model;

import java.util.Date;

public class Stock extends BaseModel{
	private String SiteCode;
	private String MerchandiseCode;
	private String MerchandiseName;
	private int OrderQuantity;
	private String DeliveryMean;
	private int ReturnQuantity;
	private int State;
	
	public Stock() {
		super(new Date());
		SiteCode = "";
		MerchandiseCode = "";
		MerchandiseName = "";
		DeliveryMean = "";
		ReturnQuantity = 0;
		State = 0;
	}
	
	public Stock(Date modifiedDate, String siteCode,
					String merchandiseCode, String merchandiseName ,int orderQuantity, 
					 int returnQuantity, String deliveryMean, int state) {
		super(modifiedDate);
		SiteCode = siteCode;
		MerchandiseCode = merchandiseCode;
		MerchandiseName = merchandiseName;
		OrderQuantity = orderQuantity;
		DeliveryMean = deliveryMean;
		ReturnQuantity = returnQuantity;
		State = state;
	}
	
	public String getSiteCode() {
		return SiteCode;
	}
	
	public void setSiteCode(String siteCode) {
		SiteCode = siteCode;
	}

//	public String getEmployeeCode() {
//		return EmployeeCode;
//	}
//	
//	public void setEmployeeCode(String employeeCode) {
//		EmployeeCode = employeeCode;
//	}
//	
	public int orderQuantity() {
		return OrderQuantity;
	}
	
	public int getOrderQuantity() {
		return OrderQuantity;
	}

	public void setOrderQuantity(int orderQuantity) {
		OrderQuantity = orderQuantity;
	}

	public String getMerchandiseCode() {
		return MerchandiseCode;
	}
	
	public void setMerchandiseCode(String merchandiseCode) {
		MerchandiseCode = merchandiseCode;
	}
	
	public String getMerchandiseName() {
		return MerchandiseName;
	}

	public void setMerchandiseName(String merchandiseName) {
		MerchandiseName = merchandiseName;
	}

	public String getDeliveryMean() {
		return DeliveryMean;
	}

	public void setDeliveryMean(String deliveryMean) {
		DeliveryMean = deliveryMean;
	}

	public int getReturnQuantity() {
		return ReturnQuantity;
	}

	public void setReturnQuantity(int returnQuantity) {
		ReturnQuantity = returnQuantity;
	}

	public int getState() {
		return State;
	}

	public void setState(int state) {
		State = state;
	}
	
//	public String getTableName() {
//		return "stock";
//	}
}
