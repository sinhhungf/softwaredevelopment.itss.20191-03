package ias.model;

import java.util.Date;

public class Site extends BaseModel {
	private String SiteId;
	private String SiteName;
	private int DeliveryByShip;
	private int DeliveryByAir;
	
	public Site() {
		super(new Date());
		SiteId = "";
		SiteName = "";
		DeliveryByShip = 0;
		DeliveryByAir = 0;
	}
	
	public Site(Date modifiedDate, String siteId, String siteName, int deliveryByShip, int deliveryByAir) {
		super(modifiedDate);
		SiteId = siteId;
		SiteName = siteName;
		DeliveryByShip = deliveryByShip;
		DeliveryByAir = deliveryByAir;
	}
	
	public String getSiteId() {
		return SiteId;
	}
	public void setSiteId(String siteId) {
		SiteId = siteId;
	}
	public String getSiteName() {
		return SiteName;
	}
	public void setSiteName(String siteName) {
		SiteName = siteName;
	}
	public int getDeliveryByShip() {
		return DeliveryByShip;
	}
	public void setDeliveryByShip(int deliveryByShip) {
		DeliveryByShip = deliveryByShip;
	}
	public int getDeliveryByAir() {
		return DeliveryByAir;
	}
	public void setDeliveryByAir(int deliveryByAir) {
		DeliveryByAir = deliveryByAir;
	}
	
	public String getTableName() {
		return "site";
	}
	
	public String getPrimaryKeyFieldName() {
		return "id";
	}
	
	public String getPrimaryKeyFieldValue() {
		return this.SiteId;
	}
	
	@Override
	public String toString() {
		return this.SiteId;
	}
}
