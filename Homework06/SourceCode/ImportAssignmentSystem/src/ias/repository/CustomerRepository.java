package ias.repository;

import ias.model.Customer;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomerRepository extends BaseRepository {
    public CustomerRepository(Customer model) {
        super(model);
    }

    public Customer getModel(ResultSet resultSet) {
        try {
            return new Customer(resultSet.getDate("modified_date"),
                    resultSet.getString("id"),
                    resultSet.getString("customer_name"),
                    resultSet.getString("customer_address")
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
