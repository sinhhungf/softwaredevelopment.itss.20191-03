package ias.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import ias.model.BaseModel;
import ias.model.InstockMerchandise;
import ias.model.Site;

public class SiteRepository extends BaseRepository {
	public SiteRepository(Site model) {
		super(model);
	}

	/*
	 * action get all data model
	 * 
	 * @override
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	protected Site getModel(ResultSet resultSet) {
		try {
			return new Site(resultSet.getDate("modified_date"), resultSet.getString("id"), resultSet.getString("site_name"),
					resultSet.getInt("deliver_by_air"), resultSet.getInt("deliver_by_ship"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * function generate query sql insert record template
	 * "<value>, <value>, <value>, <value>, ..."
	 * 
	 * @param {BaseModel} model - model that extends BaseModel
	 * 
	 * @override
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	protected String generateInsertRecord(BaseModel model) {
		String insertRecord = "'%s', '%s', %s, %s, '%s'";
		Site site = (Site) model;
		insertRecord = String.format(insertRecord, 
				site.getSiteId(), 
				site.getSiteName(), 
				site.getDeliveryByAir(),
				site.getDeliveryByShip(),
				new Date().toString());
		return insertRecord;
	}

	/*
	 * function generate query sql update record
	 * 
	 * @param {BaseModel} model - model that extends BaseModel
	 * 
	 * @override
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	protected String generateUpdateRecord(BaseModel model) {
		String updateRecord = "id = '%s', site_name = '%s', deliver_by_air = %s, deliver_by_ship = %s, modified_date = '%s'";
		Site site = (Site) model;
		updateRecord = String.format(updateRecord, 
				site.getSiteId(), 
				site.getSiteName(), 
				site.getDeliveryByAir(),
				site.getDeliveryByShip(), new Date());
		return updateRecord;
	}
}