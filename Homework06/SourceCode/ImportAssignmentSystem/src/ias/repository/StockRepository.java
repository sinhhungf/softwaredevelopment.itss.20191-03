package ias.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import ias.model.BaseModel;
import ias.model.Site;
import ias.model.Stock;

public class StockRepository extends BaseRepository{
	public StockRepository(Stock model) {
		super(model);
	}
	
	/*
	 * action get all data model
	 * @ override
	 * */
	protected Stock getViewModel(ResultSet resultSet) {
		try {
			return new Stock(resultSet.getDate("modified_date"),
					resultSet.getString("site_id"),
					resultSet.getString("merchandise_id"), resultSet.getString("merchandise_name"),
					resultSet.getInt("order_quantity"), resultSet.getInt("return_quantity"), 
					resultSet.getString("delivery_mean"), resultSet.getInt("state")
					);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;	
	}
	
	protected String generateInsertRecord(BaseModel model) {
		String insertRecord = "'%s', '%s', %s, %s";
		Site site = (Site) model;
		insertRecord = String.format(insertRecord, site.getSiteId(), site.getSiteName(), site.getDeliveryByAir(),
				site.getDeliveryByShip());
		return insertRecord;
	}

	/*
	 * function generate query sql update record
	 * 
	 * @param {BaseModel} model - model that extends BaseModel
	 * 
	 * @override
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	protected String generateUpdateRecord(BaseModel model) {
		String updateRecord = "site_code = '%s', site_name = '%s', delivery_by_air = %s, delivery_by_ship = %s";
		Site site = (Site) model;
		updateRecord = String.format(updateRecord, site.getSiteId(), site.getSiteName(), site.getDeliveryByAir(),
				site.getDeliveryByShip());
		return updateRecord;
	}
	
}
