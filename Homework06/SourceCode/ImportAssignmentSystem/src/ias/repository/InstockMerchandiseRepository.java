package ias.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import ias.model.BaseModel;
import ias.model.InstockMerchandise;
import ias.model.InstockMerchandise;

public class InstockMerchandiseRepository extends BaseRepository {
	public InstockMerchandiseRepository(InstockMerchandise model) {
		super(model);
	}

	/*
	 * action get all data model
	 * 
	 * @override
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	protected InstockMerchandise getViewModel(ResultSet resultSet) {
		try {
			return new InstockMerchandise(resultSet.getDate("modified_date"), resultSet.getString("id") , 
					resultSet.getString("merchandise_id"),
					resultSet.getString("merchandise_name"), resultSet.getString("unit"),
					resultSet.getString("site_id"), resultSet.getString("site_name"),
					resultSet.getInt("instock_quantity"), resultSet.getInt("deliver_by_ship"),
					resultSet.getInt("deliver_by_air"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * function generate query sql insert record template
	 * "<value>, <value>, <value>, <value>, ..."
	 * 
	 * @param {BaseModel} model - model that extends BaseModel
	 * 
	 * @override
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	protected String generateInsertRecord(BaseModel model) {
		String insertRecord = "'%s', '%s', %s, '%s', '%s'";
		InstockMerchandise merchandise = (InstockMerchandise) model;
		insertRecord = String.format(insertRecord, merchandise.getSiteId(), merchandise.getMerchandiseId(),
				merchandise.getInstockQuantity(), new Date().toString(), getAutoId());
		return insertRecord;
	}

	/*
	 * function generate query sql update record
	 * 
	 * @param {BaseModel} model - model that extends BaseModel
	 * 
	 * @override
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	protected String generateUpdateRecord(BaseModel model) {
		String updateRecord = "site_id = '%s', merchandise_id = '%s', instock_quantity = %s, modified_date = '%s'";
		InstockMerchandise merchandise = (InstockMerchandise) model;
		updateRecord = String.format(updateRecord, merchandise.getSiteId(),
				merchandise.getMerchandiseId(), merchandise.getInstockQuantity(), new Date());
		return updateRecord;
	}
}