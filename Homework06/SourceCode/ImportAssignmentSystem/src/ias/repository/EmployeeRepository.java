package ias.repository;

import ias.model.Employee;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeRepository extends BaseRepository {
    public EmployeeRepository(Employee model) {
        super(model);
    }

    public Employee getModel(ResultSet resultSet) {
        try {
            return new Employee(resultSet.getDate("modified_date"),
                    resultSet.getString("id"),
                    resultSet.getString("employee_name"),
                    resultSet.getString("role"),
                    resultSet.getString("user_name"),
                    resultSet.getString("password")
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
