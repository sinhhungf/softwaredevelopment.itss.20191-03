package ias.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import org.postgresql.util.PSQLException;

import ias.common.Constant;
import ias.common.ResponseCode;
import ias.common.ServiceResponse;
import ias.model.BaseModel;

/*
 * NCThanh-TODO: generate function select join by foreign key
 */

public abstract class BaseRepository {
	private BaseModel model = new BaseModel(new Date()) {
	};

	/*
	 * generate an auto increment id
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	protected String getAutoId() {
		return UUID.randomUUID().toString();
	}

	// #region Public methods

	/*
	 * get all data from table
	 * 
	 * @param {BaseModel} model - model that extends BaseModel
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	public ServiceResponse getAll() {
		ServiceResponse response = new ServiceResponse();
		try {
			Connection conn = DbConnection.getConnection();
			String sql = generateSelectAll();
			PreparedStatement preparedStatement = conn.prepareStatement(sql);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet == null) {
				return response.onError(ResponseCode.NotFound, "Not Found", null);
			}
			while (resultSet.next()) {
				response.Data.add(getModel(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return response;
	}

	/*
	 * get data by id from table
	 * 
	 * @param {BaseModel} model - model that extends BaseModel
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	public ServiceResponse getById(String id) {
		ServiceResponse response = new ServiceResponse();
		try {
			Connection conn = DbConnection.getConnection();
			String sql = generateSelectById(id);
			PreparedStatement preparedStatement = conn.prepareStatement(sql);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet == null) {
				return response.onError(ResponseCode.NotFound, "Not Found", null);
			}
			while (resultSet.next()) {
				response.Data.add(getModel(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return response;
	}

	/*
	 * get data from view table
	 * 
	 * @param {BaseModel} model - model that extends BaseModel
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	public ServiceResponse getByViewName(String viewName) {
		ServiceResponse response = new ServiceResponse();
		try {
			Connection conn = DbConnection.getConnection();
			String sql = generateSelectByViewName(viewName);
			PreparedStatement preparedStatement = conn.prepareStatement(sql);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet == null) {
				return response.onError(ResponseCode.NotFound, "Not Found", null);
			}
			while (resultSet.next()) {
				response.Data.add(getViewModel(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return response;
	}

	/*
	 * execute a sql query and return a result set
	 * 
	 * @param {String} sql - sql select query
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	public ResultSet executeQuery(String sql) {
		ResultSet resultSet = null;
		try {
			Connection conn = DbConnection.getConnection();
			PreparedStatement preparedStatement = conn.prepareStatement(sql);
			resultSet = preparedStatement.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultSet;
	}

	/*
	 * insert a new record into table
	 * 
	 * @override
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	public ServiceResponse insert(BaseModel model) {
		ServiceResponse response = new ServiceResponse();
		Connection conn = null;
		try {
			conn = DbConnection.getConnection();
			// Assume a valid connection object conn
			conn.setAutoCommit(false);
			String sql = "INSERT INTO %s.%s VALUES (%s)";
			sql = String.format(sql, Constant.DefaultSchemaName, model.getTableName(), generateInsertRecord(model));
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);
			// If there is no error.
			conn.commit();
		} catch (PSQLException e) {
			return handleSQLException(e);
		} catch (Exception e) {
			e.printStackTrace();
			// If there is any error.
			try {
				if (conn != null) {
					conn.rollback();
					conn.setAutoCommit(true);
				}
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return response.onError(ResponseCode.Database, e.getMessage(), null);
		}
		ArrayList<BaseModel> returnData = new ArrayList<BaseModel>();
		returnData.add(model);
		return response.onSuccess(returnData);
	}

	/*
	 * update a new record into table
	 * 
	 * @override
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	public ServiceResponse update(String id, BaseModel model) {
		ServiceResponse response = new ServiceResponse();
		Connection conn = null;
		try {
			conn = DbConnection.getConnection();
			// Assume a valid connection object conn
			conn.setAutoCommit(false);
			String sql = "UPDATE %s.%s AS e SET %s WHERE e.%s = '%s'";
			sql = String.format(sql, Constant.DefaultSchemaName, model.getTableName(), generateUpdateRecord(model),
					model.getPrimaryKeyFieldName(), id);
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);
			// If there is no error.
			conn.commit();
		} catch (PSQLException e) {
			return handleSQLException(e);
		} catch (Exception e) {
			e.printStackTrace();
			// If there is any error.
			try {
				if (conn != null) {
					conn.rollback();
					conn.setAutoCommit(true);
				}
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return response.onError(ResponseCode.Database, e.getMessage(), null);
		}
		ArrayList<BaseModel> returnData = new ArrayList<BaseModel>();
		returnData.add(model);
		return response.onSuccess(returnData);
	}

	/*
	 * delete data by id from table
	 * 
	 * @param {String} id
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	public ServiceResponse delete(String id) {
		ServiceResponse response = new ServiceResponse();
		Connection conn = null;
		try {
			conn = DbConnection.getConnection();
			// Assume a valid connection object conn
			conn.setAutoCommit(false);
			String sql = generateDeleteById(id);
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);
			// If there is no error.
			conn.commit();
		} catch (PSQLException e) {
			return handleSQLException(e);
		} catch (Exception e) {
			e.printStackTrace();
			// If there is any error.
			try {
				if (conn != null) {
					conn.rollback();
					conn.setAutoCommit(true);
				}
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return response.onError(ResponseCode.Database, e.getMessage(), null);
		}
		return response;
	}

	/*
	 * execute a insert/update/delete query custom
	 * 
	 * @param {String} id
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	public ServiceResponse executeCommand(String sql) {
		ServiceResponse response = new ServiceResponse();
		Connection conn = null;
		try {
			conn = DbConnection.getConnection();
			// Assume a valid connection object conn
			conn.setAutoCommit(false);
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);
			// If there is no error.
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			// If there is any error.
			try {
				if (conn != null) {
					conn.rollback();
					conn.setAutoCommit(true);
				}
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return response.onError(ResponseCode.Database, e.getMessage(), null);
		}
		return response;
	}
	// #endregion Public methods

	// #region Protected
	/*
	 * action get all data model
	 * 
	 * @override
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	protected BaseModel getModel(ResultSet resultSet) {
		return null;
	}

	/*
	 * action get all data model by view
	 * 
	 * @override
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	protected BaseModel getViewModel(ResultSet resultSet) {
		return null;
	}

	/*
	 * function generate query sql insert record
	 * 
	 * @param {BaseModel} model - model that extends BaseModel
	 * 
	 * @override
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	protected String generateInsertRecord(BaseModel model) {
		return "";
	}

	/*
	 * function generate query sql update record
	 * 
	 * @param {BaseModel} model - model that extends BaseModel
	 * 
	 * @override
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	protected String generateUpdateRecord(BaseModel model) {
		return "";
	}
	// #endregion Protected

	// #region Private methods

	/*
	 * function generate query sql select all data from table
	 * 
	 * @param {BaseModel} model - model that extends BaseModel
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	private String generateSelectAll() {
		String sql = "SELECT %s.%s.* FROM %s.%s WHERE 1=1 ORDER BY modified_date DESC";
		sql = String.format(sql, Constant.DefaultSchemaName, model.getTableName(), Constant.DefaultSchemaName,
				model.getTableName());
		return sql;
	}

	/*
	 * function generate query sql select data by primary key from table
	 * 
	 * @param {BaseModel} model - model that extends BaseModel
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	private String generateSelectById(String id) {
		String sql = "SELECT %s.%s.* FROM %s.%s WHERE %s = '%s'";
		sql = String.format(sql, Constant.DefaultSchemaName, model.getTableName(), Constant.DefaultSchemaName,
				model.getTableName(), model.getPrimaryKeyFieldName(), id);
		return sql;
	}

	/*
	 * function generate query sql select data by primary key from table
	 * 
	 * @param {BaseModel} model - model that extends BaseModel
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	private String generateSelectByViewName(String viewName) {
		String sql = "SELECT * FROM %s.%s ORDER BY modified_date DESC";
		sql = String.format(sql, Constant.DefaultSchemaName, viewName);
		return sql;
	}

	/*
	 * function generate query sql delete data by primary key from table
	 * 
	 * @param {BaseModel} model - model that extends BaseModel
	 * 
	 * @author NCThanh - 28/11/2019
	 */
	private String generateDeleteById(String id) {
		String sql = "DELETE FROM %s.%s WHERE %s = '%s'";
		sql = String.format(sql, Constant.DefaultSchemaName, model.getTableName(), model.getPrimaryKeyFieldName(), id);
		return sql;
	}
	
	private ServiceResponse handleSQLException(PSQLException e) {
		ServiceResponse response = new ServiceResponse();
//		e.printStackTrace();
		String state = e.getSQLState();
		switch (state) {
		case "23505":
			return response.onError(ResponseCode.Duplicate, e.getMessage(), null);
		}
		return response;
	}
	// #endregion Private methods

	public BaseRepository(BaseModel model) {
		this.model = model;
	}
}
