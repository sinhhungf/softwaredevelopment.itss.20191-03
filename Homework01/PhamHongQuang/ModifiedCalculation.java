 package itss20191.group03;
+import java.util.Scanner;
 
 public class Calculation {
 	int first;
 	int second;
 
 	public static void main(String[] args) {
+		
 		// ask use to enter 2 number
 		// display results
+		Calculation cal = new Calculation();
+		
+		Scanner scanner = new Scanner(System.in);
+		System.out.print("Enter first number: ");
+		
+		int first = scanner.nextInt();
+		
+		System.out.print("Enter second number: ");
+		int second = scanner.nextInt();
+		
+		System.out.println("First number = " + first);
+		System.out.println("Second number = " + second);
+		
+		System.out.println("Sum of two numbers = " + cal.calcAddition(first, second));
+		System.out.println("Subtraction of two numbers = " + cal.calcSubstraction(first, second));
+		System.out.println("Multiplication of two numbers = " + cal.calcMultiplication(first, second));
+		System.out.println("Division of two numbers  = " + cal.calcDivision(first, second));
+		
+		scanner.close();
+
 	}
 	
 	public int calcAddition(int first, int second) {
 		return first+second;
 	}
 	
 	public int calcSubstraction(int first, int second) {
 		return first-second;
 	}
 	
 	public int calcMultiplication(int first, int second) {
 		return first*second;
 	}
 	
 	public double calcDivision(int first, int second) {
 		return first/second;
 	}
 
 }
