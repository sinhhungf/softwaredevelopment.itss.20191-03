 public class HelloWorld {
 
 	public static void main(String[] args) {
-		Scanner sc = new Scanner(System.in);
+		Scanner scanner = new Scanner(System.in);
 		System.out.println("Enter your name");
-		String name = sc.nextLine();
+		String yourName = scanner.nextLine();
 		
-		System.out.println("Hello, " + name);
+		System.out.println("Hello, " + yourName);
 	}
 
 }
