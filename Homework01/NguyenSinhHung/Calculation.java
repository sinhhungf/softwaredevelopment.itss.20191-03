import java.util.Scanner;

public class Calculation {
	int firstNumber;
	int secondNumber;

	public static void main(String[] args) {
		// Create a Calculation object
		Calculation cal = new Calculation();
		// Ask user to enter 2 number
		Scanner scanner = new Scanner( System.in );
		System.out.print( "Enter first number: " );
		
		int firstNumber = scanner.nextInt();
		
		System.out.print( "Enter second number: ");
		int secondNumber = scanner.nextInt();
		
		// Display 2 number on the screen
		System.out.println( "First number = " + firstNumber );
		System.out.println( "Second number = " + secondNumber );
		
		// Display Sum, difference, product and quotient
		System.out.println( "Sum of two numbers = " + cal.calcAddition( firstNumber , secondNumber ) );
		System.out.println( "Difference of two numbers = " + cal.calcSubstraction( firstNumber , secondNumber ) );
		System.out.println( "Product of two numbers = " + cal.calcMultiplication( firstNumber , secondNumber ) );
		System.out.println( "Quotient of two numbers  = " + cal.calcDivision( firstNumber , secondNumber ) );
		
		// Close Scanner
		scanner.close();
	}
	
	public int calcAddition(int first, int second) {
		return first + second;
	}
	
	public int calcSubstraction(int first, int second) {
		return first - second;
	}
	
	public int calcMultiplication(int first, int second) {
		return first * second;
	}
	
	public double calcDivision(int first, int second) {
		return first / second;
	}
}

