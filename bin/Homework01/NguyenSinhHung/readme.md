# Homework 1 Git Report
## Nguyen Sinh Hung - 20161960

 
There is no problem when I try to do most of the requests given:
  1. Clone repository to local system
  2. Add a file to local repository and put it onto Bitbucket
  3. Create a file on Bitbucket
  4. Pull changes from a remote repository
  5. Create a branch and make a change
  6. Fast-forward merging
  7. Push changes to Bitbucket

It'd be better if I screenshoted my work but I didn't. 😢😢😢

## Problems

1 problem occured when I first pushed my changes onto BitBucket.

```http
fatal: Not a git repository (or any of the parent directories): .git
```

I managed to solve this by following suggestions on this [link](https://stackoverflow.com/questions/4630704/receiving-fatal-not-a-git-repository-when-attempting-to-remote-add-a-git-repo). Specifically, I used `git init` before pushing it. 