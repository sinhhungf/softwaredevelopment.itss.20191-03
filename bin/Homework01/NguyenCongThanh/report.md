# Git practice with BitBucket
### By Nguyen Cong Thanh

- Create and checkout a new branch
- Add a file to local repository and commit
- Pull new changes from remote repository

![ncthanh-git-practice](./images/ncthanh-git-practice.png)

- Resolve conflicts in Eclipse

![ncthanh-git-practice](./images/ncthanh-resolve-conflict.png)

- Merge change from new branch to branch master
- Push changes to remote repository

![ncthanh-git-practice](./images/ncthanh-git-practice2.png)