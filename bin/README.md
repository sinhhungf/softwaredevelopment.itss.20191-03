# To chuc Repository

**To chuc Repository phai tuon thu dung nhu sau (neu khong se KHONG duoc cham diem)**

1. Repository co ten theo quy dinh tren slide
2. Trong Repository se co cac thu muc con tuong ung voi bai tap tung tuan duoc giao
  - Vi du: Homework01, Homework02
3. Bai tap nhom:  **luon phai co file Readme.txt** phan cong cong viec.
4. Trong moi thu muc HomeworkXX, tao cac thu muc con lan luot la cac thanh vien trong nhom
  - Vi du: **VuVietHung**, **PhamHongQuang**, etc.
  - **Tat ca ket qua ca nhan** se luu vao cac thu muc nay. Khong luu tu chiu trach nhiem
  - Trong thu muc nay, tao mot tep goi la **Group** luu ket qua chung cua ca nhom.
5.  
*Huong dan chi tiet co the xem o [day](https://www.dropbox.com/sh/51lcinhcvzc2vrw/AAAg8g-Z1ALdGKKQeExi75_La?dl=0&fbclid=IwAR19m3XlLwliQqtZgNJVkprjPNJMr1MdoXd4ZcWbYbyyNqgR01lEu27yLR0&preview=BaiTapTuan.pdf). Muc so 2 - Yeu cau*
