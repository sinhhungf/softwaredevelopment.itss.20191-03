2. Define Operations/ Methods:
	- Operations: Mang tinh thiet ke
	- Methods: Mang tinh cai dat

2.1 Define Operations:
	Neu co mot phuong thuc truyen tu Object nay sang Object khac

Dependency/Association/Aggregation/Inheritance:
	- Trong lop B co khai bao lop A ( quan he Association)
	- B khong lien ket voi A moi noi moi luc (Chi phu thuoc qua mot ham nao day) - Dependency
Association/Aggregation/Composition:
	- Aggregation/ Composition deu la subset cua Association
	- Composition la strong association: Lop con khong the ton tai neu khong co lop cha
	- Aggregation la weak association: Lop con co the ton tai doc lap voi lop cha


3. Nguyen tac SOLID
3.1 Nguyen ly mot nhiem vu(SINGLE-RESPONSIBILITY PRINCIPLE):
	-Moi lop nen chi dam nhiem mot nhiem vu
	- Code se de bao tri hon vi khong phai test lai tat ca cac nhiem vu
	- Neu code duoc dung o nhieu noi khac nhau -> tao mot class rieng cho no

3.2 Nguyen ly dong mo (open-closed principle)
	- Cac thuc the phan mem nen la OPEN cho cac mo rong nhung CLOSE voi cac sua doi 
	- Open for extension:"mot module(class) cung cap cac diem mo rong , cho phep thay doi cac hanh vi cua no"
	- Closed for modification:" khong can thay doi source cu de mo rong  "
	- Giai phap : tao cac lop abstract/interface cho cac phuong thuc co behaviour giong nhau

3.3 Nguyên lý thay thế Liskov (Liskov subtitution principle)
	- Không nên thay đổi ý nghĩa của phương thức trong lớp con
	- Cách giải quyết: Viết một lớp trừu tượng và các lớp kế thừa từ lớp đó 

3.4 Nguyên lý phân tách giao diện:
	- Một interface không nên chứa các phương thức mà lớp thực thi không cần thiết
	-Nên tách các fat-interface thành small interface gọi là role interface

3.5 Dependency Inversion Principle 
- Module tầng trên không nên phụ thuộc module tầng thấp
- Tránh phụ thuộc vòng: A phụ thuộc B và B phụ thuộc A 
VD: Lớp công tắc điện ở mức cao -> Lớp bóng điện ở mức thấp 
	Nếu công tắc phụ thuộc vào bóng đèn
Giải pháp: Tạo môt Interface của mức thấp, cho mức cao kế thừa interface đó


	
