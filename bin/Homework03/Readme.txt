Delegate tasks to members:
- Vũ Việt Hưng: draw general use case diagram and use case diagram for managing inventory
- Nguyễn Sinh Hùng: draw use case diagram for managing requests from customer
- Nguyễn Công Thành: draw use case diagram for managing sites
- Phạm Hồng Quang: draw use case diagram for managing orders
