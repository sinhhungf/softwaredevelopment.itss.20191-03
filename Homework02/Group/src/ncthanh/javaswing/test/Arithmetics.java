package ncthanh.javaswing.test;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Arithmetics {

    static void GridLayoutExample() {
        JFrame jFrame = new JFrame("GridLayout Example");

        JLabel firstNumber = new JLabel("First Number");
        JTextField firstNumberTextFiled = new JTextField();
        firstNumberTextFiled.setHorizontalAlignment(JTextField.RIGHT);

        JLabel secondNumber = new JLabel("Second Number");
        JTextField secondNumberTextFiled = new JTextField();
        secondNumberTextFiled.setHorizontalAlignment(JTextField.RIGHT);

        JLabel result = new JLabel("Result");
        JTextField resultTextFiled = new JTextField();
        resultTextFiled.setHorizontalAlignment(JTextField.RIGHT);
        resultTextFiled.setEditable(false);

        JButton btnAdd = new JButton("+");
        btnAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String x = firstNumberTextFiled.getText();
                String y = secondNumberTextFiled.getText();
                resultTextFiled.setText(Double.parseDouble(x) + Double.parseDouble(y) + "");
            }
        });

        // add component to JFrame
        jFrame.getContentPane().add(firstNumber);
        jFrame.getContentPane().add(firstNumberTextFiled);
        jFrame.getContentPane().add(secondNumber);
        jFrame.getContentPane().add(secondNumberTextFiled);
        jFrame.getContentPane().add(result);
        jFrame.getContentPane().add(resultTextFiled);
        jFrame.getContentPane().add(btnAdd);
        
        // set properties for JFrame
        jFrame.pack();
        jFrame.setLayout(new GridLayout(6, 2, 8, 4));
        jFrame.setLocation(100, 100);
        jFrame.setSize(500, 400);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setVisible(true);
    }

    public static void main(String[] args) {
        GridLayoutExample();
    }

}