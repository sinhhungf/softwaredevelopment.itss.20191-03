package group03.homework2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Calculator {

    static void GridLayoutExample() {
        // new jframe
        JFrame jFrame = new JFrame("GridLayout Example");

        JLabel firstNumber = new JLabel("First Number");
        JTextField firstNumberTextFiled = new JTextField();
        firstNumberTextFiled.setHorizontalAlignment(JTextField.RIGHT);

        JLabel secondNumber = new JLabel("Second Number");
        JTextField secondNumberTextFiled = new JTextField();
        secondNumberTextFiled.setHorizontalAlignment(JTextField.RIGHT);

        JLabel result = new JLabel("Result");
        JTextField resultTextFiled = new JTextField();
        resultTextFiled.setHorizontalAlignment(JTextField.RIGHT);
        // resultTextFiled.setEnabled(false);
        resultTextFiled.setEditable(false);

        JButton btnAdd = new JButton("+");
        btnAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String x = firstNumberTextFiled.getText();
                String y = secondNumberTextFiled.getText();
                resultTextFiled.setText(Double.parseDouble(x) + Double.parseDouble(y) + "");
            }
        });
        
        /*
         * button for substract two numbers
         * created by NCThanh - 26/09/2019
         * */
        JButton btnSubstract = new JButton("-");
        btnSubstract.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String x = firstNumberTextFiled.getText();
                String y = secondNumberTextFiled.getText();
                resultTextFiled.setText(Double.parseDouble(x) - Double.parseDouble(y) + "");
            }
        });
        /*
         * button for multiply two numbers
         * created by NCThanh - 26/09/2019
         * */
        JButton btnMultiply = new JButton("x");
        btnMultiply.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String x = firstNumberTextFiled.getText();
                String y = secondNumberTextFiled.getText();
                resultTextFiled.setText(Double.parseDouble(x) * Double.parseDouble(y) + "");
            }
        });
        // add vào jframe
        jFrame.getContentPane().add(firstNumber);
        jFrame.getContentPane().add(firstNumberTextFiled);
        jFrame.getContentPane().add(secondNumber);
        jFrame.getContentPane().add(secondNumberTextFiled);
        jFrame.getContentPane().add(result);
        jFrame.getContentPane().add(resultTextFiled);
        jFrame.getContentPane().add(btnAdd);
        jFrame.getContentPane().add(btnSubstract);
        jFrame.getContentPane().add(btnMultiply);
        // set các thuộc tính cho JFrame
        jFrame.pack();
        jFrame.setLayout(new GridLayout(6, 2, 8, 4));
        jFrame.setLocation(100, 100);
        jFrame.setSize(500, 400);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setVisible(true);
    }

    public static void main(String[] args) {
        GridLayoutExample();
    }
}
