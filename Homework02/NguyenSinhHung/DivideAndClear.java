
/*
 * button for divide two numbers
 * created by Nguy?n Sinh H�ng - 26/09/2019
 * */
JButton btnDivide = new JButton("/");
btnDivide.addActionListener(new ActionListener() {
    @Override
    public void actionPerformed(ActionEvent e) {
        String x = firstNumberTextField.getText();
        String y = secondNumberTextField.getText();
        resultTextField.setText(Double.parseDouble(x) / Double.parseDouble(y) + "");
    }
});

/*
 * button for canceling fields
 * created by Nguy?n Sinh H�ng - 26/09/2019
 * */
JButton btnCancel = new JButton("Clear");
btnCancel.addActionListener(new ActionListener() {
    @Override
    public void actionPerformed(ActionEvent e) {
        firstNumberTextField.setText("");
        secondNumberTextField.setText("");
        resultTextField.setText("");
    }
});
