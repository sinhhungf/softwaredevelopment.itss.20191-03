import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

		/*
         * button for taking modulo of two numbers
         * created by Pham Hong Quang - 26/09/2019
         * */
        JButton btnModulo = new JButton("%");
        btnModulo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String x = firstNumberTextField.getText();
                String y = secondNumberTextField.getText();
                resultTextField.setText(Double.parseDouble(x) % Double.parseDouble(y) + "");
            }
        });	